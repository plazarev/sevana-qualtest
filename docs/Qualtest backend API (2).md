# Qualtest backend API

It is description of Qualtest backend API.
API is set of REST-like calls.

Basically it is CRUD API for manipulation with devices / tests / audio / result records.


**Results API.**

Test result record looks like:
```
data = {
    "id": “UUID is here”,
    "target": “called target number”,
    "duration": 23,
    "endtime": 134123535,
    "mos_pvqa": 3.5,
    "mos_aqua": 3.6,
    "r_factor": 80,
    "mos_network": 4.1,
    "instance": “phone_id”,
    "report_pvqa": "pvqa report",
    "report_aqua": "aqua report",
    "task_name": “task_id”
    "error": "optional member with error message. Can be empty also."
}
```

* `id` is unique for every result record.
* `target` can be SIP/mobile/Skype number.
* `duration` is number of seconds spent for call. In fact it is length of capture audio.
* `endtime` is timestamp of end of call. It is always UTC time.
* `mos_pvqa` is MOS value obtained from PVQA analyzer. Valid value is in range [1.0..5.0]. Value 0.0 means - no PVQA analyze here. 
* `mos_aqua` is MOS value obtained from AQuA analyzer. Valid value is in range [1.0..5.0]. Value 0.0 means - no AQuA analyze here. 
* `mos_network` is MOS value obtained from E-model. Valid value is in range [1.0..5.0]. Value 0.0 means - no E-model analyse here.
* `r_factor` is Sevana R-factor obtained from PVQA analyzer. Valid value is in range [0..100]. This value is present only if mos_pvqa is not 0.0.
* `error` is string that explain error during this test. Usually it is SIP error code, or msg from mobile phone logs etc.
* `taskname` is name of test
* `report_pvqa` and `report_aqua` are detailed text report from PVQA and AQuA analyzers. Their format is beyond of the scope of this document. 

To get results array send GET request:

Return all available results:
```
GET http://server/probes/
```

Return results for specified phone:
```
GET http://probes/?phone_id=phone
```

Multiple phones can be specified:
```
GET http://probes/?phone_id=phone_1&phone_id=phone_2
```

Return results for specified test ID (task):
```
GET http://probes/?task_id=task
```

Return results for specified test ID and phone:
```
GET http://server/probes/?task_id=task&phone_id=phone_1
```

Also to limit number of returned results `offset` and `limit` parameters can be used in GET requests. 
```
GET http://server/probes/?offset=100&limit=50
````

Default offset value is zero, default limit value is 50.

Creation (uploading) of new result record is raw -  it is POST request:

```
POST http://server/probes/
```
Content-Type has to be application/json.

Results can't be altered. 
However they can be deleted:

```
DELETE http://server/probes/?probe_id=probe
```
`probe_id` is UUID.

**Phones API.**

This part of API allows to create / change / delete / lists available test endpoints (phones in terms of API).

To get list of all available endpoints: 

```
GET http://server/phones/
```

It will return smth like this:
```
[
    {
        "instance": "First phone 2",
        "number": "4322332323",
        "type": "mobile",
        "last_timestamp": 1554056268
    },
    {
        "instance": "Second phone",
        "number": "43234232",
        "type": "mobile",
        "last_timestamp": 0
    }
]
```

* `instance` is name & ID of endpoint. Usually it is readable phone name. There is no dedicated display name for endpoint.
* `number` is mobile phone number or SIP address or Skype ID
* `type` can be mobile / sip / skype string.
* `last_timestamp` - it is last time when endpoint was online (i.e. loaded current test list). It is UNIX timestamp - UTC time.


To get list of all endpoints which have assigned task:
```
GET http://server/phones/?task_id=task
```

Get number of all available endpoints:

```
GET http://server/phones/count
```

Parameters `offset` and `limit` can be used to control amount of fetched information.
```
GET http://phones/?offset=100&limit=50
```

Default `offset` value is zero, default `limit` value is 50.

To create new endpoint definition just send POST with Content-Type: application/json
```
POST http://server/phones/
```
Please remember - `instance` (endpoint ID) has to be unique and readable.
`last_timestamp` can be any - it will be changed on backend side anyway.

To change existing endpoint definition:
```
PUT http://server/phone/?phone_id=phone
```

To delete existing endpoint:
```
DELETE http://server/phones/?phone_id=phone
```


**Tests API**

Any endpoint can have assigned tests. 
Tests can be assigned to any endpoint.
Of course SIP related tests will not work on mobile and Skype endpoints.

To receive tests list use GET method:

```
GET – http://server/tasks/
```

The request produce smth like this:
```
[
    {
        "name": "mwc_1",
        "target": "+111111111",
        "command": "call",
        "schedule": "*/2 * * * *",
        "audio_id": "reference_1",
        "prepare_audio_for_answer": false,
        "cut_begin": 0,
        "cut_end": 0,
        "trim_level": 0
    }
]
```

* `name` is ID & readable name of test
* `target` is target number to call during the test. 
* `command` is type of command. It can be call or answer
* `schedule` is schedule of test in cron 5-parts format (seconds part is missing) 
* `audio_id` is audio ID to use in this test (it will explained in Audio API part)
* `prepare_audio_for_answer` - flag to mark that audio should be preprocessed before test. It happens in way specific for test endpoint.
* `cut_begin` - number of seconds to ignore in the beginning of call record
* `cut_end` - number of seconds to ignore on the end of call record
* `trim_level` - threshold level id dB. All audio recorded in begin or end of call below this volume level will ignored. 


Tests can be loaded for specific endpoint:
```
GET http://server/tasks/?phone_id=phone
```

Total number of tests:
```
GET http://server/tasks/count
```

Number of returned results and their offset can be controlled via `offset` and `limit` parameters.
Example:
```
GET http://server/tasks/?offset=100&limit=50
```
Default `offset` value is zero, default `limit` value is 50.


To create new test - please POST the JSON definition with Content-Type: application/json

```
POST http://server/tasks/
```

PUT request to change existing test definition:
```
PUT /tasks/?task_id=task
```

DELETE request to remove existing test definition.
```
DELETE /tasks/?task_id=task
```

Tests can be assigned to endpoints via GET method:

```
GET http://server/add_task_2_phone/?phone_id=phone&task_id=task_1
```

Assignment can be removed:
```
GET http://server/delete_task_from_phone/?task_id=task_1&phone_id=phone
```


**Audio API**

This part of API is dedicated to work recorded & reference audio.
Tests can require reference audio; also tests can produce recorded audio.
Usually this audio is relatively big (comparing with small JSON definitions from previous API parts).


Download recorded audio from test result - example:
```
GET http://server/play_audio/?probe_id=fe974e59-dceb-4a6d-bd2d-5f8fa0c6fe39
```

`probe_id` is UUID of test result. Audio file will be sent down to client. 
Content-Type: will be audio/*  (where * is mp4 / vnd.wav / ogg / smth else)


Download audio by name:
```
GET http://server/play_audio/?audio_id=My_Reference_Audio
```
`audio_id` is unique name of audio. File will be sent down to client.
Content-Type: will be audio/*  (where * is mp4 / vnd.wav / ogg / smth else)


Get list of available audios:
```
GET http://server/list_audio
GET http://server/list_audio/?audio_id=reference_1
```

Optional audio_id is reference audio ID.
Result is JSON array with audio ID & name.


Upload reference audio for tests:
```
POST http://server/upload_audio/?audio_id=audio&file=<file body>
```
It is multipart-form request.

* `audio_id` is unique and readable ID with audio file extension 
* `file` is content of audio file

Delete reference audio:

```
DELETE /delete_audio/?audio_id=audio
```

Assign audio to test via GET request - PUT new test definition with audio_id set (or unset if audio has to be unassigned).


**Statistics**

This API is extension of results API. It requests backend for some basic statistics from collected results.

```
GET http://server/statistics/?start_date=unix_timestamp_1&end_date=unix_timestamp_2&scale=[hours|days|monthes]
                &phone=phone_1&phone=softphone_1&united=[true|false]&task_id=task
```

* `start_date` & `end_date` - UNIX timestamps (UTC time)
* `scale` can be *hours* / *days* / *monthes*
* `phone` is endpoint ID. It can be missed - in this case ALL endpoints will be processed. It can be used multiple times also.
* `united` - marks if return aggregated data for all specified endpoints or return individual data for every endpoint
* `task_id` - gather information for specified test ID (**NOT IMPLEMENTED YET**)

Number of returned time intervals is limited by 1000. It is to avoid too much load on server host.
Error 500 will be returned otherwise.

Example of returned data:
```
[
    {
        "mos_pvqa": {
            "average": 3.3,
            "min": 1.667,
            "max": 4.1
        },
        "mos_aqua": {
            "average": 3.4,
            "min": 2.3,
            "max": 3.8
        },
        "rfactor_pvqa": {
        	"average": 50,
        	"min": 10,
        	"max": 90
        }
        "start_timestamp": 1551391200,
        "start_time": "2019-03-01 00:00:00 +0200 EET",
        "end_timestamp": 1551477599,
        "end_time": "2019-03-01 23:59:59 +0200 EET",
        "phone": ""
    }
]
```

* `mos_pvqa` & `mos_aqua` & `rfactor_pvqa` are MOS & R-factor values for hour / day / month time interval.
* `start_timestamp` is UNIX timestamp of interval start (UTC time)
* `start_time` is time of interval start (UTC time)
* `end_timestamp` is UNIX time of interval end (UTC time)
* `end_time` is time of interval end (UTC time)
* `phone` is endpoint ID. It can be empty - it means this is aggregated data for all requested endpoints.
  