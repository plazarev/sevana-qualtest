const STYLE_MODIFIERS = {
    top: "_position-top",
    bottom: "_position-bottom",
    left: "_position-left",
    right: "_position-right"
};

const uxGetScrollbarShadowContainer = "uxGetScrollbarShadowContainer";


export class UxDomHelper {

    private static zIndex: number = 1000;

    private static getScrollbarWidthShadowBlock: HTMLInputElement;

    /**
     * hide constructor
     */
    private constructor() {
        this.removeHelperBlocks();
    }

    public static nextZIndex(): number {
        return ++UxDomHelper.zIndex;
    }

    public static addClass(element: HTMLElement, className: string): void {
        if (element.classList) {
            element.classList.add(className);
        } else {
            element.className += " " + className;
        }
    }

    public static removeClass(element: HTMLElement, className: string): void {
        if (element.classList) {
            element.classList.remove(className);
        } else {
            element.className = element.className.replace(
                new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
        }
    }

    public static hasClass(element: HTMLElement, className: string): boolean {
        if (element.classList) {
            return element.classList.contains(className);
        } else {
            return new RegExp("(^| )" + className + "( |$)", "gi").test(element.className);
        }
    }

    /**
     * Position element according to target in window
     * @param element
     * @param {HTMLElement} target
     */
    public static relativePosition(element: any, target: HTMLElement): void {
        let elementDimensions = element.offsetParent ? {
            width: element.offsetWidth,
            height: element.offsetHeight
        } : UxDomHelper.getHiddenElementDimensions(element);
        let targetHeight = target.offsetHeight;
        let targetWidth = target.offsetWidth;
        let targetOffset = target.getBoundingClientRect();
        let viewport = UxDomHelper.getViewport();
        let top, left;

        if ((targetOffset.top + targetHeight + elementDimensions.height) > viewport.height) {
            top = -1 * (elementDimensions.height);
            element.classList.add(STYLE_MODIFIERS.top);
            element.classList.remove(STYLE_MODIFIERS.bottom);
        } else {
            top = targetHeight;
            element.classList.add(STYLE_MODIFIERS.bottom);
            element.classList.remove(STYLE_MODIFIERS.top);
        }

        if ((targetOffset.left + targetWidth - elementDimensions.width) < 0) {
            left = 0;
            element.classList.add(STYLE_MODIFIERS.right);
            element.classList.remove(STYLE_MODIFIERS.left);
        } else {
            left = targetWidth - elementDimensions.width;
            element.classList.add(STYLE_MODIFIERS.left);
            element.classList.remove(STYLE_MODIFIERS.right);
        }

        element.style.top = top + "px";
        element.style.left = left + "px";
    }

    /**
     * Position element according to target in document
     * @param element
     * @param {HTMLElement} target
     */
    public static absolutePosition(element: HTMLElement, target: HTMLElement, classes: boolean = false): void {
        let elementDimensions = element.offsetParent ? {
            width: element.offsetWidth,
            height: element.offsetHeight
        } : UxDomHelper.getHiddenElementDimensions(element);
        let elementOuterHeight = elementDimensions.height;
        let elementOuterWidth = elementDimensions.width;
        let targetOuterHeight = target.offsetHeight;
        let targetOuterWidth = target.offsetWidth;
        let targetOffset = target.getBoundingClientRect();
        let windowScrollTop = UxDomHelper.getWindowScrollTop();
        let windowScrollLeft = UxDomHelper.getWindowScrollLeft();
        let viewport = UxDomHelper.getViewport();
        let top, left;

        if (targetOffset.top + targetOuterHeight + elementOuterHeight > viewport.height) {
            top = targetOffset.top + windowScrollTop - elementOuterHeight;
            element.classList.add(STYLE_MODIFIERS.top);
            element.classList.remove(STYLE_MODIFIERS.bottom);
        } else {
            top = targetOuterHeight + targetOffset.top + windowScrollTop;
            element.classList.add(STYLE_MODIFIERS.bottom);
            element.classList.remove(STYLE_MODIFIERS.top);
        }

        if (targetOffset.left + targetOuterWidth + elementOuterWidth > viewport.width) {
            left = targetOffset.left + windowScrollLeft + targetOuterWidth - elementOuterWidth;
            element.classList.add(STYLE_MODIFIERS.left);
            element.classList.remove(STYLE_MODIFIERS.right);
        } else {
            left = targetOffset.left + windowScrollLeft;
            element.classList.add(STYLE_MODIFIERS.right);
            element.classList.remove(STYLE_MODIFIERS.left);
        }

        element.style.top = top + "px";
        element.style.left = left + "px";
    }

    public static getViewport(): any {
        let documentElement = document.documentElement,
            body = document.getElementsByTagName("body")[0],
            width = documentElement.clientWidth || window.innerWidth || body.clientWidth,
            height = documentElement.clientHeight || window.innerHeight || body.clientHeight;

        return {width: width, height: height};
    }

    public static getHiddenElementDimensions(element: HTMLElement): { width: number, height: number } {
        let dimensions: any = {};
        let oldVisibility = element.style.visibility || "visible";
        let oldDisplay = element.style.display || "none";

        element.style.visibility = "hidden";
        element.style.display = "block";

        dimensions.width = element.offsetWidth;
        dimensions.height = element.offsetHeight;

        element.style.display = oldDisplay;
        element.style.visibility = oldVisibility;

        return dimensions;
    }

    public static fadeIn(element: HTMLElement, duration: number): void {
        element.style.opacity = "0";

        let last = Date.now();
        let opacity = 0;
        let tick = function () {
            opacity = +element.style.opacity + (Date.now() - last) / duration;
            element.style.opacity = "" + opacity;
            last = Date.now();

            if (+opacity < 1) {
                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
            }
        };

        tick();
    }

    public static fadeOut(element: HTMLElement, duration: number) {
        let opacity = 1,
            interval = 50,
            gap = interval / duration;

        let fading = setInterval(() => {
            opacity = opacity - gap;

            if (opacity <= 0) {
                opacity = 0;
                clearInterval(fading);
            }
            element.style.opacity = "" + opacity;
        }, interval);
    }

    public static getWindowScrollTop(): number {
        let documentElement = document.documentElement;
        return (window.pageYOffset || documentElement.scrollTop) - (documentElement.clientTop || 0);
    }

    public static getWindowScrollLeft(): number {
        let documentElement = document.documentElement;
        return (window.pageXOffset || documentElement.scrollLeft) - (documentElement.clientLeft || 0);
    }

    public static getOuterWidth(element: HTMLElement): number {
        if (!element) {
            return;
        }

        let width = element.offsetWidth;

        let style = getComputedStyle(element);
        width += parseFloat(style.marginLeft) + parseFloat(style.marginRight);

        return width;
    }

    public static getOuterHeight(element: HTMLElement, margin?: number): number {
        if (!element) {
            return;
        }

        let height = element.offsetHeight;
        if (margin) {
            let style = getComputedStyle(element);
            height += parseFloat(style.marginTop) + parseFloat(style.marginBottom);
        }
        return height;
    }

    public static appendChild(element: HTMLElement, target: any): void {
        if (UxDomHelper.isElement(target)) {
            target.appendChild(element);
        } else if (target && target.nativeElement) {
            target.nativeElement.appendChild(element);
        } else {
            throw "Cannot append " + target + " to " + element;
        }
    }

    public static removeChild(element: HTMLElement, target: any): void {

        if (!target && element) {
            element.parentNode && element.parentNode.removeChild(element);
            return;
        }

        if (UxDomHelper.isElement(target)) {
            target.removeChild(element);
        } else if (target.element && target.element.nativeElement) {
            target.element.nativeElement.removeChild(element);
        } else {
            throw "Cannot remove " + element + " from " + target;
        }
    }

    public static removeChildFromParent(element: HTMLElement): void {
        if (UxDomHelper.isElement(element) && element.parentNode) {
            element.parentNode.removeChild(element);
        } else {
            throw "Cannot remove " + element;
        }
    }

    public static isElement(object: any): boolean {
        return (typeof HTMLElement === "object" ? object instanceof HTMLElement :
                object && typeof object === "object" && object !== null && object.nodeType === 1 && typeof object.nodeName === "string"
        );
    }

    /**
     * Returns document relative client rect.
     *
     * @param {Element} element - target element
     * @returns {ClientRect}
     */
    public static getDocumentRelativePosition(element: Element): ClientRect {
        if (!element) return;

        let clientRect = element.getBoundingClientRect();

        return {
            left: clientRect.left + pageXOffset,
            right: clientRect.right + pageXOffset,
            top: clientRect.top + pageYOffset,
            bottom: clientRect.bottom + pageYOffset,
            width: clientRect.width,
            height: clientRect.height
        }
    }



    /**
     * Return viewport width and height minus scrollbar width and height.
     *
     * @returns clientSize: {{width: number; height: number}}
     */
    public static getClientSize(container: Element = document.documentElement): { width: number, height: number } {
        return {
            width: container.clientWidth,
            height: container.clientHeight
        }
    }



    public static getScrollbarWidth(): number {
        let scrollDiv;

        if (UxDomHelper.getScrollbarWidthShadowBlock) {
            scrollDiv = UxDomHelper.getScrollbarWidthShadowBlock;
        } else {
            scrollDiv = document.createElement("div");
            scrollDiv.id = uxGetScrollbarShadowContainer;
            scrollDiv.style.cssText = "width: 100px; height: 100px; overflow: scroll; position: absolute; top: 0; left: 0; opacity: 0; visibility: hidden;";
            document.body.appendChild(scrollDiv);
            UxDomHelper.getScrollbarWidthShadowBlock = scrollDiv;
        }

        return scrollDiv.offsetWidth - scrollDiv.clientWidth;
    }

    public static checkIfIE(): boolean {
        return navigator.appName === "Microsoft Internet Explorer"
            || !!(navigator.userAgent.match(/Trident/)
                || navigator.userAgent.match(/rv:11/))
            || /msie/.test(navigator.userAgent.toLowerCase());
    }

    public static checkIfEdge(): boolean {
        return navigator.userAgent.indexOf("Edge") > -1;
    }

    private removeHelperBlocks(): void {
        const uxGetScrollbarShadowContainerElement = document.getElementById(uxGetScrollbarShadowContainer);

        if (uxGetScrollbarShadowContainerElement) {
            document.body.removeChild(uxGetScrollbarShadowContainerElement);
        }
    }
}



/**
 * scrollBy polyfill for IE ERROR TypeError: Object doesn't support property or method 'scrollBy'
 * @param {Element} element
 * @param {number} x - x-axis scroll
 * @param {number} y - y-axis scroll
 */
export function uxScrollBy(element: Element, x: number, y: number): void {
    if (typeof element.scrollBy === "function") {
        element.scrollBy(x, y);
    } else {
        uxScrollByPolyfill(element, x, y);
    }
}


export function uxIsMobileBrowser(): boolean {
    let check = false;
    (function(a): void { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {check = true; } })(navigator.userAgent || navigator.vendor || window["opera"]);
    return check;
}


export function uxIsMacLike(): boolean {
    return navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
}

export function uxThrottle(func: any, wait: any, options: any = {}): any {
    let context: any,
        args: any,
        result: any;
    let timeout: number = null;
    let previous = 0;

    let later = function(): void {
        previous = options.leading === false ? 0 : new Date().getTime();
        timeout = null;
        result = func.apply(context, args);
        context = args = null;
    };
    return function (): void {
        let now = new Date().getTime();

        if (!previous && options.leading === false) {
            previous = now;
        }

        let remaining = wait - (now - previous);
        context = this;
        args = arguments;

        if (remaining <= 0) {
            clearTimeout(timeout);
            timeout = null;
            previous = now;
            result = func.apply(context, args);
            context = args = null;
        } else if (!timeout && options.trailing !== false) {
            timeout = window.setTimeout(later, remaining);
        }
        return result;
    };
}


// copy text to clipboard
export function uxCopyText (text: string) {
    const element = document.createElement('textarea');
    element.value = text;
    document.body.appendChild(element);
    element.focus();
    element.setSelectionRange(0, element.value.length);
    document.execCommand('copy');
    document.body.removeChild(element);
}

// passive EventListenerOption support
export function  uxEventListenerOptionsSupported(): boolean {
    let supported = false;

    try {
        const opts = Object.defineProperty({}, "passive", {
            get() {
                supported = true;
            }
        });

        window.addEventListener("uxEventListenerOptionsSupportedEvent", null, opts);
        window.removeEventListener("uxEventListenerOptionsSupportedEvent", null, opts);
    } catch (e) {}

    return supported;
}

export function uxAddPassiveEventListenerOption() {
    return uxEventListenerOptionsSupported() ? { passive: true } : false;
}


/*helpers*/
function uxScrollByPolyfill(element: Element, x: number, y: number): void {
    element.scrollLeft = element.scrollLeft + x;
    element.scrollTop = element.scrollTop + y;
}
