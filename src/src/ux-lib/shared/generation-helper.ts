export class UxGenerationHelper {

    /*return unique string identifier like 3b9ab302-4c14*/
    public static generateUniqueId(): string {
        return UxGenerationHelper.getRandomSymbols4() + UxGenerationHelper.getRandomSymbols4() + "-" + UxGenerationHelper.getRandomSymbols4();
    }

    private static getRandomSymbols4(): string {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
}