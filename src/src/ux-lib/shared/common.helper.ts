export class UxCommonHelper {

    public static getUniqueArray(array: any[]): any[] {
        return array.filter(function (item, index, self) {
            return self.indexOf(item) === index;
        });
    }

    public static isNumeric(n: any): boolean {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    public static convertToNumber(fromValue: any): number {
        if (typeof(fromValue) === "string") {
            fromValue = fromValue.trim().toLowerCase();

            if (fromValue === "") {
                fromValue = undefined;

                return fromValue;
            }
        }
        fromValue = +fromValue;
        if (isNaN(fromValue)) {
            fromValue = undefined;
        }
        return fromValue;
    }

}
