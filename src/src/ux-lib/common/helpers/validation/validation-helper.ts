import {FormGroup} from "@angular/forms";

export const VALIDATION_PATTERNS = {
    email: "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
    url: "(http:\\/\/www\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?"
};

export class UxValidationHelper {
    public static createPasswordMatchValidator(passwordConfirmFormField: { checkValid: () => void },
                                               passwordFieldControlName: string = "password",
                                               passwordConfirmFieldControlName: string = "passwordConfirm",
                                               passwordMismatchId: string = "mismatchPassword"): Function {
        return function passwordMatchValidator(group: FormGroup): { [key: string]: boolean } {
            let password = group.get(passwordFieldControlName),
                passwordConfirm = group.get(passwordConfirmFieldControlName),
                changed = false,
                result: { [key: string]: boolean } = null;

            if (password && passwordConfirm) {
                let errors = Object.assign({}, passwordConfirm.errors);
                if (password.value === passwordConfirm.value) {
                    if (errors[passwordMismatchId]) {
                        delete errors[passwordMismatchId];
                        changed = true;
                        if (Object.keys(errors).length === 0) {
                            errors = null; //to change control state by angular
                        }
                    }
                } else {
                    if (!errors[passwordMismatchId]) {
                        changed = true;
                        errors[passwordMismatchId] = true;
                    }
                    result = {};
                    result[passwordMismatchId] = true;
                }
                if (changed) {
                    passwordConfirm.setErrors(errors);
                    if (passwordConfirmFormField && passwordConfirmFormField.checkValid) {
                        passwordConfirmFormField.checkValid();
                    }
                }
            }
            return result;
        }
    }
}

export class ValidationHelper extends UxValidationHelper {  //deprecated. todo remove after 1.0.34
}