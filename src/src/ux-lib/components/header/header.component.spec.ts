import {ComponentFixture, TestBed} from "@angular/core/testing";
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";
import {By} from "@angular/platform-browser";
import {UxHeaderComponent} from "./header.component";
import {UxRouterLinkComponent} from "./router-link/router-link.component";
import {UxHeaderButtonComponent} from "./header-button/header-button.component";
import {UxHeaderGroupComponent} from "./header-group/header-group.component";
import {UxRouteTabPanelComponent} from "../route-tab-panel/route-tab-panel.component";

@Component({
    selector: "ux-header-test",
    template: `
        <ux-header>

            <ux-header-group>
                <ux-router-link [link]="'/overview'">Overview</ux-router-link>
                <ux-router-link [link]="'/services'">Services</ux-router-link>
            </ux-header-group>

            <ux-header-group position="right">
                <ux-header-button [icon]="'search'" title="search"></ux-header-button>
                <ux-header-button [icon]="'bell'" title="bell"></ux-header-button>
            </ux-header-group>
            
            Projected content

        </ux-header>
    `
})
class UxHeaderTestComponent {}

describe("UxHeaderComponent", () => {

    let fixture:   ComponentFixture<UxHeaderTestComponent>,
        fixtureHeader:   ComponentFixture<UxHeaderComponent>,
        fixtureHeaderGroup:   ComponentFixture<UxHeaderGroupComponent>,
        fixtureHeaderButton:   ComponentFixture<UxHeaderButtonComponent>,
        component: UxHeaderTestComponent,
        componentHeader: UxHeaderComponent,
        componentHeaderGroup: UxHeaderGroupComponent,
        componentRouterLink: UxRouterLinkComponent,
        componentHeaderButton: UxHeaderButtonComponent,
        element;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                UxHeaderTestComponent,
                UxHeaderComponent,
                UxHeaderGroupComponent,
                UxRouteTabPanelComponent,
                UxHeaderButtonComponent
            ],
            schemas:      [ NO_ERRORS_SCHEMA ],
            providers: []
        });

        fixture = TestBed.createComponent(UxHeaderTestComponent);
        fixtureHeader = TestBed.createComponent(UxHeaderComponent);
        fixtureHeaderGroup = TestBed.createComponent(UxHeaderGroupComponent);
        fixtureHeaderButton = TestBed.createComponent(UxHeaderButtonComponent);

        component = fixture.componentInstance;
        componentHeader = fixtureHeader.componentInstance;
        componentHeaderGroup = fixtureHeaderGroup.componentInstance;
        componentHeaderButton = fixtureHeaderButton.componentInstance;
        element = fixture.nativeElement;
        fixture.detectChanges();
        fixtureHeader.detectChanges();
    });

    it("UxHeader should have #mobile boolean property", () => {
        expect(typeof componentHeader.mobile).toBe("boolean");
    });

    it("UxHeader should have #logo boolean property and .ux-header__logo appears by default", () => {
        expect(typeof componentHeader.logo).toBe("boolean");
        expect(
            fixtureHeader.debugElement.query(By.css(".ux-header__logo")).nativeElement).not.toBeNull();
    });

    it("UxHeader should have #links property", () => {
        let isArray: boolean = Array.isArray(componentHeader.links);

        // Check default
        expect(isArray).toBe(true);

        // Check type
        componentHeader.links = [
            {
                "title": "Products",
                "url": "test url"
            }
        ];
        fixtureHeader.detectChanges();
        expect(
            fixtureHeader.debugElement.query(By.css(".ux-route-tab-panel-header__item-title")).nativeElement.innerHTML)
            .toContain("Products");

    });

    it("UxHeader should Project content", () => {
        expect(
            fixture.debugElement.query(By.css("ux-header")).nativeElement.innerHTML).toContain("Projected content");
    });

    it("UxHeader should project ux-header-button", () => {
        expect(
            fixture.debugElement.query(By.css("ux-header")).nativeElement.innerHTML).toContain("ux-header-button");
    });

    it("UxHeader Should have #onLogoClick() event", () => {
        let currentEvent: MouseEvent,
            clicked = false;

        componentHeader.onLogoClick.subscribe((event: MouseEvent) => {
            clicked = true;
        });

        fixtureHeader.debugElement.query(By.css(".ux-header__logo")).triggerEventHandler("click", null);

        fixtureHeader.detectChanges();

        expect(clicked).toBe(true);
    });
});
