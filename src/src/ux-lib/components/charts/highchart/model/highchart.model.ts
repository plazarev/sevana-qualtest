import * as Highcharts from 'highcharts';
import {ChartType} from "../../../../../app/modules/reports/model/chart.model";
const HighchartsMore = require("highcharts/highcharts-more");
const HighchartsHeatmap = require("highcharts/modules/heatmap");
HighchartsMore(Highcharts);
HighchartsHeatmap(Highcharts);

export interface HighChartModel extends Highcharts.Options {
  seriesEmpty: boolean;
  chartType: ChartType;
  dateFrom: number;
  dateTo: number;
}
