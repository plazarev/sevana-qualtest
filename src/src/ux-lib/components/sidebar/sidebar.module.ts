import {NgModule} from "@angular/core";

import {SharedModule} from "../../shared/shared.module";
import {UxSidebarComponent} from "./sidebar.component";
import {UxScrollModule} from "../scroll/scroll.module";


@NgModule({
    imports: [SharedModule, UxScrollModule],
    declarations: [UxSidebarComponent],
    exports: [UxSidebarComponent]
})
export class UxSidebarModule {
}
