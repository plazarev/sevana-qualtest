import {
    AfterViewInit,
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    HostBinding,
    Input,
    NgZone,
    OnDestroy,
    Output,
    ViewChild,
    ChangeDetectionStrategy
} from "@angular/core";
import {UxDomHelper} from "../../shared/dom/dom-helper";
import {UxPropertyHandler} from "../../common/decorator/ux-property-handler";
import {UxDataOptionsModel} from "../common.model";

@Component({
    selector: "ux-sidebar",
    templateUrl: "sidebar.component.html",
    changeDetection:ChangeDetectionStrategy.OnPush
})
export class UxSidebarComponent implements AfterViewInit, OnDestroy {

    @Input()
    public animatedElementId: string;

    @UxPropertyHandler({
        afterChange: afterChangeActive
    })
    @HostBinding("class._active")
    @Input()
    public active: boolean = false;

    @Output()
    public activeChange: EventEmitter<boolean> = new EventEmitter<boolean>();


    @ViewChild("sidebarPanel")
    private sidebarPanelRef: ElementRef;

    @ViewChild("overlay")
    private overlayRef: ElementRef;

    @ContentChild("customControl")
    private set customControl(customControlElement) {
        this._customControl = !!customControlElement;
    }

    @HostBinding("class.ux-sidebar")
    private hostClass: boolean = true;

    /** @internal */
    public _customControl: boolean = false;

    private _dataOptions: UxDataOptionsModel = {
        iconLabel:
            {
                activateSidebarIcon: "Activate sidebar"
            }
    };

    @Input()
    public set dataOptions(value: UxDataOptionsModel) {
        if (value) {
            this._dataOptions.iconLabel = {...this._dataOptions.iconLabel, ...value.iconLabel};
        }
    }

    public get dataOptions(): UxDataOptionsModel {
        return this._dataOptions;
    }

    private sidebarPanel: HTMLElement;
    private overlay: HTMLElement;

    public ngAfterViewInit(): void {
        this.sidebarPanel = this.sidebarPanelRef.nativeElement;
        this.overlay = this.overlayRef.nativeElement;
        document.body.appendChild(this.sidebarPanel);
        document.body.appendChild(this.overlay);
    }

    constructor(private zone: NgZone) {
    }

    public ngOnDestroy(): void {
        UxDomHelper.removeChildFromParent(this.sidebarPanel);
        UxDomHelper.removeChildFromParent(this.overlay);
    }

    /** @internal */
    public _onControlClick(): void {
        this.active = true;

        this.zone.runOutsideAngular(() => {
            if (this.animatedElementId) {
                let element = document.getElementById(this.animatedElementId),
                    body = document.body;

                body.classList.add("ux-sidebar__overflow");

                element.style.transition = "transform .3s ease";
                element.classList.add("ux-sidebar__animated-element");
            }
        });
    }

    /** @internal */
    public _onOverlayClick(): void {

        this.active = false;

        this.zone.runOutsideAngular(() => {
            if (this.animatedElementId) {
                let element = document.getElementById(this.animatedElementId);
                element.classList.remove("ux-sidebar__animated-element");
            }
        });
    }
}



/*Helpers*/
export function afterChangeActive(newValue: boolean, oldValue: boolean): void {
    if (this.sidebarPanel && (newValue !== oldValue)) {
        if (newValue) {
            this._onControlClick();
        } else {
            this._onOverlayClick();
        }

        this.activeChange.emit(this.active);
    }
}
