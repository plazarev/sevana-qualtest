import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {UxTableComponent} from "./table.component";
import {UxScrollModule} from "../scroll/scroll.module";
import {VirtualScrollerModule} from "ngx-virtual-scroller";
import {UxCheckboxFieldModule} from "../fields/checkbox/checkbox-field.module";
import {UxTableHeaderComponent} from "./header/table-header.component";
import {UxTableRowComponent} from "./row/table-row.component";

@NgModule({
    imports: [SharedModule, UxCheckboxFieldModule, UxScrollModule, VirtualScrollerModule],
    exports: [UxTableComponent, UxTableHeaderComponent],
    declarations: [UxTableComponent, UxTableHeaderComponent, UxTableRowComponent]
})
export class UxTableModule {}
