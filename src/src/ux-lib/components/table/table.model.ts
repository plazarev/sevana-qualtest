import {TemplateRef} from "@angular/core";

export type UxSortTypes = "asc" | "desc" | "default" | true;

/* deprecated use instead UxInlineNgStyles. todo remove in 2.0 version */
export interface InlineNgStyles {
    [key: string]: string;
}

export interface UxInlineNgStyles extends InlineNgStyles {
}

export interface UxTableColumn {
    id?: string;
    type?: string;
    value?: any;
    pin?: boolean;
    url?: string;
    sort?: UxSortTypes;
    filter?: boolean;
    filterType?: string;
    styleClass?: string;
    inlineStyles?: InlineNgStyles;
    unclickable?: boolean;
    contentModel?: any;
    resizable?: boolean;
}

export interface UxTableRow {
    columns: UxTableColumn[];
    styleClass?: string;
    inlineStyles?: InlineNgStyles;
    selected?: boolean;
    unclickable?: boolean;
    initIndex?: number;
    id?: string | number;
}

export interface UxTableSection {
  rows: UxTableRow[];
  styleClass?: string;
}

export interface UxTableHeader extends UxTableSection {
}

export interface UxTableBody extends UxTableSection {
}

export interface UxTable {
    header?: UxTableHeader;
    body?: UxTableBody;
    clearCheckedRows?: () => void;
    clearPinnedRows?: () => void;
    tableData?: UxTableData;
    emptyTableContent?: TemplateRef<{model: UxTable}> | string;
    customSortFunction?: Function;
    updateTableData?: Function;
}

export interface UxTableColumnEvent {
    header: boolean;
    column: UxTableColumn;
    columnIndex: number;
    row: UxTableRow;
    originalEvent: Event;
}

export interface UxTableColumnSortEvent {
  columnIndex: number;
  sortOrder: UxSortTypes;
}

export interface UxTableLinkEvent {
    header: boolean;
    column: UxTableColumn;
    columnIndex: number;
    row: UxTableRow;
    originalEvent: Event;
}

export interface UxTableFilterEvent {
    header: boolean;
    column: UxTableColumn;
    columnIndex: number;
    row: UxTableRow;
    originalEvent: Event;
}

export interface UxTablePinEvent {
    header: boolean;
    column: UxTableColumn;
    columnIndex: number;
    row: UxTableRow;
    pinnedRows: UxTableRow[] | null;
    originalEvent: Event;
}

export interface UxTableCheckEvent {
  uncheckedRows: UxTableRow[];
  checkedRows: UxTableRow[];
}

export interface UxTableData {
    checkedRows: UxTableRow[];
    pinnedRows: UxTableRow[]
}
