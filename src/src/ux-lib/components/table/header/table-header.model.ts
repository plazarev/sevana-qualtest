export interface UxTableHeaderInfoModel {
    text?: string;
    number? : number;
    type? : "pin" | "checkbox";
    styleClass: string;
}

export interface UxTableHeaderModel {
    title?: string;
    info?: UxTableHeaderInfoModel[];
}

export interface UxTableHeaderInfoCrossEvent {
    item: UxTableHeaderInfoModel;
    originalEvent: Event;
}

export interface UxTableHeaderInfoItemEvent {
    item: UxTableHeaderInfoModel;
    originalEvent: Event;
}