export interface UxDataOptionsModel {
    iconLabel?: { [key: string]: string };
    inputLabel?: any; // { [key: string]: string } | string;
    [key: string]: any;
}
