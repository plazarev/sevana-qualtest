import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {UxFadeToggleDirective} from "./fade-toggle.directive";

@NgModule({
    imports: [SharedModule],
    exports: [UxFadeToggleDirective],
    declarations: [UxFadeToggleDirective],
})
export class UxFadeToggleModule {}