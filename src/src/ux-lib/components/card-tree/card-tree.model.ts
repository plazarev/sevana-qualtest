import {TemplateRef} from "@angular/core";

export interface UxCardTreeModel {
    children: UxCardTreeNodeModel[];
    template?: TemplateRef<any>;
    controlTemplate?: TemplateRef<any>;
}

export interface UxCardTreeNodeModel {
    id?: string;
    visible?: boolean;
    opened?: boolean;
    selected?: boolean;
    children?: UxCardTreeNodeModel[];
    hasChildren?: boolean;
    manualOperation?: boolean;
}
