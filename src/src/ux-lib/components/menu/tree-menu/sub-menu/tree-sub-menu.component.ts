import {Component, EventEmitter, Input, Output, TemplateRef} from "@angular/core";
import {TreeMenuItem, TreeMenuItemChangeEvent} from "../tree-menu.model";
import {UxPropertyConverter} from "../../../../common/decorator/ux-property-converter";

@Component({
    selector: "ux-tree-sub-menu",
    templateUrl: "./tree-sub-menu.component.html",
    host: {"[class.ux-tree-sub-menu]": "true"}
})
export class UxTreeSubMenuComponent {

    @Input()
    public items: TreeMenuItem[];

    @UxPropertyConverter("string", "caption")
    @Input()
    public itemCaptionKey: string;

    @Input()
    public template: TemplateRef<any>;

    /** for internal use */
    @Input()
    public level: number;

    @Output()
    public onItemClick: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    @Output()
    public onToggle: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    @Output()
    public onItemClose: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    /** @internal */
    public _itemClick(event: Event, item: TreeMenuItem): void {
        this.onItemClick.emit({
            item: item,
            originalEvent: event
        });
    }

    /** @internal */
    public _itemClose(event: Event, item: TreeMenuItem): void {
        event.stopPropagation();
        this.onItemClose.emit({
            item: item,
            originalEvent: event
        });
    }

    /** @internal */
    public _itemArrowClick(event: Event, item: TreeMenuItem): void {
        event.stopPropagation();
        item.opened = !item.opened;

        this.onToggle.emit({
            item: item,
            originalEvent: event
        });
    }

    /** @internal */
    public _itemHasChildren(item: TreeMenuItem): boolean {
        return item.children && item.children.length > 0;
    }

    /** @internal */
    public _trackByFn(index: number, item: TreeMenuItem): string {
        return item && item.id;
    }
}
