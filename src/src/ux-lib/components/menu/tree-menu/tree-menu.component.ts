import {Component, EventEmitter, Input, Output, TemplateRef} from "@angular/core";
import {TreeMenuItem, TreeMenuItemChangeEvent} from "./tree-menu.model";
import {UxPropertyConverter} from "../../../common/decorator/ux-property-converter";

@Component({
    selector: "ux-tree-menu",
    templateUrl: "./tree-menu.component.html",
    host: {"[class.ux-tree-menu]": "true"}
})
export class UxTreeMenuComponent {

    @Input()
    public items: TreeMenuItem[];

    @UxPropertyConverter("string", "caption")
    @Input()
    public itemCaptionKey: string;

    @Input()
    public template: TemplateRef<any>;

    @Input()
    public mobile: boolean = false;


    @Output()
    public onItemSelect: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    @Output()
    public onToggle: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    @Output()
    public onItemClose: EventEmitter<TreeMenuItemChangeEvent> = new EventEmitter<TreeMenuItemChangeEvent>();

    /** @internal */
    public _onItemClick(event: TreeMenuItemChangeEvent): void {
        let selectedItem = this.findSelectedItem(this.items);

        if (selectedItem !== event.item) {

            if (selectedItem) {
                selectedItem.selected = false;
            }

            event.item.selected = true;
            this.onItemSelect.emit(event);
        }
    }

    /** @internal */
    public _onToggle(event: TreeMenuItemChangeEvent): void {
        this.onToggle.emit(event);
    }

    /** @internal */
    public _onItemClose(event: TreeMenuItemChangeEvent): void {
        event.originalEvent.stopPropagation();
        this.onItemClose.emit(event);
    }

    /** @internal */
    public _itemHasChildren(item: TreeMenuItem): boolean {
        return item.children && item.children.length > 0;
    }

    private findSelectedItem(items: TreeMenuItem[]): TreeMenuItem {
        if (!items) {
            return null;
        }
        for (let item of items) {
            if (item.selected) {
                return item;
            } else if (item.children && item.children.length > 0) {
                let result = this.findSelectedItem(item.children);
                if (result) {
                    return result;
                }
            }
        }
        return null;
    }
}
