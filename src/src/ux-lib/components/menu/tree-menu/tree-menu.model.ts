/* deprecated use instead UxTreeMenuItem. todo remove in 2.0 version */
export interface TreeMenuItem {
    id?: string;
    caption?: string;
    opened?: boolean;
    selected?: boolean;
    closeable?: boolean;
    disabled?: boolean;
    children?: TreeMenuItem[];
}

export interface UxTreeMenuItem extends TreeMenuItem {
}

/* deprecated use instead UxTreeMenuItemChangeEvent. todo remove in 2.0 version */
export interface TreeMenuItemChangeEvent {
    item: TreeMenuItem;
    originalEvent: Event;
}

export interface UxTreeMenuItemChangeEvent extends TreeMenuItemChangeEvent {
}
