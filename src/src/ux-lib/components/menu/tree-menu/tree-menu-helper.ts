import {TreeMenuItem} from "./tree-menu.model";

export class UxTreeHelper {

    public static isDirectParent(possibleParent: TreeMenuItem, currentItem: TreeMenuItem): boolean {
        let children: TreeMenuItem[] = possibleParent.children;
        if (children) {
            for (let i = 0; i < children.length; i++) {
                if (children[i] === currentItem) {
                    return true;
                }
            }
        }
        return false;
    }

    public static isParent(possibleParent: TreeMenuItem, currentItem: TreeMenuItem): boolean {
        let children: TreeMenuItem[] = possibleParent.children;
        if (children) {
            for (let i = 0; i < children.length; i++) {
                let item: TreeMenuItem = children[i];

                if (item === currentItem) {
                    return true;
                }

                if (item.children instanceof Array) {
                    let result = this.isParent(item, currentItem);
                    if (result === true) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static getParent(model: TreeMenuItem, currentItem: TreeMenuItem): TreeMenuItem {
        let children: TreeMenuItem[] = model.children;
        if (children) {
            for (let i = 0; i < children.length; i++) {
                let item: TreeMenuItem = children[i];

                if (item === currentItem) {
                    return model;
                }

                if (item.children instanceof Array) {
                    let result = this.getParent(item, currentItem);
                    if (result !== undefined) {
                        return result;
                    }
                }
            }
        }
        return undefined;
    }


}