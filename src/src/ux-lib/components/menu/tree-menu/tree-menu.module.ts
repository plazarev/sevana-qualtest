import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {UxTreeMenuComponent} from "./tree-menu.component";
import {UxTreeSubMenuComponent} from "./sub-menu/tree-sub-menu.component";

export {MenuItem} from "../model/menu-item.model";
export {MenuItemChangeEvent} from "../model/menu-item-change-event.model";
export {UxTreeHelper} from "./tree-menu-helper";

@NgModule({
    imports: [SharedModule],
    exports: [UxTreeMenuComponent],
    declarations: [UxTreeMenuComponent, UxTreeSubMenuComponent]
})
export class UxTreeMenuModule {
}