/* deprecated use instead UxMenuItem. todo remove in 2.0 version */
export interface MenuItem {
    id?: string;
    caption?: string;
    description?: string;
    iconClass?: string;
    selected?: boolean;
    closeable?: boolean;
    disabled?: boolean;
}

export interface UxMenuItem extends MenuItem {
}