import {MenuItem} from "./menu-item.model";

/* deprecated use instead UxMenuItemChangeEvent. todo remove in 2.0 version */
export interface MenuItemChangeEvent {
    item: MenuItem;
    originalEvent: Event;
}

export interface UxMenuItemChangeEvent extends MenuItemChangeEvent{
}