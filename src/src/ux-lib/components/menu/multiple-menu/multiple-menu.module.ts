import {NgModule} from '@angular/core';
import {UxMultipleMenuComponent} from './multiple-menu.component';
import {SharedModule} from "../../../shared/shared.module";
import {UxScrollModule} from "../../scroll/scroll.module";

@NgModule({
    imports: [SharedModule, UxScrollModule],
    exports: [UxMultipleMenuComponent],
    declarations: [UxMultipleMenuComponent],
    providers: [],
})
export class UxMultipleMenuModule {
}
