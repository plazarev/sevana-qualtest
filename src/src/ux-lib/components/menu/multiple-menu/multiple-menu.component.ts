import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    TemplateRef,
    ViewChild
} from '@angular/core';
import {UxScrollComponent} from "../../scroll/scroll.component";


export class UxMultipleMenuModel {
    links?: [UxMultipleMenuLinkModel];
    template?: TemplateRef<any>;
    templateModel?: any;
    title?: string;
}

export class UxMultipleMenuLinkModel {
    id?: string;
    caption?: string;
    url?: string;
    originalEvent?: Event;
    data?: UxMultipleMenuModel;
    showChild?: boolean;
}


const DELAY_TIME = 50;


@Component({
    selector: 'ux-multiple-menu',
    templateUrl: './multiple-menu.component.html',
    host: {
        "[class.ux-multiple-menu]": "true"
    }
})

export class UxMultipleMenuComponent implements OnInit, AfterViewInit {

    @Input()
    public model: UxMultipleMenuModel;

    @Input()
    public mobile: boolean;

    @Input()
    public menuHeight: number;

    @Input()
    public menuMaxHeight: number;

    @Input()
    public menuMinHeight: number;

    @Output()
    public onMenuLinkClick: EventEmitter<UxMultipleMenuLinkModel> = new EventEmitter<UxMultipleMenuLinkModel>();

    @Output()
    public onMenuLinkMouseEnter: EventEmitter<UxMultipleMenuLinkModel> = new EventEmitter<UxMultipleMenuLinkModel>();

    @Output()
    public onMenuLinkMouseLeave: EventEmitter<UxMultipleMenuLinkModel> = new EventEmitter<UxMultipleMenuLinkModel>();

    /* @internal */
    @Input("menuLevel")
    public _menuLevel: number;

    @ViewChild("scroll")
    private scroll: UxScrollComponent;

    /* @internal */
    public _childMenuLevel: number;

    /* @internal */
    public _onMenuLinkClick(link: UxMultipleMenuLinkModel, event: Event): void {
        let self = this;

        link.originalEvent = event;

        event.stopPropagation();
        
        if (!link.url || self.mobile) {
            event.preventDefault();
        }

        self.onMenuLinkClick.emit(link);

        if (self.mobile) {
            self.onMenuLinkTap(link, event);
        }
    }

    private mouseEnterTimeoutId: number;
    private mouseLeaveTimeoutId: number;
    private element: HTMLElement;
    private previousLink: UxMultipleMenuLinkModel;

    constructor(private elementRef: ElementRef) {
        this.element = elementRef.nativeElement;
    }

    ngOnInit(): void {
        this._menuLevel = this._menuLevel || 0;
        this._childMenuLevel = this._menuLevel + 1;
    }

    private findMaxHeight(): number {
        let self = this;

        let menus = self.element.querySelectorAll(".ux-multiple-menu"),
            menusArray = Array.prototype.slice.call(menus),
            finalMaxHeight = 0;

        self.element.style.height = "auto";

        menusArray.forEach((menu: HTMLElement) => {
            menu.style.height = "auto";
        });

        finalMaxHeight = self.element.offsetHeight;

        menusArray.forEach((menu: HTMLElement) => {
            finalMaxHeight = menu.offsetHeight > finalMaxHeight ? menu.offsetHeight : finalMaxHeight;
        });

        if (self.menuMinHeight && finalMaxHeight < self.menuMinHeight) {
            finalMaxHeight = self.menuMinHeight;
        } else if (self.menuMaxHeight && finalMaxHeight > self.menuMaxHeight) {
            finalMaxHeight = self.menuMaxHeight;
        }

        return finalMaxHeight;
    }

    ngAfterViewInit(): void {
        let self = this;

        if (self._menuLevel === 0) {
            self.update();
        }
    }

    public update(): void {
        let self = this,
            finalMaxHeight;

        let currentMaxHight = self.findMaxHeight();

        if (self.menuHeight) {
            finalMaxHeight = self.menuHeight;
        } else {
            finalMaxHeight = currentMaxHight;
        }

        self.element.style.height = finalMaxHeight + "px";
        self.element.classList.add("_absolute");

        let childrenArray = [].slice.call(self.element.querySelectorAll(".ux-multiple-menu._child"));

        childrenArray.forEach((item: HTMLElement) => {
            item.style.height = finalMaxHeight + "px";
        });

        self.scroll.update();
    }

    /* @internal */
    public onMenuLinkTap(link: UxMultipleMenuLinkModel, event: Event): void {
        let self = this;

        if (!self.mobile) {
            return;
        }

        link.showChild = true;

        if (self._menuLevel === 0) {
            let cbClick = (e: Event) => {
                if (!(<HTMLElement>e.target).classList.contains("ux-multiple-menu")
                    && !self.element.contains(<HTMLElement>e.target)) {
                    link.showChild = false;
                    document.removeEventListener("click", cbClick);
                }
            };

            document.addEventListener("click", cbClick);
        }
    }

    /* @internal */
    public _onLinkMouseEnter(link: UxMultipleMenuLinkModel, event: Event): void {
        let self = this;

        if (self.model && Array.isArray(self.model.links)) {
            self.closeAllLinks(self.model.links);
        }

        window.clearTimeout(self.mouseLeaveTimeoutId);

        self.mouseEnterTimeoutId = window.setTimeout(() => {
            link.showChild = true;
            self.previousLink = link;
        }, DELAY_TIME);

        self.onMenuLinkMouseEnter.emit(link);
    }

    /* @internal */
    public _onLinkMouseLeave(link: UxMultipleMenuLinkModel, event: Event): void {
        let self = this;

        window.clearTimeout(self.mouseEnterTimeoutId);

        if (self.previousLink) {
            self.previousLink.showChild = false;
        }

        self.mouseLeaveTimeoutId = window.setTimeout(() => {
            link.showChild = false;
        }, DELAY_TIME);

        self.onMenuLinkMouseLeave.emit(link);
    }

    private closeAllLinks(links: UxMultipleMenuLinkModel[]): void {
        const self = this;

        if (Array.isArray(links)) {
            links.forEach((link: UxMultipleMenuLinkModel) => {
                link.showChild = false;

                if (link.data && Array.isArray(link.data.links)) {
                    self.closeAllLinks(link.data.links);
                }
            });
        }
    }

    /** @internal */
    public _trackByFn(index: number, item: UxMultipleMenuLinkModel): string {
        return item && item.id;
    }
}
