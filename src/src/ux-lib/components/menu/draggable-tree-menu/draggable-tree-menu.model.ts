import {TreeMenuItem} from "../tree-menu/tree-menu.model";

/* deprecated use instead UxTreeMenuItemMoveEvent. todo remove in 2.0 version */
export interface TreeMenuItemMoveEvent {
    draggedItem: TreeMenuItem;
    targetItem: TreeMenuItem;
    originalEvent: Event;
}

export interface UxTreeMenuItemMoveEvent extends TreeMenuItemMoveEvent {

}