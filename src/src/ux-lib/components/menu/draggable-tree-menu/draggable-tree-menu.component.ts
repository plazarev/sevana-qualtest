import {Component, EventEmitter, Output} from "@angular/core";

import {UxTreeMenuComponent} from "../tree-menu/tree-menu.component";
import {UxTreeHelper} from "../tree-menu/tree-menu-helper";
import {TreeMenuItem} from "../tree-menu/tree-menu.model";
import {TreeMenuItemMoveEvent} from "./draggable-tree-menu.model";
import {TreeMenuDropEvent} from "./sub-menu/draggable-tree-sub-menu.component";

@Component({
    selector: "ux-draggable-tree-menu",
    templateUrl: "./draggable-tree-menu.component.html",
    host: {
        "[class.ux-tree-menu]": "true",
        "[class.ux-draggable-tree-menu]": "true"
    }
})
export class UxDraggableTreeMenuComponent extends UxTreeMenuComponent {

    @Output()
    public onItemMove: EventEmitter<TreeMenuItemMoveEvent> = new EventEmitter<TreeMenuItemMoveEvent>();

    protected draggedItem: TreeMenuItem;

    /** @internal */
    public _dragStartHandler(draggedItem: TreeMenuItem): void {
        this.draggedItem = draggedItem;
    }

    /** @internal */
    public _dragEndHandler(): void {
        this.draggedItem = null;
    }

    /** @internal */
    public _dropHandler(dropEvent: TreeMenuDropEvent): void {
        let draggedItem = this.draggedItem,
            targetItem = dropEvent.targetItem;

        // Conditions:
        // 1. target item and dragget item are not the same
        // 2. dragged item is not moving in its direct parent (this dragged item is already there)
        // 3. dragged item is not moving in its child
        if (draggedItem !== targetItem
            && !UxTreeHelper.isDirectParent(targetItem, draggedItem)
            && !UxTreeHelper.isParent(draggedItem, targetItem)) {
            this.onItemMove.emit({
                draggedItem,
                targetItem,
                originalEvent: dropEvent.originalEvent
            });
        }

        this.draggedItem = null;
    }
}