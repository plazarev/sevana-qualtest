import {Component, EventEmitter, Input, Output, Renderer2} from "@angular/core";
import {TreeMenuItem} from "../../tree-menu/tree-menu.model";
import {UxTreeSubMenuComponent} from "../../tree-menu/sub-menu/tree-sub-menu.component";

/* deprecated use instead UxTreeMenuDropEvent. todo remove in 2.0 version */
export interface TreeMenuDropEvent {
    targetItem: TreeMenuItem;
    originalEvent: any;
}

export interface UxTreeMenuDropEvent extends TreeMenuDropEvent {
}

@Component({
    selector: "ux-draggable-tree-sub-menu",
    templateUrl: "./draggable-tree-sub-menu.component.html",
    host: {
        "[class.ux-tree-sub-menu]": "true",
        "[class.ux-draggable-tree-sub-menu]": "true"
    }
})
export class UxDraggableTreeSubMenuComponent extends UxTreeSubMenuComponent {

    /** for internal use */
    @Input()
    public level: number;

    @Output()
    public onDragStart: EventEmitter<TreeMenuItem> = new EventEmitter<TreeMenuItem>();

    @Output()
    public onDragEnd: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    public onDrop: EventEmitter<TreeMenuDropEvent> = new EventEmitter<TreeMenuDropEvent>();

    protected draggedItem: TreeMenuItem;

    private branchDragOverTimer: {
        treeItem: TreeMenuItem,
        timeoutId: number
    };


    constructor(private renderer: Renderer2) {
        super();
    }


    /** @internal */
    public _dragStartHandler(draggedItem: TreeMenuItem): void {
        this.draggedItem = draggedItem;
        this.onDragStart.emit(draggedItem);
    }

    /** @internal */
    public _dragEndHandler(): void {
        this.draggedItem = null;
        this.onDragEnd.emit();
    }

    /** @internal */
    public _dragEnterHandler(event: any, draggedOverItem: TreeMenuItem): void {
        if (this.draggedItem !== draggedOverItem) {
            this.renderer.addClass(event.currentTarget, "_highlighted");

            if (!(this.branchDragOverTimer && draggedOverItem === this.branchDragOverTimer.treeItem)
                && this._itemHasChildren(draggedOverItem)) {

                let timeoutId = window.setTimeout(() => {
                    if (draggedOverItem.opened === undefined) {
                        draggedOverItem.opened = true;
                    } else {
                        draggedOverItem.opened = !draggedOverItem.opened;
                    }
                }, 1000);

                this.branchDragOverTimer = {
                    treeItem: draggedOverItem,
                    timeoutId
                };
            }
        }
    }

    /** @internal */
    public _dragLeaveHandler(event: any, draggedOverItem: TreeMenuItem): void {
        if (this.draggedItem !== draggedOverItem) {

            if (event.currentTarget === event.relatedTarget || event.currentTarget.contains(event.relatedTarget)) {
                return; // it's dragging in the same element
            }

            this.renderer.removeClass(event.currentTarget, "_highlighted");

            if (this.branchDragOverTimer && draggedOverItem === this.branchDragOverTimer.treeItem) {
                clearTimeout(this.branchDragOverTimer.timeoutId);
                this.branchDragOverTimer = null;
            }
        }
    }

    /** @internal */
    public _dropHandler(dropEvent: any, targetItem: TreeMenuItem): void {
        if (this.draggedItem !== targetItem) {
            this.renderer.removeClass(dropEvent.currentTarget, "_highlighted");

            if (this.branchDragOverTimer && targetItem === this.branchDragOverTimer.treeItem) {
                clearTimeout(this.branchDragOverTimer.timeoutId);
                this.branchDragOverTimer = null;
            }

            this.onDrop.emit({
                targetItem,
                originalEvent: dropEvent
            });
        }

        this.draggedItem = null;
    }
}
