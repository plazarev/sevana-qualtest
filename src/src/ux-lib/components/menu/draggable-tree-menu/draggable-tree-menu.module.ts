import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {UxDraggableTreeMenuComponent} from "./draggable-tree-menu.component";
import {UxDraggableTreeSubMenuComponent} from "./sub-menu/draggable-tree-sub-menu.component";
import {UxDragDropModule} from "../../dragdrop/dragdrop.module";
import {UxTreeMenuModule} from "../tree-menu/tree-menu.module";

export {MenuItem} from "../model/menu-item.model";
export {MenuItemChangeEvent} from "../model/menu-item-change-event.model";
export {TreeMenuItemMoveEvent} from "./draggable-tree-menu.model";

@NgModule({
    imports: [SharedModule, UxDragDropModule, UxTreeMenuModule],
    exports: [UxDraggableTreeMenuComponent],
    declarations: [UxDraggableTreeMenuComponent, UxDraggableTreeSubMenuComponent]
})
export class UxDraggableTreeMenuModule {
}