/* deprecated use instead UxWizardStep. todo remove in 2.0 version */
export interface WizardStep {
    id?: string;
    name: string;
    isActive?: boolean;
    isPassed?: boolean;
    styleClass?: string;
}

export interface UxWizardStep extends WizardStep{}