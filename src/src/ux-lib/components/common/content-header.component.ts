import {Component} from "@angular/core";

@Component({
    selector: "ux-content-header",
    template: `<ng-content></ng-content>`,
    host: {"[class.ux-content-header]": "true"}
})
export class UxContentHeaderComponent {
}
