import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {UxSlideToggleDirective} from "./slide-toggle.directive";

@NgModule({
    imports: [SharedModule],
    exports: [UxSlideToggleDirective],
    declarations: [UxSlideToggleDirective],
})
export class UxSlideToggleModule {}