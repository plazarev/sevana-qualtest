export interface UxNumberFormatModel {
    precision?: number;
    thousandsDelimiter?: string;
    decimalDelimiter?: string;
    autoComplete?: boolean;
}
