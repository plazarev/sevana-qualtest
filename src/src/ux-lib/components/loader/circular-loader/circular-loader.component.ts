import {Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, Output, ViewChild} from "@angular/core";
import {UxAbstractPeriodicLoaderComponent} from "../abstract-periodic-loader.component";
import {UxPropertyConverter} from "../../../common/decorator/ux-property-converter";
import {UxPropertyHandler} from "../../../common/decorator/ux-property-handler";

@Component({
    selector: "ux-circular-loader",
    templateUrl: "./circular-loader.component.html",
    host: {"[class.ux-circular-loader]": "true"}
})
export class UxCircularLoaderComponent extends UxAbstractPeriodicLoaderComponent implements OnDestroy {

    @UxPropertyConverter("number", 3500)
    @UxPropertyHandler({
        afterChange: afterChangeDuration
    })
    @Input()
    public duration: number;

    @Output()
    public onComplete: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild("loader")
    private loader: ElementRef;

    private attachElement: HTMLElement;

    private alpha: number = 0;
    private timeout: number = 0;

    constructor(private element: ElementRef, private zone: NgZone) {
        super();
    }

    private showLoader(): void {
        let self = this;
        if (self.attachElement && self.element) {
            self.alpha = 0;
            clearTimeout(self.timeout);
            if (self.duration) {
                self.attachElement.appendChild(self.element.nativeElement);
                if (self.loader) {
                    self.loader.nativeElement.removeAttribute('d');
                }
                self.draw();
            }
        }
    }

    public drawLoader(element: HTMLElement): void {
        this.attachElement = element;
        this.showLoader();
    }

    private draw(): void {
        let self = this;
        self.alpha += 4;

        self.zone.runOutsideAngular(() => {
            let r = ( self.alpha * Math.PI / 180 ),
                x = Math.sin(r) * 9,
                y = Math.cos(r) * -9,
                mid = ( self.alpha >= 180 ) ? 1 : 0,
                animate = 'M 0 0 v -9 A 9 9 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';

            if (self.alpha < 360) {
                // Redraw
                self.timeout = window.setTimeout(() => {
                    self.draw();
                }, parseInt(self.duration / 90 + ''));
            } else {
                animate = "M 0 0 v -9 A 9 9 1 1 1 -.1 -9 z";

                self.zone.run(() => {
                    self.onComplete.emit();
                });
            }

            if (self.loader && self.duration) {
                self.loader.nativeElement.setAttribute('d', animate);
            }
        });
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    protected afterVisibilityChange(): void {
        let self = this;
        if (self.visible) {
            self.showLoader();
        } else {
            if (self.timeout) {
                clearTimeout(self.timeout);
                self.timeout = null;
            }
        }
    }
}

/*Helpers*/
export function afterChangeDuration(newValue: number, oldValue: number): void {
    if (newValue === 0) {
        clearTimeout(this.timeout);
    }
}
