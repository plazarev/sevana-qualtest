import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {UxCircularLoaderComponent} from "./circular-loader.component";

@NgModule({
    imports: [SharedModule],
    exports: [UxCircularLoaderComponent],
    declarations: [UxCircularLoaderComponent,]
})
export class UxCircularLoaderModule {
}