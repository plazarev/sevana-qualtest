import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {UxBagelLoaderComponent} from "./bagel-loader.component";

@NgModule({
    imports: [SharedModule],
    exports: [UxBagelLoaderComponent],
    declarations: [UxBagelLoaderComponent,]
})
export class UxBagelLoaderModule {
}