import {
    Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, NgZone
} from "@angular/core";
import {UxAbstractPeriodicLoaderComponent} from "../abstract-periodic-loader.component";
import {UxPropertyConverter} from "../../../common/decorator/ux-property-converter";
import {UxPropertyHandler} from "../../../common/decorator/ux-property-handler";

@Component({
    selector: "ux-bagel-loader",
    templateUrl: "./bagel-loader.component.html",
    host: {"[class.ux-bagel-loader]": "true"}
})
export class UxBagelLoaderComponent extends UxAbstractPeriodicLoaderComponent implements OnInit, OnDestroy {
    @Input()
    @UxPropertyConverter("number", 0)
    @UxPropertyHandler({
        afterChange: afterProgressChange
    })
    public progress: number;

    @Input()
    @UxPropertyConverter("number", 0)
    @UxPropertyHandler({
        afterChange: afterChangeDuration
    })
    public duration: number;

    @Output()
    public onComplete: EventEmitter<boolean> = new EventEmitter<boolean>();

    private _internalProgress: number;

    private set internalProgress(value: number) {
        this._internalProgress = value;
        this.updateInternalProgress();
    }

    private get internalProgress(): number {
        return this._internalProgress;
    }

    private loaderDrawingIntervalId: number;

    @ViewChild("circle")
    @UxPropertyHandler({
        afterChange: afterChangeCircleRef
    })
    private circleRef: ElementRef;

    private circleElement: HTMLElement;

    ngOnInit() {
        this.internalProgress = 0;
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this.loaderDrawingIntervalId) {
            clearInterval(this.loaderDrawingIntervalId);
        }
    }

    private updateInternalProgress(): void {
        let self = this;

        if (self.circleElement) {
            self.circleElement.setAttribute("stroke-dashoffset", 62.8 * (100 - self._internalProgress)/100 + "");
        }
    }

    public startLoader(): void {
        let self = this;

        if (!self.duration) {
            return;
        }

        if (self.loaderDrawingIntervalId) {
            clearInterval(self.loaderDrawingIntervalId);
        }

        self.internalProgress = 0;

        let timeInterval = (self.duration / 100 < 100) ? 100 : self.duration / 100;

        self.zone.runOutsideAngular(() => {
            self.loaderDrawingIntervalId = window.setInterval(() => {
                self.internalProgress += 100 * timeInterval / self.duration;

                if (self.internalProgress >= 100) {
                    self.internalProgress = 100;
                    clearInterval(self.loaderDrawingIntervalId);
                    self.loaderDrawingIntervalId = undefined;

                    self.zone.run(() => {
                        self.onComplete.emit(true);
                    });
                }
            }, timeInterval);
        });
    }

    public stopLoader(): void {
        if (this.loaderDrawingIntervalId) {
            clearInterval(this.loaderDrawingIntervalId);
            this.loaderDrawingIntervalId = undefined;
        }
    }

    constructor(private zone: NgZone) {
        super();
    }

    protected afterVisibilityChange(): void {
        if (this.visible) {
            this.startLoader();
        } else {
            this.stopLoader();
        }
    }
}

export function afterProgressChange(newValue: number): void {
    if (this.loaderDrawingIntervalId) {
        return;
    }

    if (newValue < 0) {
        newValue = 0;
    }

    if (newValue == 100) {
        this.onComplete.emit(true);
    }

    if (newValue > 100) {
        newValue = 100;
    }

    this.internalProgress = newValue;
}

export function afterChangeDuration(newValue: number): void {
    if (newValue === 0) {
        clearInterval(this.loaderDrawingIntervalId);
        this.loaderDrawingIntervalId = undefined;
    }
}

export function afterChangeCircleRef(): void {
    if (this.circleRef) {
        this.circleElement = this.circleRef.nativeElement;
    }
}
