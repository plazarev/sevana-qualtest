import {UxTabPanelItem} from "./item/tab-panel-item.component";

/* deprecated use instead UxTabChangeEvent. Will be removed in 2.0 */
export interface TabChangeEvent {
    originalEvent: Event;
    index: number;
    selectedTab: UxTabPanelItem;
}

export interface UxTabChangeEvent extends TabChangeEvent {

}
