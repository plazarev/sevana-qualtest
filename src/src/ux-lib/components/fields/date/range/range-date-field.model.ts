export interface UxRangeDateFieldModel {
    from?: Date | null;
    to?: Date | null;
    fromInputName?: string;
    toInputName?: string;
    /** Deprecated, will be removed in 3.0 and remove from template range-date-field.component.html
     value?.toInputPlaceholder and value?.fromInputPlaceholder in placeholder **/
    fromInputPlaceholder?: string;
    toInputPlaceholder?: string;
    /** ******************************************************* **/
    selectDateRangeText?: string;
}

export interface UxRangeDateFieldViewEventModel {
    from?: string;
    to?: string;
}

export interface UxRangeDateFieldPlaceholderModel {
    from?: string;
    to? : string;
}
