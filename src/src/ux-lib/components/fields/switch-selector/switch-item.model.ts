export interface UxSwitchItemModel {
  id?: string;
  label?: string;
  name?: string;
  value?: any;
  disabled?: boolean;
}
