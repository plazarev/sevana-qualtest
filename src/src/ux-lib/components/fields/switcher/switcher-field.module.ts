import {NgModule} from "@angular/core";
import {SharedModule} from "../../../shared/shared.module";
import {UxSwitcherFieldComponent} from "./switcher-field.component";

@NgModule({
    imports: [SharedModule],
    exports: [UxSwitcherFieldComponent],
    declarations: [UxSwitcherFieldComponent]
})
export class UxSwitcherFieldModule {
}