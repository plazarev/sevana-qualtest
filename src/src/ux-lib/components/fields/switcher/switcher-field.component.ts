import {Component, Input} from "@angular/core";
import {UxAbstractBooleanFieldComponent} from "../abstract-boolean-field.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: "ux-switcher-field",
    templateUrl: "./switcher-field.component.html",
    host: {"[class.ux-switcher-field]": "true"},
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: UxSwitcherFieldComponent,
            multi: true
        }
    ]
})
export class UxSwitcherFieldComponent extends UxAbstractBooleanFieldComponent {

    @Input()
    public leftLabel: string;
}
