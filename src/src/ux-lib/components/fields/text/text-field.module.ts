import {NgModule} from "@angular/core";
import {UxTextFieldComponent} from "./text-field.component";
import {SharedModule} from "../../../shared/shared.module";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [SharedModule, FormsModule],
    exports: [UxTextFieldComponent],
    declarations: [UxTextFieldComponent]
})
export class UxTextFieldModule {
}