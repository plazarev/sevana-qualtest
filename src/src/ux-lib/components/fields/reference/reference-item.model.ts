import {UxMultipleFieldItemModel} from "../multiple/multiple-field-item.model";
import {MenuItem} from "../../menu/model/menu-item.model";

/* deprecated use instead UxReferenceSelectorItem. todo remove in 2.0 version */
export interface ReferenceSelectorItem extends UxMultipleFieldItemModel {
    type?: string;
    menuItems?: MenuItem[];
    customData?: any;
}

export interface UxReferenceSelectorItem extends ReferenceSelectorItem{

}