import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {Component, NO_ERRORS_SCHEMA, TemplateRef, ViewChild} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {UxScrollModule} from "../../scroll/scroll.module";
import {UxFieldSuggestionPopupModule} from "../suggestion-popup/field-suggestion-popup.module";
import {UxReferenceFieldComponent} from "./reference-field.component";
import {UxPopoverModule} from "../../popover/popover.module";
import {UxRoundDotsLoaderModule} from "../../loader/round-dots-loader/round-dots-loader.module";
import {UxMenuModule} from "../../menu/menu.module";
import {ReferenceSelectorItem} from "./reference-item.model";
import {By} from "@angular/platform-browser";
import {MenuItem} from "../../menu/model/menu-item.model";

@Component({
    selector: "ux-reference-field-test",
    template: `
        <ng-template #customTemplate>
            Template Message
        </ng-template>
    `
})
class UxReferenceFieldTestComponent {
    @ViewChild("customTemplate")
    public customTemplate: TemplateRef<any>;
}

describe("UxReferenceFieldComponent", () => {

    let fixture: ComponentFixture<UxReferenceFieldTestComponent>,
        fixtureReferenceField: ComponentFixture<UxReferenceFieldComponent>,
        componentTest: UxReferenceFieldTestComponent,
        componentReferenceField: UxReferenceFieldComponent,
        element;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [UxRoundDotsLoaderModule,
                UxFieldSuggestionPopupModule,
                UxScrollModule,
                UxPopoverModule,
                UxMenuModule,
                FormsModule],
            declarations: [UxReferenceFieldComponent, UxReferenceFieldTestComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: []
        });

        fixture = TestBed.createComponent(UxReferenceFieldTestComponent);
        fixtureReferenceField = TestBed.createComponent(UxReferenceFieldComponent);

        componentTest = fixture.componentInstance;
        componentReferenceField = fixtureReferenceField.componentInstance;
        fixture.detectChanges();
    });

    let menuItems: MenuItem[] = [
        {caption: 'Edit'},
        {caption: 'Copy', disabled: true},
        {caption: 'Remove', id: 'remove'},
        {caption: 'Go to', disabled: true}
    ];

    let items: ReferenceSelectorItem[] = [{
        id: "1",
        caption: "Page Location join",
        type: "token",
        menuItems: menuItems
    }, {
        id: "2",
        caption: "Printer M013"
    }, {
        id: "3",
        caption: "item 3"
    }, {
        id: "5",
        caption: "long long long other"
    }, {
        id: "6",
        caption: "qwertyuiop"
    }, {
        id: "7",
        caption: "wasdwasdwasd"
    }];

    //inputs
    it("UxReferenceFieldComponent should have #showDropdownButton, #multiple, #isLoading, #onlyExistingItems," +
        "#delay, #placeholder, #itemCaptionKey, #emptyMessage, #maxListHeight, #optionsPopoverVisible, #value, #mobile, " +
        "#disabled property", () => {
        componentReferenceField.items = items;
        expect(Array.isArray(componentReferenceField.items)).toBe(true);
        expect(typeof componentReferenceField.showDropdownButton).toBe("boolean");
        expect(typeof componentReferenceField.multiple).toBe("boolean");
        expect(typeof componentReferenceField.isLoading).toBe("boolean");
        expect(typeof componentReferenceField.onlyExistingItems).toBe("boolean");
        expect(typeof componentReferenceField.delay).toBe("number");
        expect(typeof componentReferenceField.itemCaptionKey).toBe("string");
        expect(typeof componentReferenceField.emptyMessage).toBe("string");
        expect(typeof componentReferenceField.maxListHeight).toBe("string");
        expect(typeof componentReferenceField.optionsPopoverVisible).toBe("boolean");


        componentReferenceField.opened = false;
        expect(typeof componentReferenceField.opened).toBe("boolean");

        expect(typeof componentReferenceField.placeholder).toBe("string");
        expect(typeof componentReferenceField.inputName).toBe("string");
        expect(typeof componentReferenceField.disabled).toBe("boolean");
        expect(typeof componentReferenceField.mobile).toBe("boolean");

        componentReferenceField.listContainer = ".ux-test-style-container";
        expect(typeof componentReferenceField.listContainer).toBe("string");

        componentReferenceField.listStyleClass = "ux-test-style-list";
        expect(typeof componentReferenceField.listStyleClass).toBe("string");

        componentReferenceField.value = [].concat(items[0]);
        expect(typeof componentReferenceField.value).toBe("object");
    });

    //templates
    it("UxReferenceFieldComponent. Change #itemTemplate input", () => {
        componentReferenceField.itemTemplate = componentTest.customTemplate;
        componentReferenceField.items = items;
        componentReferenceField.opened = true;

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__suggestions-popup"));
        expect(element).not.toBeNull("ux-reference-field__suggestions-popup should be found");

        expect(element.nativeElement.innerHTML).toContain("Template Message");
    });

    it("UxReferenceFieldComponent. Change #valueItemTemplate input", () => {
        componentReferenceField.valueItemTemplate = componentTest.customTemplate;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[0]);

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-item"));
        expect(element).not.toBeNull("ux-reference-field__value-item should be found");

        expect(element.nativeElement.innerHTML).toContain("Template Message");
    });

    it("UxReferenceFieldComponent. Change #itemTemplate input", () => {
        componentReferenceField.itemTemplate = componentTest.customTemplate;
        componentReferenceField.items = items;
        componentReferenceField.opened = true;

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__suggestions-popup"));
        expect(element).not.toBeNull("ux-reference-field__suggestions-popup should be found");

        expect(element.nativeElement.innerHTML).toContain("Template Message");
    });

    it("UxReferenceFieldComponent. Change #toolbarTemplate input", () => {
        componentReferenceField.toolbarTemplate = componentTest.customTemplate;
        componentReferenceField.opened = true;

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-field-suggestion-popup__inner"));
        expect(element).not.toBeNull("ux-field-suggestion-popup__inner should be found");

        expect(element.nativeElement.innerHTML).toContain("Template Message");
    });

    it("UxReferenceFieldComponent. Change #valueItemOptionsTemplate input and check #optionsPopoverVisibleChange() output", () => {
        let optionsPopoverVisibleChanged: boolean = false;
        componentReferenceField.valueItemOptionsTemplate = componentTest.customTemplate;
        componentReferenceField.opened = true;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[0]);

        componentReferenceField.optionsPopoverVisibleChange.subscribe((event: boolean) => {
            optionsPopoverVisibleChanged = event;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-item-arrow"));
        expect(element).not.toBeNull("ux-reference-field__value-item-arrow should be found");
        element.triggerEventHandler("tap", {
            stopPropagation: function () {
            }
        });

        expect(optionsPopoverVisibleChanged).toBe(true, "Case 'Open options popover'. optionsPopoverVisibleChanged should be true");

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__options-popover"));
        expect(element).not.toBeNull("ux-reference-field__options-popover should be found");

        expect(element.nativeElement.innerHTML).toContain("Template Message");
    });

    it("UxReferenceFieldComponent. Check #onValueItemOptionSelect() output", () => {
        let valueItemOptionSelected: boolean = false;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[0]);

        componentReferenceField.onValueItemOptionSelect.subscribe((event: any) => {
            valueItemOptionSelected = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-item-arrow"));
        expect(element).not.toBeNull("ux-reference-field__value-item-arrow should be found");
        element.triggerEventHandler("tap", {
            stopPropagation: function () {
            }
        });

        fixtureReferenceField.detectChanges();

        //tap on option in popup
        element = fixtureReferenceField.debugElement.query(By.css(".ux-menu__item"));
        expect(element).not.toBeNull("ux-menu__item should be found");

        element.triggerEventHandler("tap", {
            stopPropagation: function () {
            }
        });
        expect(valueItemOptionSelected).toBe(true, "valueItemOptionSelected should be true");
    });


    //outputs
    it("UxReferenceFieldComponent should have #openedChange() and #onDropdownClick() property", async(() => {
        let openedChanged: boolean = false,
            dropdownButtonClicked: boolean = false;
        componentReferenceField.items = items;

        componentReferenceField.openedChange.subscribe((event: boolean) => {
            openedChanged = event;
        });
        componentReferenceField.onDropdownClick.subscribe((event: Event) => {
            dropdownButtonClicked = true;
        });

        /******************************/
        //openedChange. Change #opened property
        componentReferenceField.opened = true;
        expect(openedChanged).toBe(true, "Case 'Change #opened property'. openedEvent should be true");

        /******************************/
        //openedChange. Open popup by click on dropdown button
        openedChanged = false;
        fixtureReferenceField.detectChanges();
        componentReferenceField.opened = false;
        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__dropdown-button"));
        expect(element).not.toBeNull("ux-reference-field__dropdown-button should be found");

        element.triggerEventHandler("tap", {});

        setTimeout(() => {
            fixture.whenStable().then(() => {
                expect(dropdownButtonClicked).toBe(true, "Case 'Click on dropdown button'. dropdownButtonClicked should be true");
                expect(openedChanged).toBe(true, "Case 'Open popup by click on dropdown button'. openedEvent should be true");
            });
        }, 100);
    }));


    it("UxReferenceFieldComponent should have #onSelect() property", () => {
        let selected: boolean = false;
        componentReferenceField.items = items;
        componentReferenceField.opened = true;

        componentReferenceField.onSelect.subscribe((event: ReferenceSelectorItem) => {
            selected = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__suggestion-item"));
        expect(element).not.toBeNull("ux-reference-field__suggestion-item should be found");

        element.triggerEventHandler("tap", items[0]);

        expect(selected).toBe(true, "Selected should be true");
    });

    it("UxReferenceFieldComponent should have #onUnselect() property", () => {
        let unselected: boolean = false;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[1]);

        componentReferenceField.onUnselect.subscribe((event: ReferenceSelectorItem) => {
            unselected = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-item-remove"));
        expect(element).not.toBeNull("ux-reference-field__value-item-remove should be found");

        element.triggerEventHandler("tap", items[1]);

        expect(unselected).toBe(true, "Unselected should be true");
    });


    it("UxReferenceFieldComponent should have #onSelectedItemClick() property", () => {
        let selected: boolean = false;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[0]);

        componentReferenceField.onSelectedItemClick.subscribe((event: ReferenceSelectorItem) => {
            selected = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-caption"));
        expect(element).not.toBeNull("ux-reference-field__value-caption should be found");

        element.triggerEventHandler("tap", items[0]);

        expect(selected).toBe(true, "Selected should be true");
    });

    it("UxReferenceFieldComponent should have #onPopupClick() property", () => {
        let clicked: boolean = false;
        componentReferenceField.items = items;
        componentReferenceField.opened = true;

        componentReferenceField.onPopupClick.subscribe((event: ReferenceSelectorItem) => {
            clicked = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-field-suggestion-popup__inner"));
        expect(element).not.toBeNull("ux-field-suggestion-popup__inner should be found");

        element.triggerEventHandler("tap", items[0]);

        expect(clicked).toBe(true, "Clicked should be true");
    });

    it("UxReferenceFieldComponent should have #onKeyDown() property", () => {
        let pressed: boolean = false;

        componentReferenceField.onKeyDown.subscribe((event: any) => {
            pressed = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__input"));
        expect(element).not.toBeNull("ux-reference-field__input should be found");

        element.triggerEventHandler("keydown", {
            which: 32//space
        });

        expect(pressed).toBe(true, "pressed should be true");
    });

    it("UxReferenceFieldComponent should have #onSearchValueChange() property", () => {
        let changed: boolean = false;
        componentReferenceField.opened = true;

        componentReferenceField.onSearchValueChange.subscribe((event: any) => {
            changed = true;
        });

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__input"));
        expect(element).not.toBeNull("ux-reference-field__input should be found");

        element.nativeElement.value = "a";
        let evt = document.createEvent('Event');
        evt.initEvent('input', true, false);
        element.nativeElement.dispatchEvent(evt);

        element.triggerEventHandler("keydown", {
            which: 13,//enter
            preventDefault: () => {
            }
        });
        element.triggerEventHandler("keyup", {
            which: 13 //enter
        });

        expect(changed).toBe(true, "changed should be true");
    });

    //Change inputs
    it("UxReferenceFieldComponent. Change #emptyMessage property", () => {
        componentReferenceField.emptyMessage = "Test Empty Message";
        componentReferenceField.opened = true;

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__empty-message"));
        expect(element).not.toBeNull("ux-reference-field__empty-message should be found");

        expect(element.nativeElement.innerHTML).toContain("Test Empty Message");
    });

    it("UxReferenceFieldComponent. Change #showDropdownButton property", () => {
        componentReferenceField.showDropdownButton = false;

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__dropdown-button"));
        expect(element).not.toBeNull("ux-reference-field__dropdown-button should be found");

        expect(element.classes["_show-dropdown"]).toBe(false);
    });

    it("UxReferenceFieldComponent. Change #isLoading property", async(() => {
        componentReferenceField.isLoading = true;

        fixtureReferenceField.detectChanges();

        expect(fixtureReferenceField.debugElement.classes["ux-reference-field"]).toBe(true);

        setTimeout(() => {
            fixture.whenStable().then(() => {
                fixtureReferenceField.detectChanges();//detectChanges after change _visible variable
                element = fixtureReferenceField.debugElement.query(By.css(".ux-round-dots-loader__inner"));
                expect(element).not.toBeNull("ux-round-dots-loader__inner should be found");
            });
        }, 100);
    }));

    it("UxReferenceFieldComponent. Change #placeholder property", () => {
        componentReferenceField.placeholder = "Test Message";

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__input"));
        expect(element).not.toBeNull("ux-reference-field__input should be found");

        expect(element.nativeElement.placeholder).toBe("Test Message");
    });

    it("UxReferenceFieldComponent. Change #itemCaptionKey property", () => {
        componentReferenceField.itemCaptionKey = "id";
        componentReferenceField.items = items;
        componentReferenceField.opened = true;
        componentReferenceField.multiple = true;
        componentReferenceField.value = [].concat(items[0]);

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__value-item"));
        expect(element).not.toBeNull("ux-reference-field__value-item should be found");
        expect(element.nativeElement.innerHTML).toContain("1");

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__suggestions-list"));
        expect(element).not.toBeNull("ux-reference-field__suggestions-list should be found");
        expect(element.nativeElement.innerHTML).toContain("7");
    });

    it("UxReferenceFieldComponent. Change #listContainer property", () => {
        componentReferenceField.items = items;
        componentReferenceField.listContainer = "body";
        fixtureReferenceField.detectChanges();
        componentReferenceField.opened = true;
        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css("ux-field-suggestion-popup"));
        expect(element).not.toBeNull("ux-field-suggestion-popup should be found");

        expect(element.nativeElement.innerHTML).not.toContain("item 3");
        expect(element.nativeElement.innerHTML).not.toContain("1");
    });

    it("UxReferenceFieldComponent. Change #listStyleClass property", () => {
        componentReferenceField.items = items;
        componentReferenceField.listStyleClass = "ux-test-style-class";
        componentReferenceField.opened = true;
        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-field-suggestion-popup__inner"));
        expect(element).not.toBeNull("ux-field-suggestion-popup__inner should be found");

        expect(element.classes["ux-test-style-class"]).toBe(true);
    });

    it("UxReferenceFieldComponent. Change #inputName property", () => {
        componentReferenceField.inputName = "test-input";

        fixtureReferenceField.detectChanges();

        element = fixtureReferenceField.debugElement.query(By.css(".ux-reference-field__input"));
        expect(element).not.toBeNull("ux-reference-field__input should be found");

        expect(element.nativeElement.name).toBe("test-input");
    });

});
