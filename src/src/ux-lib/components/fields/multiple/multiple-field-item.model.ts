export interface UxMultipleFieldItemModel {
    id?: string;
    caption?: string;
    selected?: boolean;

    styleClass?: string;
}
