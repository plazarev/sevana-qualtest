import {ComponentFixture, ComponentFixtureAutoDetect, TestBed} from "@angular/core/testing";
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";
import {By} from "@angular/platform-browser";
import {UxToolbarComponent} from "./toolbar.component";
import {UxToolbarLeftComponent} from "./parts/toolbar-left.component";
import {UxToolbarRightComponent} from "./parts/toolbar-right.component";

@Component({
    selector: "ux-accordion-test",
    template: `
        <ux-toolbar>
            <ux-toolbar-left>
                left toolbar
            </ux-toolbar-left>
            <ux-toolbar-right>
                right toolbar
            </ux-toolbar-right>
        </ux-toolbar>
    `
})
class UxToolbarTestComponent {}

describe("UxToolbarComponent", () => {

    let fixture:   ComponentFixture<UxToolbarTestComponent>,
        fixtureToolbar:   ComponentFixture<UxToolbarComponent>,
        fixtureToolbarLeft:   ComponentFixture<UxToolbarLeftComponent>,
        fixtureToolbarRight:   ComponentFixture<UxToolbarRightComponent>,
        component: UxToolbarTestComponent,
        componentToolbar: UxToolbarComponent,
        element;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ UxToolbarTestComponent, UxToolbarComponent, UxToolbarLeftComponent, UxToolbarRightComponent],
            schemas:      [ NO_ERRORS_SCHEMA ],
            providers: []
        });

        fixture = TestBed.createComponent(UxToolbarTestComponent);
        fixtureToolbar = TestBed.createComponent(UxToolbarComponent);
        fixtureToolbarLeft = TestBed.createComponent(UxToolbarLeftComponent);
        fixtureToolbarRight = TestBed.createComponent(UxToolbarRightComponent);

        component = fixture.componentInstance;
        componentToolbar = fixtureToolbar.componentInstance;
        element = fixture.nativeElement;
        fixture.detectChanges();
        fixtureToolbar.detectChanges();
    });

    it("UxToolbar Should have #mobile property", () => {
        expect(typeof componentToolbar.mobile).toBe("boolean");
    });
    
    it("UxToolbar should correctly project ux-toolbar-left with content", () => {
        expect(
            fixture.debugElement.query(By.css("ux-toolbar")).query(By.css("ux-toolbar-left"))
                .nativeElement.innerHTML
        ).toContain("left toolbar");
    });

    it("UxToolbar should correctly project ux-toolbar-right with content", () => {
        expect(
            fixture.debugElement.query(By.css("ux-toolbar")).query(By.css("ux-toolbar-right"))
                .nativeElement.innerHTML
        ).toContain("right toolbar");
    });
});
