import {Component} from "@angular/core";

@Component({
    selector: "ux-toolbar-right",
    template: `<ng-content></ng-content>`,
    host: { "[class.ux-toolbar-right]": "true" }
})
export class UxToolbarRightComponent {
}
