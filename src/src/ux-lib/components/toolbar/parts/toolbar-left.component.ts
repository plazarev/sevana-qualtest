import {Component} from "@angular/core";

@Component({
    selector: "ux-toolbar-left",
    template: `<ng-content></ng-content>`,
    host: { "[class.ux-toolbar-left]": "true" }
})
export class UxToolbarLeftComponent {
}
