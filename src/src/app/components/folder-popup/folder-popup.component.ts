import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef, ViewChild
} from "@angular/core";
import {UxPropertyConverter} from "../../../ux-lib/common/decorator/ux-property-converter";
import {UxPropertyHandler} from "../../../ux-lib/common/decorator/ux-property-handler";
import {Subject, Subscription} from "rxjs/index";
import {takeUntil, tap} from "rxjs/internal/operators";
import {PostOperationResult, PutOperationResult} from "../../common/services/entity.service";
import {PopupComponent} from "../base/popup.component";
import {ModelService} from "../../common/services/model.service";
import {UxDropdownListItem} from "../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {DeviceEntity, UpdateDeviceEntity} from "../../common/models/entity/device-entity.model";
import {FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {DevicesService} from "../../modules/devices/devices.service";
import {DictionaryService} from "../../common/services/dictionary.service";
import {DeviceLinkedTestComponent} from "../../modules/devices/components/device-linked-test.component";
import {DictionaryType} from "../../common/models/entity/dictionary-type.model";
import {FolderPopupEvent} from "./folder-popup.event";

@Component({
  selector: "sq-folder-popup",
  templateUrl: "folder-popup.component.html",
  host: {"[class.sq-folder-popup]": "true"}
})
export class FolderPopupComponent<T> extends PopupComponent<T> {

  private _formGroup: FormGroup;

  private stopSubscription$ = new Subject<boolean>();

  @Input()
  public userData: T;

  private _folderName: string;
  private _oldFolderName: string;
  @Input()
  public set folderName(value: string) {
    this._oldFolderName = value;
    this._folderName = value;

    if (value) {
      this._formGroup.patchValue({
        "folderNameCtrl": this._folderName
      });
    }
    else {
      this.resetFormData();
    }
  }

  public get folderName(): string {
    return this._folderName;
  }

  @Input()
  operationStatus: string;

  private _creatingMode: boolean = false;
  @Input()
  public set creatingMode(value: boolean) {
    this._creatingMode = value;
    if (this._creatingMode) {
      this.folderName = "";
    }
  }

  public get creatingMode(): boolean {
    return this._creatingMode;
  }

  @Output()
  public onAccepted: EventEmitter<FolderPopupEvent<T>> = new EventEmitter<FolderPopupEvent<T>>();

  constructor(private formBuilder: FormBuilder,
              private cdRef: ChangeDetectorRef) {
    super();
  }

  public initComponent() {
    super.initComponent();
    this.initForm();
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  public getUserData(): T {
    return this.userData;
  }

  public _getErrorText(controlName: string): string {
    let errorText = "",
      control = this._formGroup.controls[controlName];

    if (control.errors) {
      errorText = "!";
      let errors: ValidationErrors = control.errors;

      if (controlName === "folderNameCtrl") {
        if (errors["required"] !== undefined) {
          errorText = "Folder name is required";
        }
      }
    }
    return errorText;
  }

  _onEditConfirm() {
    this.onAccepted.emit({
      userData: this.userData,
      oldFolderName: this._oldFolderName,
      newFolderName: this._folderName
    });
  }

  protected _close() {
    this.resetFormData();
    super._close();
  }

  private initForm(): void {
    this._formGroup = this.formBuilder.group({
      "folderNameCtrl": ["", Validators.compose([Validators.required])]
    });
  }

  private resetFormData() {
    this._formGroup.updateValueAndValidity();
    this._formGroup.reset();
    this._formGroup.markAsUntouched();
  }
}
