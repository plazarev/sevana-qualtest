import {UxCardTreeNodeModel} from "../../../ux-lib/components/card-tree/card-tree.model";
import {Subscription} from "rxjs/index";
import {
  StatisticsDataEntry, StatisticsDataStyledVisualEntry, StatisticsEntity,
  StatisticsEntityBase
} from "../../common/models/entity/statistics-entity.model";
import {StatisticDataType} from "../../common/models/statistic-data.type";
import {StatisticPeriodType} from "../../common/models/statistic-period.type";

export interface FolderPopupEvent<T> {
  userData: T;
  oldFolderName: string;
  newFolderName: string;
}

