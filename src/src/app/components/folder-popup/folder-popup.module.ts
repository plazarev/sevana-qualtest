import {NgModule} from "@angular/core";
import {FolderPopupComponent} from "./folder-popup.component";
import {UxOverlayModule} from "../../../ux-lib/components/overlay/overlay.module";
import {CommonModule} from "@angular/common";
import {UxButtonModule} from "../../../ux-lib/components/button/button.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {OverlayPopupModule} from "../overlay-popup/overlay-popup.module";
import {UxLabelModule} from "../../../ux-lib/components/label/label.module";
import {UxTextFieldModule} from "../../../ux-lib/components/fields/text/text-field.module";
import {UxTooltipModule} from "../../../ux-lib/components/tooltip/tooltip.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UxButtonModule,
    UxLabelModule,
    UxTextFieldModule,
    UxTooltipModule,
    OverlayPopupModule
  ],
  declarations: [
    FolderPopupComponent
  ],
  exports: [
    FolderPopupComponent
  ]
})
export class FolderPopupModule {
}
