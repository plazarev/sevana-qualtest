import {
  Component, EventEmitter, Input, IterableChangeRecord, IterableDiffer, IterableDiffers, KeyValueChangeRecord,
  KeyValueDiffer,
  KeyValueDiffers, OnDestroy,
  OnInit, Output,
} from "@angular/core";
import {Progress} from "../../common/models/progress.model";
import {UxCardTreeModel} from "../../../ux-lib/components/card-tree/card-tree.model";
import {UxPageChangeEvent} from "../../../ux-lib/components/paging/paging.component";
import {CardDataModel} from "./card-data.model";
import {UxCardTreeCheckedEvent} from "../../../ux-lib/components/card-tree/card-tree.component";
import {ModelService} from "../../common/services/model.service";
import {Subject} from "rxjs/index";
import {
  StatisticsDataStyledVisualEntry,
  StatisticsEntityBase
} from "../../common/models/entity/statistics-entity.model";
import {CssService} from "../../common/services/css.service";
import {Entity} from "../../common/models/entity/entity.model";
import {DefaultKeyValueDiffer} from "@angular/core/src/change_detection/differs/default_keyvalue_differ";

@Component({
  template: ""
})
export class CardListComponent<T extends Entity> implements OnInit, OnDestroy {
  protected _cardsModel: UxCardTreeModel;
  protected _entityCards: CardDataModel<T>[] = [];
  protected _filteredCards: CardDataModel<T>[] = [];
  protected _cardsTree: CardDataModel<T>[] = [];
  protected _pagingVisible: boolean = true;
  protected _viewInitiated: boolean = false;

  @Input()
  public pagingEnabled: boolean = true;

  protected _totalCount: number;
  @Input()
  public set totalCount(value: number) {
    this._totalCount = value;
    this.updatePagingVisibility();
  }

  public get totalCount(): number {
    return this._totalCount;
  }

  @Input()
  public pageSize: number;

  protected _pageSizeOptions: number[];
  @Input()
  public set pageSizeOptions(values: number[]) {
    this._pageSizeOptions = values;
    this.updatePagingVisibility();
  }

  public get pageSizeOptions(): number[] {
    return this._pageSizeOptions;
  }

  @Input()
  public currentPage: number;

  protected stopSubscription$ = new Subject<boolean>();

  @Input()
  public loadStatus: Progress = 'loading';

  @Input()
  public cardLoadStatus: Progress = 'loading';

  @Input()
  public selectAllEnabled: boolean = false;

  @Input()
  public selectAllCaption: string;

  protected _selectItemEnabled: boolean = false;
  @Input()
  public set selectItemEnabled(value: boolean) {
    this._selectItemEnabled = value;
    this.updateCardsSelection();
  }

  public get selectItemEnabled() {
    return this._selectItemEnabled;
  }

  protected _entities: T[] = [];
  protected _entitiesDiffers: IterableDiffer<T>;
  protected _entityDiffersMap = new Map<string, KeyValueDiffer<string, any>>();
  protected _entitiesMap = new Map<string, T>();

  @Input()
  public set entities(values: T[]) {
    this._entities = values;
    if (this._viewInitiated) {
      this.updateCards();
    }

    this.initNgCheck();
  }

  public get entities(): T[] {
    return this._entities;
  }

  private _filteredEntities: string[];
  @Input()
  public set filteredEntities(values: string[]) {
    this._filteredEntities = values;
    if (this._viewInitiated) {
      this.filterCards();
    }
  }

  public get filteredEntities(): string[] {
    return this._filteredEntities;
  }


  private _entitySummaryVisualStatistics: Map<string, StatisticsEntityBase<StatisticsDataStyledVisualEntry>>;
  @Input()
  public set entitySummaryVisualStatistics(values: Map<string, StatisticsEntityBase<StatisticsDataStyledVisualEntry>>) {
    this._entitySummaryVisualStatistics = values;
    this.updateCardsVisualStatistics();
  }

  public get entitySummaryVisualStatistics(): Map<string, StatisticsEntityBase<StatisticsDataStyledVisualEntry>> {
    return this._entitySummaryVisualStatistics;
  }

  protected _selectedEntities: string[] = [];
  @Input()
  public set selectedEntities(value: string[]) {
    this._selectedEntities = value || [];
    this.updateCardsSelection();
  }

  public get selectedEntities(): string[] {
    return this._selectedEntities;
  }

  @Input()
  public statisticsEnabled: boolean = false;

  @Input()
  public editEnabled: boolean = true;

  @Input()
  public deleteEnabled: boolean = true;

  @Input()
  public unlinkEnabled: boolean = false;

  @Input()
  public chartEnabled: boolean = true;

  @Input()
  public addEnabled: boolean = true;

  @Input()
  public expandEnabled: boolean = true;

  @Output()
  public onPageChange: EventEmitter<UxPageChangeEvent> = new EventEmitter<UxPageChangeEvent>();

  @Output()
  public onPageSizeChange: EventEmitter<UxPageChangeEvent> = new EventEmitter<UxPageChangeEvent>();

  @Output()
  public onEditCard: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onDeleteCard: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onUnlinkCard: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onChartCard: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onAddFolder: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onAddItem: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onExpandCard: EventEmitter<CardDataModel<T>> = new EventEmitter<CardDataModel<T>>();

  @Output()
  public onCardChecked: EventEmitter<UxCardTreeCheckedEvent> = new EventEmitter<UxCardTreeCheckedEvent>();

  @Output()
  public onCardCheckedAll: EventEmitter<UxCardTreeCheckedEvent> = new EventEmitter<UxCardTreeCheckedEvent>();

  @Output()
  public onLoadInitialData: EventEmitter<any> = new EventEmitter<any>();

  public isViewInitiated(): boolean {
    return this._viewInitiated;
  }

  constructor(protected modelService: ModelService,
              protected iterableDiffers: IterableDiffers,
              protected keyValueDiffers: KeyValueDiffers) {
  }

  public loadInitialData() {
    this.onLoadInitialData.emit();
  }

  ngOnInit(): void {
    this.initCardModel();
    this.loadInitialData();
    this._viewInitiated = true;
  }

  ngOnDestroy(): void {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  protected getEntityID(entity: T): string {
    return null;
  }

  protected getEntityIDField(entity: T): string {
    return null;
  }

  protected compareCardToEntity(entity: T, card: CardDataModel<T>): boolean {
    return false;
  }

  protected updateEntityCard(entity: T, card: CardDataModel<T>) {
  }

  protected isEntitySelected(entity: T): boolean {
    return false;
  }

  protected getCard(test: T, selected?: boolean): CardDataModel<T> {
    return null;
  }

  protected getDefaultCardModel(): UxCardTreeModel {
    return {
      children: []
    }
  }

  protected onCardExpanded(enityCard: CardDataModel<T>) {
  }

  private initNgCheck(): void {
    if (this._entities) {
      this._entitiesDiffers = this.iterableDiffers.find(this._entities).create(this._trackEntityByFn);

      this._entityDiffersMap.clear();
      this._entitiesMap.clear();

      this._entities.forEach((entity: T) => {
        this._entityDiffersMap.set(this.getEntityID(entity), this.keyValueDiffers.find(entity).create());
        this._entitiesMap.set(this.getEntityID(entity), entity);
      })
    }
  }

  ngDoCheck() {
    let changedIds: {old: string, curr: string}[] = [];

    if (this._entityDiffersMap) {
      this._entityDiffersMap.forEach((entityDiffer: KeyValueDiffer<string, any>, key: string) => {
        let entity: T = this._entitiesMap.get(key);
        let idField = this.getEntityIDField(entity);
        let card = this._entityCards.find((value: CardDataModel<T>) => {
          return this.compareCardToEntity(entity, value);
        })

        const changes = entityDiffer.diff(entity);
        if (changes && this._entityCards) {
          let changed: boolean = false;
          changes.forEachChangedItem((record: KeyValueChangeRecord<string, any>) => {
            if (idField === record.key) {
              changedIds.push({
                old: record.previousValue,
                curr: record.currentValue
              });
            }
            changed = true;
          });
          if (changed) {
            this.updateEntityCard(entity, card);
          }
        }
      })
    }

    if (this._entitiesDiffers) {
      const changes = this._entitiesDiffers.diff(this._entities);
      if (changes && this._entityCards) {
        changes.forEachRemovedItem((record: IterableChangeRecord<T>) => {
          let deletedIndex: number = this.modelService.findInArray(this._entityCards,
            (item: CardDataModel<T>) => {
              return this.compareCardToEntity(record.item, item);
            });
          if (deletedIndex >= 0) {
            let deletingCard = this._entityCards[deletedIndex];
            let isIDChanged: boolean = (changedIds.filter(value => {
              return value.curr === deletingCard.id
            }).length === 1);
            if (!isIDChanged) {
              this._entityCards.splice(deletedIndex, 1);
              this.filterCards();
            }
          }
        });

        changes.forEachAddedItem((record: IterableChangeRecord<T>) => {
          let existsIndex: number = this.modelService.findInArray(this._entityCards,
            (item: CardDataModel<T>) => {
              return this.compareCardToEntity(record.item, item);
            });

          if (existsIndex < 0) {
            let enityCard = this.getCard(record.item);
            this._entityCards.splice(record.currentIndex, 0, enityCard);
            this.filterCards();
          }
        });
      }
    }

    if (changedIds.length > 0) {
      this.initNgCheck();
    }
  }

  _onEdit(item: CardDataModel<T>) {
    this.onEditCard.emit(item);
  }

  _onDelete(item: CardDataModel<T>) {
    this.onDeleteCard.emit(item);
  }

  _onUnlink(item: CardDataModel<T>) {
    this.onUnlinkCard.emit(item);
  }

  _onChart(item: CardDataModel<T>) {
    this.onChartCard.emit(item);
  }

  _onAddFolder(item: CardDataModel<T>) {
    this.onAddFolder.emit(item);
  }

  _onAddItem(item: CardDataModel<T>) {
    this.onAddItem.emit(item);
  }

  _onExpand(card: CardDataModel<T>) {
    card.expanded = !card.expanded;
    this.onCardExpanded(card);
    this.onExpandCard.emit(card);
  }

  protected updateCards(): void {
    this._entityCards = this._entities.map((item: T) => {
      return this.getCard(item, this._selectItemEnabled ? this.isEntitySelected(item) : undefined);
    });
    this.filterCards();
  }

  protected filterCards(): void {
    this._filteredCards = this._entityCards.filter((card: CardDataModel<T>) => {
      return this._filteredEntities ? this._filteredEntities.indexOf(card.id) >= 0 : true;
    });

    this.buildCardTree();
  }

  protected buildCardTree(): void {
    let rootNode: CardDataModel<T> = <CardDataModel<T>>({id: "0", children: []});
    for (let card of this._filteredCards) {
      card.hasChildren = false;
      card.children = undefined;
    }
    this.getCardNode(rootNode, this._filteredCards);

    this._cardsModel.children = rootNode.children;
  }

  protected getCardNode(cardNode: CardDataModel<T>, availableCards: CardDataModel<T>[]): CardDataModel<T>[] {
    let remainCards = [];
    for (let card of availableCards) {
      if (card.parent_id.toString() === cardNode.id.toString()) {
        if (!cardNode.children) {
          cardNode.children = [];
          cardNode.hasChildren = true;
        }
        else if (!cardNode.hasChildren) {
          cardNode.hasChildren = true;
        }

        cardNode.children.push(card);
      }
      else {
        remainCards.push(card);
      }
    }

    if (cardNode.hasChildren) {
      for (let node of cardNode.children) {
        remainCards = this.getCardNode(node, remainCards);
      }
    }

    return remainCards;
  }

  protected updateCardsVisualStatistics() {
    this._entityCards.forEach((card: CardDataModel<T>) => {
      let entitySummaryVisible = this._entitySummaryVisualStatistics.get(card.name);
      card.entitySummaryVisible = entitySummaryVisible;
    })
  }

  protected updateCardsSelection(): void {
    if (this._entityCards) {
      this._entityCards.forEach((card: CardDataModel<T>) => {
        card.selected = this._selectItemEnabled ? this.isEntitySelected(card.originalEntity) : undefined;
      })
    }
  }

  protected initCardModel(): void {
    if (this._cardsModel === undefined) {
      this._cardsModel = this.getDefaultCardModel();
    }
  }

  protected updatePagingVisibility(): void {
    if (this._totalCount !== undefined && this._pageSizeOptions !== undefined) {
      this._pagingVisible = this.pagingEnabled && (this._totalCount > Math.min(...this._pageSizeOptions));
    }
  }

  public _trackEntityByFn(index: number, item: T): string {
    return "";
  }
}
