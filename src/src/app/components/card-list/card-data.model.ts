import {UxCardTreeNodeModel} from "../../../ux-lib/components/card-tree/card-tree.model";
import {Subscription} from "rxjs/index";
import {
  StatisticsDataEntry, StatisticsDataStyledVisualEntry, StatisticsEntity,
  StatisticsEntityBase
} from "../../common/models/entity/statistics-entity.model";
import {StatisticDataType} from "../../common/models/statistic-data.type";
import {StatisticPeriodType} from "../../common/models/statistic-period.type";

export interface CardDataModel<E> extends UxCardTreeNodeModel {
  parent_id?: string;
  expanded?: boolean;

  name?: string;

  originalEntity?: E;

  statisticsInProgress?: boolean;
  statisticsSubscription$?: Subscription;

  entityStatistics?: StatisticsEntity[];
  entitySummary?: StatisticsEntityBase<StatisticsDataEntry>;
  entitySummaryVisible?: StatisticsEntityBase<StatisticsDataStyledVisualEntry>;

  defaultStatistics?: StatisticDataType;
  defaultPeriod?: StatisticPeriodType;
  defaultDuration?: number;

  lastValueTimestampStart?: number;
  lastValueTimestampEnd?: number;
}

