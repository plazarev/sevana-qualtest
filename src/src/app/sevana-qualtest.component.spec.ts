import { TestBed, async } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { SevanaQualtestComponent } from "./sevana-qualtest.component";

describe("SevanaQualtestComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        SevanaQualtestComponent
      ],
    }).compileComponents();
  }));

  it("should create the app", () => {
    const fixture = TestBed.createComponent(SevanaQualtestComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'sevana-qualtest'`, () => {
    const fixture = TestBed.createComponent(SevanaQualtestComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual("sevana-qualtest");
  });

  it("should render title in a h1 tag", () => {
    const fixture = TestBed.createComponent(SevanaQualtestComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("h1").textContent).toContain("Welcome to sevana-qualtest!");
  });
});
