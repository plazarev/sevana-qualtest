import {Injectable} from "@angular/core";

@Injectable()
export class ValidatorService {

  validate(value: string, pattern: string | RegExp): boolean {
    if (!pattern)
      return true;

    var regex;
    var regexStr;
    if (typeof pattern === 'string') {
      regexStr = '';
      if (pattern.charAt(0) !== '^')
        regexStr += '^';
      regexStr += pattern;
      if (pattern.charAt(pattern.length - 1) !== '$')
        regexStr += '$';
      regex = new RegExp(regexStr);
    }
    else {
      regexStr = pattern.toString();
      regex = pattern;
    }

    return regex.test(value);
  }

  public getNumberValidationPattern(): string | RegExp {
    return /^^((?:(?:\+?\d\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?)|([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)$/;
  }
}
