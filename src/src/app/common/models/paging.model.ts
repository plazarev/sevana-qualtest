import {PagingConfig} from "../services/app-config.service";

export interface PagingModel extends PagingConfig {
  totalCount?: number;
  currentPage?: number;
  maxPageSize?: number;
}
