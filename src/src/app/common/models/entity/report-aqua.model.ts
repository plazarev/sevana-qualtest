export interface AQuAValueItem<T> {
  SourceValue: T | number | boolean;
  DegradedValue: T | number | boolean;
  Units: string;
  up: boolean;
}

export interface SpectrumPair<T> {
  Reference: T;
  Test: T;
}

export interface SpectrumPairsModel<T> {
  PairsNumber: number;
  Pairs: SpectrumPair<T>[]
}

export interface QualityResultsModel<T> {
  Percent: T;
  MOS: T;
  mosStyle?: string;
}

export interface AQuAReportModel<T> {
  Reasons?: string[];
  ValuesArray?: any;
  SpectrumPairs?: SpectrumPairsModel<T>;
  QualityResults?: QualityResultsModel<T>;
}

export interface ReportAquaModel<T> {
  AQuAReport?: AQuAReportModel<T>
}
