
export interface DictionaryType {
  label: string;
  value: string;
}
