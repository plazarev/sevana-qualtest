import {Entity} from "./entity.model";

export interface UpdateEntityModel<T extends Entity> {
  entityID: string;
  value: T;
}
