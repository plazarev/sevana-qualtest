import {Entity} from "./entity.model";
import {UpdateEntityModel} from "./update-entity.model";
import {HierarchyEntityModel} from "./hierarchy-entity.model";

export class DeviceEntity implements HierarchyEntityModel {
  id?: number;
  parent_id?: number;
  attributes?: string;
  instance?: string;
  number?: string;
  type?: string;
  last_timestamp?: number;

  public constructor() {
    this.parent_id = 0;
  }


  public copyTo(to: DeviceEntity): void {
    DeviceEntity.copy(to, this);
  }

  public copyFrom(from: DeviceEntity): void {
    DeviceEntity.copy(this, from);
  }

  public static copy(to: DeviceEntity, from: DeviceEntity): void {
    if (to && from) {
      to.id = from.id;
      to.parent_id = from.parent_id;
      to.attributes = from.attributes;
      to.instance = from.instance;
      to.number = from.number;
      to.type = from.type;
      to.last_timestamp = from.last_timestamp;
    }
  }
}

export interface UpdateDeviceEntity extends UpdateEntityModel<DeviceEntity> {
}
