import {Entity} from "./entity.model";
import {TestResultEntity} from "./test-result-entity.model";

export interface StatisticsDataEntryBase<T> {
  Sum?: T;
  Count?: T;
  average?: T;
  min?: T;
  min_timestamp?: number;
  min_probe_id?: string;
  max?: T;
  max_timestamp?: number;
  max_probe_id?: string;
  last?: T;
  last_timestamp?: number;
  last_probe_id?: string;
}

export interface StatisticsDataEntry extends StatisticsDataEntryBase<number> {
}

export interface StatisticsDataVisualEntry extends StatisticsDataEntryBase<string> {
}

export interface StatisticsDataStyledVisualEntry extends StatisticsDataVisualEntry{
  avgLevel?: string;
  minLevel?: string;
  maxLevel?: string;
  lastLevel?: string;
}

export interface StatisticsEntityBase<T> extends Entity {
  mos_pvqa?: T;
  mos_aqua?: T;
  r_factor?: T;
  mos_network?: T;
}

export interface StatisticsEntityBaseWithLastValueTimestamp<T> extends StatisticsEntityBase<T> {
  lastValueTimestampStart?: number;
  lastValueTimestampEnd?: number;
}

export interface StatisticsStyledVisualEntity extends StatisticsEntityBase<StatisticsDataStyledVisualEntry> {
}

export interface StatisticsEntity extends StatisticsEntityBase<StatisticsDataEntry> {
  start_timestamp: number;
  start_time: string;
  end_timestamp: number;
  end_time: string;
  phone: string;
  nr_of_errors: number;
  nr_of_attempts: number;
}

export interface Statistics24EntityBase<T> {
  id: string,

  avg_mos_pvqa: T,
  min_mos_pvqa: T,
  max_mos_pvqa: T,

  avg_mos_aqua: T,
  min_mos_aqua: T,
  max_mos_aqua: T,

  avg_mos_network: T,
  min_mos_network: T,
  max_mos_network: T,

  avg_rfactor: T,
  min_rfactor: T,
  max_rfactor: T
}

export interface Statistics24Entity extends Statistics24EntityBase<number> {
}

export interface SummaryStatisticsItemEntry {
  average: number,
  min: number,
  min_probe: TestResultEntity,
  max: number,
  max_probe: TestResultEntity,
  last: number,
  last_probe: TestResultEntity
}

export interface SummaryStatisticsItem {
  mos_aqua: SummaryStatisticsItemEntry,
  mos_pvqa: SummaryStatisticsItemEntry,
  mos_network: SummaryStatisticsItemEntry,
  r_factor: SummaryStatisticsItemEntry,
}
