import {UpdateEntityModel} from "./update-entity.model";
import {HierarchyEntityModel} from "./hierarchy-entity.model";

export class TestEntity implements HierarchyEntityModel {
  id?: number;
  parent_id?: number;
  name?: string;
  target?: string;
  command?: string;
  schedule?: string;
  audio_id?: number;
  attributes?: string;
  prepare_audio_for_answer?: boolean;

  public copyTo(to: TestEntity): void {
    TestEntity.copy(to, this);
  }

  public copyFrom(from: TestEntity): void {
    TestEntity.copy(this, from);
  }

  public static copy(to: TestEntity, from: TestEntity): void {
    if (to && from) {
      to.id = from.id;
      to.parent_id = from.parent_id;
      to.name = from.name;
      to.target = from.target;
      to.command = from.command;
      to.schedule = from.schedule;
      to.audio_id = from.audio_id;
      to.attributes = from.attributes;
      to.prepare_audio_for_answer = from.prepare_audio_for_answer;
    }
  }
}

export interface UpdateTestEntity extends UpdateEntityModel<TestEntity> {
}
