import {Entity} from "./entity.model";

export class TestResultEntity implements Entity {
  id: string;
  phone_id: number;
  phone_name: string;
  target: string;
  duration: number;
  endtime: number;
  error: string;
  mos_pvqa: number;
  mos_aqua: number;
  mos_network: number;
  r_factor: number;
  report_pvqa: string;
  report_aqua: string;
  task_id: number;
  task_name: string;
  sip_src: string;
  sip_dst: string;
  sip_callid: string;
  packet_loss: number;
  packets_total: number;
  packets_lost: number;
  jitter: number;
  rtt: number;
}

