import {Entity} from "./entity.model";

export interface HierarchyEntityModel extends Entity {
  id?: number;
  parent_id?: number;
  attributes?: string;
}
