export interface PvqaReportModel<T> {
  mos_pvqa: T;
  endtime: number;
  mosStyle?: string;
}
