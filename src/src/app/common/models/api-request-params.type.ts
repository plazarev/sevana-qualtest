
import {SimpleMap} from "./simple-map.model";
import {QueryParam} from "./query-param.type";
import {HttpResponse} from "@angular/common/http";


export interface ApiRequestParams {
  pathParams?: SimpleMap<any | null>;
  queryParams?: QueryParam[];

  body?: any;
  requestParams?: SimpleMap<any>;
  dataExtracter?: (res: HttpResponse<any>) => any;
  headerParams?: SimpleMap<any>;
  responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
}
