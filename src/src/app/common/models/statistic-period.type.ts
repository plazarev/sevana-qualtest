export type StatisticPeriodType = 'hour' | 'day' | 'month';

export class StatisticPeriod {

  public static readonly PERIOD_HOURS: StatisticPeriodType = 'hour';
  public static readonly PERIOD_DAYS: StatisticPeriodType = 'day';
  public static readonly PERIOD_MONTHS: StatisticPeriodType = 'month';

}
