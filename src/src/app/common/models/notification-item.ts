
export interface NotificationItem {
  type?: string;
  caption?: string;
  content?: string;
  timeout?: number;
  closable?: boolean;
  visible?: boolean;
}
