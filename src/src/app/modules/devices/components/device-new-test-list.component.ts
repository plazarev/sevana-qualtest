import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output
} from "@angular/core";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {forkJoin, Subject, Subscription} from "rxjs/index";
import {takeUntil, tap} from "rxjs/internal/operators";
import {ModelService} from "../../../common/services/model.service";
import {NotificationService} from "../../../common/services/notification.service";
import {CssService} from "../../../common/services/css.service";
import {TestsService} from "../../tests/tests.service";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ReportsService} from "../../reports/reports.service";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {UxCardTreeCheckedEvent} from "../../../../ux-lib/components/card-tree/card-tree.component";
import {PagedComponent} from "../../../components/base/paged.component";

@Component({
  selector: "sq-device-new-test-list",
  templateUrl: "device-new-test-list.component.html",
  host: {"[class.sq-device-new-test-list]": "true"}
})
export class DeviceNewTestListComponent extends PagedComponent {
  private _tests: TestEntity[] = [];
  private _selectedTests: string[] = [];

  private stopSubscription$ = new Subject<boolean>();
  private forkSubscription$: Subscription;

  private _device: DeviceEntity;
  @Input()
  public set device(value: DeviceEntity) {
    this._device = value;

    this.resetSelectedTests();
    if (this.viewInitiated) {
      this.refresh();
    }
  }

  public get device(): DeviceEntity {
    return this._device;
  }

  @Output()
  public onCardChecked: EventEmitter<string[]> = new EventEmitter<string[]>();

  constructor(private notificationService: NotificationService,
              private cdr: ChangeDetectorRef,
              private testsService: TestsService,
              reportsService: ReportsService,
              modelService: ModelService,
              cssService: CssService,
              configService: AppConfigService) {
    super(
      configService
    )
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  protected _onCardChecked(event: UxCardTreeCheckedEvent) {
    this._selectedTests = this._selectedTests.filter((value: string) => value !== event.model.id);
    if (event.model.selected) {
      this._selectedTests.push(event.model.id);
    }
    this.onCardChecked.emit(this._selectedTests);
  }

  private resetSelectedTests(): void {
    this._selectedTests = [];
  }

  public loadPageData(currentPage: number, pageSize: number): void {
    if (this._device) {
      this._pageLoadingStatus = 'loading';

      let deviceID = this._device.instance;
      this.forkSubscription$ && this.forkSubscription$.unsubscribe();
      this.forkSubscription$ = forkJoin(
        this.testsService.getEntityList(),
        this.testsService.getLinkedTests(deviceID),
      )
        .pipe(
          takeUntil(this.stopSubscription$),
          tap((data: [TestEntity[], TestEntity[]]) => {
            let allTests = data[0];
            let linkedTests = data[1];
            let linkedTestsIds = linkedTests.map(item => item.name);
            let filteredTests = allTests.filter(item => linkedTestsIds.indexOf(item.name) < 0);

            this.pagingConfig.totalCount = filteredTests.length;
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = startIndex + pageSize;
            this._tests = filteredTests.slice(startIndex, endIndex > filteredTests.length ? filteredTests.length : endIndex);
            this._pageLoadingStatus = 'loaded';
            this.cdr.detectChanges();
          })
        )
        .subscribe();
    }
  }
}
