import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output, ViewChild
} from "@angular/core";
import {DeviceEntity, UpdateDeviceEntity} from "../../../common/models/entity/device-entity.model";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {UxValueChangeEvent} from "../../../../ux-lib/components/fields/abstract-field.component";
import {FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {DictionaryService} from "../../../common/services/dictionary.service";
import {Subject, Subscription} from "rxjs/index";
import {DictionaryType} from "../../../common/models/entity/dictionary-type.model";
import {takeUntil, tap} from "rxjs/internal/operators";
import {PostOperationResult, PutOperationResult} from "../../../common/services/entity.service";
import {DevicesService} from "../devices.service";
import {ModelService} from "../../../common/services/model.service";
import {DeviceLinkedTestComponent} from "./device-linked-test.component";
import {PopupComponent} from "../../../components/base/popup.component";

@Component({
  selector: "sq-device-edit",
  templateUrl: "device-edit.component.html",
  host: {"[class.sq-device-edit]": "true"}
})
export class DeviceEditComponent extends PopupComponent<DeviceEntity> {

  private _formGroup: FormGroup;
  private _typesItemsModel: UxDropdownListItem[] = [];
  private _typesItemsValue: UxDropdownListItem = {};
  private _availableDeviceTypes: DictionaryType[] = [];
  private _operationErrorText: string = "";

  private stopSubscription$ = new Subject<boolean>();
  private dictionarySubscription$: Subscription;
  private deviceCreateSubscription$: Subscription;
  private deviceUpdateSubscription$: Subscription;

  @ViewChild("linkedTestsPopup")
  linkedTestsPopup: DeviceLinkedTestComponent;

  private _creatingMode: boolean = false;
  @Input()
  public set creatingMode(value: boolean) {
    this._creatingMode = value;
    if (this._creatingMode) {
      this.deviceModel = undefined;
    }
  }

  public get creatingMode(): boolean {
    return this._creatingMode;
  }

  private _deviceID: string;
  private _deviceModel: DeviceEntity = new DeviceEntity();
  @Input()
  public set deviceModel(value: DeviceEntity) {
    if (value) {
      this._deviceID = value.instance;
      DeviceEntity.copy(this._deviceModel, value);
      this._typesItemsValue =
        this.dictionaryService.findDictionaryItem(this._availableDeviceTypes, this._deviceModel.type);

      this._formGroup.patchValue({
        "deviceName": this.deviceModel.instance,
        "deviceType": this._typesItemsValue,
        "deviceNumber": this.deviceModel.number,
      });
    }
    else {
      this._deviceModel = new DeviceEntity();
      this.resetFormData();
    }
  }

  public get deviceModel(): DeviceEntity {
    return this._deviceModel;
  }

  @Input()
  public parentEntity: DeviceEntity;

  @Output()
  public onCreateComplete: EventEmitter<DeviceEntity> = new EventEmitter<DeviceEntity>();

  @Output()
  public onEditComplete: EventEmitter<UpdateDeviceEntity> = new EventEmitter<UpdateDeviceEntity>();

  constructor(private devicesService: DevicesService,
              private modelService: ModelService,
              private dictionaryService: DictionaryService,
              private formBuilder: FormBuilder,
              private cdRef: ChangeDetectorRef) {
    super();
  }

  public initComponent() {
    super.initComponent();
    this.initForm();
  }

  public loadInitialData() {
    super.loadInitialData();
    this.updateDeviceTypes();
    this.cdRef.detectChanges();
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  public getUserData(): DeviceEntity {
    return this._deviceModel;
  }

  private updateDeviceTypes() {
    this.dictionarySubscription$ && this.dictionarySubscription$.unsubscribe();
    this.dictionarySubscription$ = this.dictionaryService
      .getDevicesTypes()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((types: DictionaryType[]) => {
          this._availableDeviceTypes = types;
          this._typesItemsModel = types;
          this._typesItemsValue = this._typesItemsModel[0];
        })
      )
      .subscribe();
  }

  _onDeviceTypeChange(event: UxValueChangeEvent<UxDropdownListItem>) {
    if (event && event.newValue) {
      this._deviceModel.type = event.newValue.value;
    }
  }

  public _getErrorText(controlName: string): string {
    let errorText = "",
      control = this._formGroup.controls[controlName];

    if (control.errors) {
      errorText = "!";
      let errors: ValidationErrors = control.errors;

      if (controlName === "deviceName") {
        if (errors["required"] !== undefined) {
          errorText = "Device name is required";
        }
      } else if (controlName === "deviceNumber") {
        if (errors["required"] !== undefined) {
          errorText = "Device number is required";
        } else if (errors["pattern"] !== undefined) {
          errorText = "Device number must contains phone number or SIP address!";
        }
      }
    }
    return errorText;
  }

  _onLinkedTest(): void {
    this.linkedTestsPopup.device = this.deviceModel;
    this.linkedTestsPopup.show();
  }

  _onEditConfirm() {
    if (this.creatingMode) {
      if (this.parentEntity) {
        this._deviceModel.parent_id = this.parentEntity.id;
      }
      else {
        this._deviceModel.parent_id = 0;
      }

      this.deviceCreateSubscription$ && this.deviceCreateSubscription$.unsubscribe();
      this.deviceCreateSubscription$ = this.devicesService
        .createEntity(this._deviceModel)
        .pipe(
          takeUntil(this.stopSubscription$),
          tap((data: PostOperationResult<DeviceEntity>) => {
            if (data.opResult) {
              let createdDevice: DeviceEntity = this.modelService.deepCopy(data.opEntity);
              this.onCreateComplete.emit(createdDevice);
              this._close();
            } else {
              this._operationErrorText = "Failed to create device";
            }
          })
        )
        .subscribe()
    }
    else {
      this.deviceUpdateSubscription$ && this.deviceUpdateSubscription$.unsubscribe();
      this.deviceUpdateSubscription$ = this.devicesService
        .updateEntityById(this._deviceID, this._deviceModel)
        .pipe(
          takeUntil(this.stopSubscription$),
          tap((data: PutOperationResult<DeviceEntity>) => {
            if (data.opResult) {
              let updateDevice: UpdateDeviceEntity = {
                entityID: this._deviceID,
                value: this._deviceModel
              }
              this.onEditComplete.emit(updateDevice);
              this._close();
            }
            else {
              this._operationErrorText = "Failed o update device information";
            }
          })
        )
        .subscribe();
    }
  }

  protected _close() {
    this.parentEntity = undefined;
    this.resetFormData();
    super._close();
  }

  private initForm(): void {
    this._formGroup = this.formBuilder.group({
      "deviceName": ["", Validators.compose([Validators.required])],
      "deviceType": [this._typesItemsValue],
      "deviceNumber": ["", Validators.compose([
        Validators.required,
        Validators.pattern(this.devicesService.getNumberValidationPattern())])
      ]
    });
  }

  private resetFormData() {
    this._formGroup.updateValueAndValidity();
    this._formGroup.reset();
    this._formGroup.markAsUntouched();
  }
}
