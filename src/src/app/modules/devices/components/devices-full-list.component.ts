import {Component, ViewChild} from "@angular/core";
import {AppConfigService} from "../../../common/services/app-config.service";
import {Subject, Subscription} from "rxjs/index";
import {DevicesService} from "../devices.service";
import {takeUntil, tap} from "rxjs/internal/operators";
import {CountResult, DeleteOperationResult, PostOperationResult} from "../../../common/services/entity.service";
import {QueryParam} from "../../../common/models/query-param.type";
import {ApiPath} from "../../../common/services/api.path";
import {DeviceEntity, UpdateDeviceEntity} from "../../../common/models/entity/device-entity.model";
import {DeviceCardModel} from "../model/device-card.model";
import {ModelService} from "../../../common/services/model.service";
import {NotificationService} from "../../../common/services/notification.service";
import {DeviceEditComponent} from "./device-edit.component";
import {TestsService} from "../../tests/tests.service";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {PagedComponent} from "../../../components/base/paged.component";
import {ConfirmPopupComponent} from "../../../components/confirm-popup/confirm-popup.component";
import {Router} from "@angular/router";
import * as moment from "moment";
import {ReportsService} from "../../reports/reports.service";
import {ReportRequestParametersModel} from "../../reports/model/report-request-parameters.model";
import {
  Statistics24Entity,
  StatisticsDataEntry, StatisticsDataStyledVisualEntry, StatisticsEntity,
  StatisticsEntityBase, SummaryStatisticsItem
} from "../../../common/models/entity/statistics-entity.model";
import {Progress} from "../../../common/models/progress.model";
import {DevicesCardListComponent} from "./devices-card-list.component";
import {FolderPopupComponent} from "../../../components/folder-popup/folder-popup.component";
import {FolderPopupEvent} from "../../../components/folder-popup/folder-popup.event";
import {CardDataModel} from "../../../components/card-list/card-data.model";

@Component({
  selector: "sq-devices-full-list",
  templateUrl: "devices-full-list.component.html",
  host: {"[class.sq-devices-full-list]": "true"}
})
export class DevicesFullListComponent extends PagedComponent {

  _deviceSearchValue: string;
  _devices: DeviceEntity[] = [];
  _filteredDevices: string[];

  private _deviceStatisticsLoading: Progress = 'loading';
  private _devicesFullStatistics: Map<string, SummaryStatisticsItem> = new Map();
  private _devicesSummaryStatistics: Map<string, StatisticsEntityBase<StatisticsDataEntry>>;
  private _devicesSummaryVisualStatistics: Map<string, StatisticsEntityBase<StatisticsDataStyledVisualEntry>>;

  _deviceDeleteID: string;

  private stopSubscription$ = new Subject<boolean>();
  private devicesCountSubscription$: Subscription;
  private devicesListSubscription$: Subscription;
  private devicesStatisticsSubscription$: Subscription;
  private deviceDeleteSubscription$: Subscription;
  private folderCreateSubscription$: Subscription;

  @ViewChild("deviceCardList")
  deviceCardList: DevicesCardListComponent;

  @ViewChild("deviceEditPopup")
  deviceEditPopup: DeviceEditComponent;
  deviceEditCard: DeviceCardModel;

  @ViewChild("deviceDeleteConfirmPopup")
  deviceDeleteConfirmPopup: ConfirmPopupComponent<DeviceEntity>;

  @ViewChild("folderCreatePopup")
  folderCreatePopup: FolderPopupComponent<DeviceEntity>;

  constructor(protected configService: AppConfigService,
              private devicesService: DevicesService,
              private reportsService: ReportsService,
              private modelService: ModelService,
              private notificationService: NotificationService,
              private router: Router) {
    super(configService)
  }

  public loadPageData(currentPage: number, pageSize: number): void {
    this.retrieveDevicesList(currentPage, pageSize);
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  _onSearchValueChange(value: string) {
    this.filterDevices();
  }

  private filterDevices() {
    if (this._devices) {
      this._filteredDevices = this._devices.filter((value: DeviceEntity) => {
        if (this._deviceSearchValue) {
          return (value.type === "folder") ||
            value.instance.indexOf(this._deviceSearchValue) >= 0 ||
            value.number.indexOf(this._deviceSearchValue) >= 0;
        }
        else {
          return true;
        }
      })
        .map((entity: DeviceEntity) => {
          return entity.id.toString();
        });
    }
    else {
      this._filteredDevices = [];
    }
  }

  _onAddNewFolderClicked(card: DeviceCardModel): void {
    this.folderCreatePopup.creatingMode = true;
    if (card) {
      this.folderCreatePopup.userData = card.originalEntity;
    }
    else {
      this.folderCreatePopup.userData = undefined;
    }

    this.folderCreatePopup.show();
  }

  _onAddNewClicked(card: DeviceCardModel): void {
    this.deviceEditPopup.creatingMode = true;
    if (card) {
      this.deviceEditPopup.parentEntity = card.originalEntity;
    }
    else {
      this.deviceEditPopup.parentEntity = undefined;
    }
    this.deviceEditPopup.show();
  }

  _onEditCard(deviceCard: DeviceCardModel) {
    this.deviceEditCard = deviceCard;
    this.deviceEditPopup.creatingMode = false;
    this.deviceEditPopup.deviceModel = deviceCard.originalEntity;
    this.deviceEditPopup.show();
  }

  _onDeleteCard(deviceCard: DeviceCardModel) {
    this._deviceDeleteID = deviceCard.originalEntity.instance;
    this.deviceDeleteConfirmPopup.userData = deviceCard.originalEntity;
    this.deviceDeleteConfirmPopup.show();
  }

  _onChartCard(deviceCard: DeviceCardModel) {
    let period: moment.unitOfTime.DurationConstructor = 'h';
    if (this.devicesConfig.defaultPeriod === 'hour') {
      period = 'h';
    }
    else if (this.devicesConfig.defaultPeriod === 'day') {
      period = 'd';
    }
    else if (this.devicesConfig.defaultPeriod === 'month') {
      period = 'M';
    }
    let dateFrom: Date = moment().subtract(this.devicesConfig.defaultDuration, period).toDate();
    let dateTo: Date = new Date();

    this.router.navigate(['/reports/view'], {
      queryParams: {
        period: this.devicesConfig.defaultPeriod,
        from: dateFrom.getTime(),
        to: dateTo.getTime(),
        devices: [deviceCard.originalEntity.instance],
        tests: []
      }
    });
  }

  _onExpandCard(deviceCard: DeviceCardModel) {
  }

  _onDeviceCreateComplete(deviceData: DeviceEntity) {
    this.retrieveDevicesList(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
  }

  _onDeviceEditComplete(deviceData: UpdateDeviceEntity) {
    for (let device of this._devices) {
      if (device.instance === deviceData.entityID) {
        DeviceEntity.copy(device, deviceData.value);
      }
    }
  }

  _onDevicePopupClose(deviceEntity: DeviceEntity) {
    if (this.deviceEditCard !== undefined) {
      this.deviceCardList.updateLinkedList(this.deviceEditCard);

      this.deviceEditCard = undefined;
    }
  }

  _onDeviceCancel(deviceData: DeviceEntity) {
    this._deviceDeleteID = undefined;
  }

  _onDeviceDelete(deviceData: DeviceEntity) {
    let deviceID = deviceData.instance;
    this.deviceDeleteSubscription$ && this.deviceDeleteSubscription$.unsubscribe();
    this.deviceDeleteSubscription$ = this.devicesService
      .deleteEntity(deviceID)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((data: DeleteOperationResult) => {
          if (data.opResult) {
            //delete entity
            let deletedIndex: number = this.modelService.findInArray(this._devices,
              (item: DeviceEntity) => {
              return (item.instance === deviceID);
            });
            if (deletedIndex >= 0) {
              this._devices.splice(deletedIndex, 1);
            }

          } else {
            this.notificationService.pushNotification({
              type: "error",
              caption: "Deletion error",
              content: `Fail to delete device '{deviceID}'`
            })
          }
        })
      )
      .subscribe();

    this._deviceDeleteID = undefined;
  }

  _onFolderCreate(popupEvent: FolderPopupEvent<DeviceEntity>) {
    let folderModel = new DeviceEntity();
    folderModel.type = "folder";
    folderModel.instance = popupEvent.newFolderName;
    if (popupEvent.userData) {
      folderModel.parent_id = popupEvent.userData.id;
    }

    this.folderCreateSubscription$ && this.folderCreateSubscription$.unsubscribe();
    this.folderCreateSubscription$ && this.folderCreateSubscription$.unsubscribe();
    this.folderCreateSubscription$ = this.devicesService
      .createEntity(folderModel)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((data: PostOperationResult<DeviceEntity>) => {
          if (data.opResult) {
            let createdFolder: DeviceEntity = this.modelService.deepCopy(data.opEntity);
            this.folderCreatePopup.hide();

            this.retrieveDevicesList(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
          } else {
            this.folderCreatePopup.operationStatus = "Failed to create folder";
          }
        })
      )
      .subscribe()
  }

  private retrieveDevicesList(currentPage: number, pageSize: number): void {
    this._pageLoadingStatus = 'loading';
    this.devicesCountSubscription$ && this.devicesCountSubscription$.unsubscribe();
    this.devicesCountSubscription$ = this.devicesService
      .getEntitiesCount()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((result: CountResult) => {
          this.pagingConfig.totalCount = result.totalcount;
          this._pageLoadingStatus = 'loaded';
          this.retrieveDevicesListPage(currentPage, pageSize);
        })
      )
      .subscribe();
  }

  private retrieveDevicesListPage(currentPage: number, pageSize: number): void {
    this._pageLoadingStatus = 'loading';
    let queryParams: QueryParam[] = [
      {
        key: ApiPath.OFFSET,
        value: (currentPage-1) * pageSize
      },
      {
        key: ApiPath.LIMIT,
        value: pageSize
      }
    ];

    this.devicesListSubscription$ && this.devicesListSubscription$.unsubscribe();
    this.devicesListSubscription$ = this.devicesService
      .getEntityList(queryParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((devices: DeviceEntity[]) => {
          this._pageLoadingStatus = 'loaded';
          this._devices = devices;
          this.filterDevices();

          this.retrieveDevicesStatistics();
        })
      )
      .subscribe();
  }

  private retrieveDevicesStatistics() {
    this._deviceStatisticsLoading = 'loading';

    let period: moment.unitOfTime.DurationConstructor = 'h';
    if (this.devicesConfig.defaultPeriod === 'hour') {
      period = 'h';
    }
    else if (this.devicesConfig.defaultPeriod === 'day') {
      period = 'd';
    }
    else if (this.devicesConfig.defaultPeriod === 'month') {
      period = 'M';
    }
    let dateFrom: Date = moment().subtract(this.devicesConfig.defaultDuration, period).toDate();
    let dateTo: Date = new Date();
    let phonesList = this._devices.map((device: DeviceEntity) => {
      return device.instance;
    });

    let reportParams: ReportRequestParametersModel = {
      startDate: dateFrom,
      endDate: dateTo,
      scale: this.devicesConfig.defaultPeriod,
      united: false,
      phonesList: phonesList,
      testsList: []
    };

    this.devicesStatisticsSubscription$ && this.devicesStatisticsSubscription$.unsubscribe();
    this.devicesStatisticsSubscription$ = this.reportsService
      .getDevicesStatistics(reportParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((statistics: Map<string, SummaryStatisticsItem>) => {
          if (statistics) {
            this._devicesFullStatistics = statistics;
          } else {
            this._devicesFullStatistics = new Map();
          }

          this._devicesSummaryStatistics = this.reportsService.convertSummaryStatisticsPerItem(phonesList, this._devicesFullStatistics);
          this._devicesSummaryVisualStatistics = this.reportsService.calculateSummaryVisualStatisticsPerItem(this._devicesSummaryStatistics);

          this._deviceStatisticsLoading = 'loaded';
        })
      )
      .subscribe();
  }
}
