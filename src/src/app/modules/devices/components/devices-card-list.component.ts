import {
  Component, IterableDiffers, KeyValueDiffers, QueryList, TemplateRef,
  ViewChild, ViewChildren
} from "@angular/core";
import {AppConfigService, DevicesConfig} from "../../../common/services/app-config.service";
import {UxCardTreeModel} from "../../../../ux-lib/components/card-tree/card-tree.model";
import {DeviceCardModel} from "../model/device-card.model";
import {CardListComponent} from "../../../components/card-list/card-list.component";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {ResultsService} from "../../results/results.service";
import {ReportsService} from "../../reports/reports.service";
import {ModelService} from "../../../common/services/model.service";
import {CssService} from "../../../common/services/css.service";
import {ReportRequestParametersModel} from "../../reports/model/report-request-parameters.model";
import * as moment from "moment";
import {DeviceLinkedTestListComponent} from "./device-linked-test-list.component";

@Component({
  selector: "sq-devices-card-list",
  templateUrl: "devices-card-list.component.html",
  host: {"[class.sq-devices-card-list]": "true"}
})
export class DevicesCardListComponent extends CardListComponent<DeviceEntity> {

  protected devicesConfig: DevicesConfig = {
    defaultStatistics: 'mos_aqua',
    defaultPeriod: 'hour',
    defaultDuration: 24
  };

  @ViewChild("cardTemplate")
  private cardTemplate: TemplateRef<any>;

  @ViewChildren(DeviceLinkedTestListComponent)
  private linkedLists: QueryList<DeviceLinkedTestListComponent>;

  constructor(protected modelService: ModelService,
              protected configService: AppConfigService,
              protected iterableDiffers: IterableDiffers,
              protected keyValueDiffers: KeyValueDiffers) {
    super(modelService,
      iterableDiffers,
      keyValueDiffers);

    this.loadDevicesConfig();
  }

  public updateLinkedList(card: DeviceCardModel): void {
    this.linkedLists.forEach((linkedList: DeviceLinkedTestListComponent) => {
      if (linkedList.device.instance === card.id) {
        linkedList.refresh();
      }
    })
  }

  protected loadDevicesConfig(): void {
    let devsConfig: DevicesConfig = this.configService.devices();
    if (devsConfig) {
      this.devicesConfig.defaultStatistics = devsConfig.defaultStatistics || 'mos_aqua';
      this.devicesConfig.defaultPeriod = devsConfig.defaultPeriod || 'hour';
      this.devicesConfig.defaultDuration = devsConfig.defaultDuration || 24;
    }
  }

  protected getEntityID(entity: DeviceEntity): string {
    return entity.instance;
  }

  protected getEntityIDField(entity: DeviceEntity): string {
    return "instance";
  }

  protected compareCardToEntity(entity: DeviceEntity, card: DeviceCardModel): boolean {
    return (card.originalEntity.instance === entity.instance);
  }

  protected updateEntityCard(entity: DeviceEntity, card: DeviceCardModel) {
    card.id = entity.instance;
    card.name = entity.instance;
    card.number = entity.number;
    card.type = entity.type;
  }

  protected isEntitySelected(entity: DeviceEntity): boolean {
    return this._selectedEntities.indexOf(entity.instance) >= 0;
  }

  protected getCard(device: DeviceEntity, selected?: boolean): DeviceCardModel {
    return <DeviceCardModel> {
      id: device.id.toString(),
      parent_id: device.parent_id.toString(),
      visible: true,
      hasChildren: false,
      name: device.instance,
      type: device.type,
      number: device.number,
      lastTime: device.last_timestamp,
      originalEntity: device,
      selected: selected,
      expanded: false,
      statisticsInProgress: false,
      entityStatistics: [],
      entitySummary: undefined,
      defaultStatistics: this.devicesConfig.defaultStatistics,
      defaultPeriod: this.devicesConfig.defaultPeriod,
      defaultDuration: this.devicesConfig.defaultDuration
    }
  }

  protected getDefaultCardModel(): UxCardTreeModel {
    return {
      template: this.cardTemplate,
      children: []
    }
  }

  public _trackEntityByFn(index: number, item: DeviceEntity): string {
    return item && item.instance;
  }
}
