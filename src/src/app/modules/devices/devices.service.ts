import {Injectable, Injector} from "@angular/core";
import {
  EntityService
  } from "../../common/services/entity.service";
import {DeviceEntity} from "../../common/models/entity/device-entity.model";
import {ApiPath} from "../../common/services/api.path";
import {DictionaryType} from "src/app/common/models/entity/dictionary-type.model";
import {Observable, Subscriber} from "rxjs/index";

@Injectable()
export class DevicesService extends EntityService<DeviceEntity> {
  protected readonly baseUrl: string = ApiPath.PHONES_PATH;
  protected readonly entityID: string = ApiPath.PHONE_ID;
  protected readonly entityUrl: string[] = [this.baseUrl, "/json/device-by-id.json"];
  protected readonly entityCountUrl: string[] = [this.baseUrl + ApiPath.COUNT_PATH, "/json/devices-count.json"];
  protected readonly entityListUrl: string[] = [this.baseUrl, "/json/devices-list.json"];
  protected readonly entityCreateUrl: string[] = [this.baseUrl, "/json/device-create.json"];
  protected readonly entityUpdateUrl: string[] = [this.baseUrl, "/json/device-create.json"];
  protected readonly entityDeleteUrl: string[] = [this.baseUrl, "/json/device-create.json"];

  constructor(protected injector: Injector) {
    super(injector);
  }

  public findInArray(items: DeviceEntity[], id: string): number {
    return this.modelService.findInArray(items, (el: DeviceEntity) => {
      return el.instance === id;
    });
  }

  public getNumberValidationPattern(): string | RegExp {
    return /^^((?:(?:\+?\d\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?)|([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)$/;
  }
}
