import {DeviceCardModel} from "./device-card.model";
import {TestCardModel} from "../../tests/model/test-card.model";

export interface DeviceTestEvent {
  deviceCard: DeviceCardModel;
  testCard: TestCardModel;
}
