import {Subscription} from "rxjs/index";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {Progress} from "../../../common/models/progress.model";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {CardDataModel} from "../../../components/card-list/card-data.model";

export interface DeviceCardModel extends CardDataModel<DeviceEntity> {
  type?: string;
  number?: string;
  lastTime?: number;

  childs?: number;

  linkedTestsSubscription$?: Subscription;
  unlinkTestsSubscription$?: Subscription;
  linkedTests?: TestEntity[];
  linkedTestsLoading?: Progress;
}
