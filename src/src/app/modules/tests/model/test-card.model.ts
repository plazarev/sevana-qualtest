import {Subscription} from "rxjs/index";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {CardDataModel} from "../../../components/card-list/card-data.model";

export interface TestCardModel extends CardDataModel<TestEntity> {
  command?: string;
  target?: string;
  cron?: string;

  lastValueSubscription$?: Subscription;
}
