import {
  Component, EventEmitter, Input, IterableDiffers, KeyValueDiffers, Output,
  TemplateRef,
  ViewChild
} from "@angular/core";
import {AppConfigService, TestsConfig} from "../../../common/services/app-config.service";
import {UxCardTreeModel} from "../../../../ux-lib/components/card-tree/card-tree.model";
import {TestCardModel} from "../model/test-card.model";
import {takeUntil, tap} from "rxjs/operators";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import * as moment from "moment";
import {ReportsService} from "../../reports/reports.service";
import {ModelService} from "../../../common/services/model.service";
import {CssService} from "../../../common/services/css.service";
import {ReportRequestParametersModel} from "../../reports/model/report-request-parameters.model";
import {ResultsService} from "../../results/results.service";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {CardListComponent} from "../../../components/card-list/card-list.component";

@Component({
  selector: "sq-tests-card-list",
  templateUrl: "tests-card-list.component.html",
  host: {"[class.sq-tests-card-list]": "true"}
})
export class TestsCardListComponent extends CardListComponent<TestEntity> {

  protected testsConfig: TestsConfig = {
    defaultStatistics: 'mos_aqua',
    defaultPeriod: 'hour',
    defaultDuration: 24
  };

  @ViewChild("cardTemplate")
  private cardTemplate: TemplateRef<any>;

  private _activeDeviceID: string;
  @Input()
  public set activeDeviceID(value: string) {
    this._activeDeviceID = value;
  }

  public get activeDeviceID(): string {
    return this._activeDeviceID;
  }

  @Output()
  public onProbeStatistics: EventEmitter<string> = new EventEmitter<string>();

  constructor(private resultsService: ResultsService,
              protected reportsService: ReportsService,
              protected modelService: ModelService,
              protected cssService: CssService,
              protected configService: AppConfigService,
              protected iterableDiffers: IterableDiffers,
              protected keyValueDiffers: KeyValueDiffers) {
    super(modelService,
      iterableDiffers,
      keyValueDiffers);

    this.loadTestsConfig();
  }

  public loadTestsConfig(): void {
    let testsConfig: TestsConfig = this.configService.tests();
    if (testsConfig) {
      this.testsConfig.defaultStatistics = testsConfig.defaultStatistics || 'mos_aqua';
      this.testsConfig.defaultPeriod = testsConfig.defaultPeriod || 'hour';
      this.testsConfig.defaultDuration = testsConfig.defaultDuration || 24;
    }
  }

  protected getEntityID(entity: TestEntity): string {
    return entity.name;
  }

  protected getEntityIDField(entity: TestEntity): string {
    return "name";
  }

  protected compareCardToEntity(entity: TestEntity, card: TestCardModel): boolean {
    return (card.originalEntity.name === entity.name);
  }

  protected updateEntityCard(entity: TestEntity, card: TestCardModel) {
    card.id = entity.name;
    card.name = entity.name;
    card.target = entity.target;
    card.command = entity.command;
    card.cron = entity.schedule;
  }

  protected isEntitySelected(entity: TestEntity): boolean {
    return this._selectedEntities.indexOf(entity.name) >= 0;
  }

  protected getCard(test: TestEntity, selected?: boolean): TestCardModel {
    return <TestCardModel> {
      id: test.name,
      visible: true,
      hasChildren: false,
      name: test.name,
      selected: selected,
      command: test.command,
      target: test.target,
      cron: test.schedule,
      originalEntity: test,
      expanded: false,
      statisticsInProgress: false,
      entityStatistics: [],
      entitySummary: undefined,
      defaultStatistics: this.testsConfig.defaultStatistics,
      defaultPeriod: this.testsConfig.defaultPeriod,
      defaultDuration: this.testsConfig.defaultDuration
    }
  }

  protected getDefaultCardModel(): UxCardTreeModel {
    return {
      template: this.cardTemplate,
      children: []
    }
  }

  public _trackEntityByFn(index: number, item: TestEntity): string {
    return item && item.name;
  }
}

