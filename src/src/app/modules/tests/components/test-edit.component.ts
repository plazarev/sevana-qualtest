import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output
} from "@angular/core";
import {FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {DictionaryService} from "../../../common/services/dictionary.service";
import {Subject, Subscription} from "rxjs/index";
import {DictionaryType} from "../../../common/models/entity/dictionary-type.model";
import {takeUntil, tap} from "rxjs/internal/operators";
import {PostOperationResult, PutOperationResult} from "../../../common/services/entity.service";
import {ModelService} from "../../../common/services/model.service";
import {NotificationService} from "../../../common/services/notification.service";
import {TestsService} from "../tests.service";
import {TestEntity, UpdateTestEntity} from "../../../common/models/entity/test-entity.model";
import {ReferenceSelectorItem} from "../../../../ux-lib/components/fields/reference/reference-item.model";
import {DevicesService} from "../../devices/devices.service";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {PopupComponent} from "../../../components/base/popup.component";

const TEST_DEFAULT_CRON: string = "0 0 * ? * *";

@Component({
  selector: "sq-test-edit",
  templateUrl: "test-edit.component.html",
  host: {"[class.sq-test-edit]": "true"}
})
export class TestEditComponent extends PopupComponent<TestEntity> {

  private _formGroup: FormGroup;
  private _availableTestsTypes: DictionaryType[] = [];
  private _selectedTarget: ReferenceSelectorItem[];
  private _availableTargets: ReferenceSelectorItem[] = [];
  private _creatingTarget: ReferenceSelectorItem = {};
  private _operationErrorText: string = "";

  private stopSubscription$ = new Subject<boolean>();
  private dictionarySubscription$: Subscription;
  private testCreateSubscription$: Subscription;
  private testUpdateSubscription$: Subscription;
  private devicesSubscription$: Subscription;

  private _creatingMode: boolean = false;
  @Input()
  public set creatingMode(value: boolean) {
    this._creatingMode = value;
    if (this._creatingMode) {
      this.testModel = undefined;
    }
  }

  public get creatingMode(): boolean {
    return this._creatingMode;
  }

  private _testID: string;
  private _testModel: TestEntity = new TestEntity();
  @Input()
  public set testModel(value: TestEntity) {
    if (value) {
      this._testID = value.name;
      TestEntity.copy(this._testModel, value);
      let item: ReferenceSelectorItem = this._availableTargets
        .find((value: ReferenceSelectorItem) => {
          return value.caption === this._testModel.target;
        });
      if (item === undefined) {
        this.createNewReference(this._testModel.target);
      }
      else {
        this._selectedTarget = [item];
      }

      this._formGroup.patchValue({
        "testName": value.name,
        "testTarget": this._selectedTarget,
        "cronCommand": value.schedule,
      });
    }
    else {
      this._testModel = new TestEntity();
      this.resetFormData();
    }
  }

  public get testModel(): TestEntity {
    return this._testModel;
  }

  @Output()
  public onCreateComplete: EventEmitter<TestEntity> = new EventEmitter<TestEntity>();

  @Output()
  public onEditComplete: EventEmitter<UpdateTestEntity> = new EventEmitter<UpdateTestEntity>();

  constructor(private testsService: TestsService,
              private devicesService: DevicesService,
              private modelService: ModelService,
              private notificationService: NotificationService,
              private dictionaryService: DictionaryService,
              private formBuilder: FormBuilder,
              private cdRef: ChangeDetectorRef) {
    super();
  }

  public initComponent() {
    this.initForm();
    this.updateTestsTargets();
    this.cdRef.detectChanges();
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  private initForm(): void {
    this._formGroup = this.formBuilder.group({
      "testName": ["", Validators.compose([Validators.required])],
      "testTarget": ["", Validators.compose([Validators.required])],
      "cronCommand": [TEST_DEFAULT_CRON, Validators.compose([Validators.required])]
    });
  }

  private updateTestsTargets() {
    this.devicesSubscription$ && this.devicesSubscription$.unsubscribe();
    this.devicesSubscription$ = this.devicesService
      .getEntityList()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((values: DeviceEntity[]) => {
          this._availableTargets = values.map((value: DeviceEntity) => {
            return {
              id: value.instance,
              caption: value.number,
              type:'token'
            };
          });
        })
      )
      .subscribe();
  }

  _onCreateNewTargetValue(newValue: string) {
    let self=  this;
    self._creatingTarget.id = "{test-create-target-555ec657-aa2d-436c-913f-ff5507f6acff}";
    self._creatingTarget.caption = newValue;
    self._creatingTarget.type = "token";

    this._selectedTarget = [self._creatingTarget];
    let found = this._availableTargets.find((item: ReferenceSelectorItem) => item.id === self._creatingTarget.id);
    if (found === undefined) {
      this._availableTargets.push(self._creatingTarget);
    }
  }

  public _getErrorText(controlName: string): string {
    let errorText = "",
      control = this._formGroup.controls[controlName];

    if (control.errors) {
      errorText = "!";
      let errors: ValidationErrors = control.errors;

      if (controlName === "testName") {
        if (errors["required"] !== undefined) {
          errorText = "Test name is required";
        }
      } else if (controlName === "testTarget") {
        if (errors["required"] !== undefined) {
          errorText = "Test target is required";
        }
      } else if (controlName === "cronCommand") {
        if (errors["required"] !== undefined) {
          errorText = "Cron command is required";
        }
      }
    }
    return errorText;
  }

  _onEditConfirm() {
    if (this.creatingMode) {
      this._testModel.target = this._selectedTarget && this._selectedTarget[0] && this._selectedTarget[0].caption;
      this._testModel.command = "call";

      this.testCreateSubscription$ && this.testCreateSubscription$.unsubscribe();
      this.testCreateSubscription$ = this.testsService
        .createEntity(this._testModel)
        .pipe(
          takeUntil(this.stopSubscription$),
          tap((data: PostOperationResult<TestEntity>) => {
            if (data.opResult) {
              let createdTest: TestEntity = this.modelService.deepCopy(data.opEntity);
              this.onCreateComplete.emit(createdTest);
              this._close();
            } else {
              this._operationErrorText = "Failed o create test";
            }
          })
        )
        .subscribe()
    }
    else {
      this._testModel.target = this._selectedTarget && this._selectedTarget[0] && this._selectedTarget[0].caption;

      this.testUpdateSubscription$ && this.testUpdateSubscription$.unsubscribe();
      this.testUpdateSubscription$ = this.testsService
        .updateEntityById(this._testID, this._testModel)
        .pipe(
          takeUntil(this.stopSubscription$),
          tap((data: PutOperationResult<TestEntity>) => {
            if (data.opResult) {
              let updateTest: UpdateTestEntity = {
                entityID: this._testID,
                value: this._testModel
              }
              this.onEditComplete.emit(updateTest);
              this._close();
            }
            else {
              this._operationErrorText = "Failed o update test information";
            }
          })
        )
        .subscribe();
    }
  }

  protected _close() {
    this.resetFormData();
    super._close();
  }

  private resetFormData() {
    this._formGroup.updateValueAndValidity();
    this._formGroup.reset();
    this._formGroup.markAsUntouched();
    this._formGroup.patchValue({
      "cronCommand": TEST_DEFAULT_CRON
    });
  }


  private createNewReference(newValue: string) {
    let self=  this;
    self._creatingTarget.id = "{test-create-target-555ec657-aa2d-436c-913f-ff5507f6acff}";
    self._creatingTarget.caption = newValue;
    self._creatingTarget.type = "token";

    this._selectedTarget = [self._creatingTarget];
    let found = this._availableTargets.find((item: ReferenceSelectorItem) => item.id === self._creatingTarget.id);
    if (found === undefined) {
      this._availableTargets.push(self._creatingTarget);
    }
  }
}
