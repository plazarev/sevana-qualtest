import {Component, ViewChild} from "@angular/core";
import {AppConfigService} from "../../../common/services/app-config.service";
import {Subject, Subscription} from "rxjs/index";
import {takeUntil, tap} from "rxjs/internal/operators";
import {CountResult, DeleteOperationResult} from "../../../common/services/entity.service";
import {QueryParam} from "../../../common/models/query-param.type";
import {ApiPath} from "../../../common/services/api.path";

import {ModelService} from "../../../common/services/model.service";
import {NotificationService} from "../../../common/services/notification.service";
import {TestEntity, UpdateTestEntity} from "../../../common/models/entity/test-entity.model";
import {TestCardModel} from "../model/test-card.model";
import {TestsService} from "../tests.service";
import {PagedComponent} from "../../../components/base/paged.component";
import {TestEditComponent} from "./test-edit.component";
import {ConfirmPopupComponent} from "../../../components/confirm-popup/confirm-popup.component";
import * as moment from "moment";
import {Router} from "@angular/router";
import {Progress} from "../../../common/models/progress.model";
import {
  Statistics24Entity, StatisticsDataEntry, StatisticsDataStyledVisualEntry,
  StatisticsEntityBase, SummaryStatisticsItem
} from "../../../common/models/entity/statistics-entity.model";
import {ReportRequestParametersModel} from "../../reports/model/report-request-parameters.model";
import {ReportsService} from "../../reports/reports.service";

@Component({
  selector: "sq-tests-full-list",
  templateUrl: "tests-full-list.component.html",
  host: {"[class.sq-tests-full-list]": "true"}
})
export class TestsFullListComponent extends PagedComponent {

  private _testSearchValue: string;
  private _tests: TestEntity[];
  private _filteredTests: string[];

  private _testsStatisticsLoading: Progress = 'loading';
  private _testsFullStatistics: Map<string, SummaryStatisticsItem> = new Map();
  private _testsSummaryStatistics: Map<string, StatisticsEntityBase<StatisticsDataEntry>>;
  private _testsSummaryVisualStatistics: Map<string, StatisticsEntityBase<StatisticsDataStyledVisualEntry>>;

  private _testDeleteID: string;

  private stopSubscription$ = new Subject<boolean>();
  private testsCountSubscription$: Subscription;
  private testsListSubscription$: Subscription;
  private testsStatisticsSubscription$: Subscription;
  private testDeleteSubscription$: Subscription;

  @ViewChild("testEditPopup")
  testEditPopup: TestEditComponent;

  @ViewChild("testDeleteConfirmPopup")
  testDeleteConfirmPopup: ConfirmPopupComponent<TestEntity>;

  constructor(private notificationService: NotificationService,
              private testsService: TestsService,
              private reportsService: ReportsService,
              private modelService: ModelService,
              protected configService: AppConfigService,
              private router: Router) {
    super(configService);
  }

  public loadPageData(currentPage: number, pageSize: number): void {
    this.retrieveTestsList(currentPage, pageSize);
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  _onSearchValueChange(value: string) {
    this.filterTests();
  }

  private filterTests() {
    if (this._tests) {
      this._filteredTests = this._tests.filter((value: TestEntity) => {
        if (this._testSearchValue) {
          return value.name.indexOf(this._testSearchValue) >= 0 ||
            value.target.indexOf(this._testSearchValue) >= 0;
        }
        return true;
      })
        .map((entity: TestEntity) => {
          return entity.name;
        });
    }
    else {
      this._filteredTests = [];
    }
  }

  _onAddNewClicked(event: MouseEvent): void {
    this.testEditPopup.creatingMode = true;
    this.testEditPopup.show();
  }

  _onEditCard(testCard: TestCardModel) {
    this.testEditPopup.creatingMode = false;
    this.testEditPopup.testModel = testCard.originalEntity;
    this.testEditPopup.show();
  }

  _onDeleteCard(testCard: TestCardModel) {
    this._testDeleteID = testCard.originalEntity.name;
    this.testDeleteConfirmPopup.userData = testCard.originalEntity;
    this.testDeleteConfirmPopup.show();
  }

  _onChartCard(testCard: TestCardModel) {
    let period: moment.unitOfTime.DurationConstructor = 'h';
    if (this.testsConfig.defaultPeriod === 'hour') {
      period = 'h';
    }
    else if (this.testsConfig.defaultPeriod === 'day') {
      period = 'd';
    }
    else if (this.testsConfig.defaultPeriod === 'month') {
      period = 'M';
    }
    let dateFrom: Date = moment().subtract(this.testsConfig.defaultDuration, period).toDate();
    let dateTo: Date = new Date();

    this.router.navigate(['/reports/view'], {
      queryParams: {
        period: this.testsConfig.defaultPeriod,
        from: dateFrom.getTime(),
        to: dateTo.getTime(),
        devices: [],
        tests: [testCard.originalEntity.name]
      }
    });
  }

  _onProbeStatistics(probeID: string) {
    this.router.navigate(['/reports/view'], {
      queryParams: {
        probe: probeID
      }
    });
  }

  _onExpandCard(testCard: TestCardModel) {
  }

  _onTestCreateComplete(testData: TestEntity) {
    this.retrieveTestsList(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
  }

  _onTestEditComplete(testData: UpdateTestEntity) {
    for (let test of this._tests) {
      if (test.name === testData.entityID) {
        TestEntity.copy(test, testData.value);
      }
    }
  }

  _onTestCancel() {
    this._testDeleteID = undefined;
  }

  _onTestDelete(testData: TestEntity) {
    let testID = testData.name;
    this.testDeleteSubscription$ && this.testDeleteSubscription$.unsubscribe();
    this.testDeleteSubscription$ = this.testsService
      .deleteEntity(testID)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((data: DeleteOperationResult) => {
          if (data.opResult) {
            //delete entity
            let deletedIndex: number = this.modelService.findInArray(this._tests,
              (item: TestEntity) => {
                return (item.name === testID);
              });
            if (deletedIndex >= 0) {
              this._tests.splice(deletedIndex, 1);
            }

          } else {
            this.notificationService.pushNotification({
              type: "error",
              caption: "Deletion error",
              content: `Fail to delete test '{testID}'`
            })
          }
        })
      )
      .subscribe();

    this._testDeleteID = undefined;
  }

  protected retrieveTestsList(currentPage: number, pageSize: number): void {
    this._pageLoadingStatus = 'loading';
    this.testsCountSubscription$ && this.testsCountSubscription$.unsubscribe();
    this.testsCountSubscription$ = this.testsService
      .getEntitiesCount()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((result: CountResult) => {
          this.pagingConfig.totalCount = result.totalcount;
          this._pageLoadingStatus = 'loaded';
          this.retrieveTestsListPage(currentPage, pageSize);
        })
      )
      .subscribe();
  }

  protected retrieveTestsListPage(currentPage: number, pageSize: number): void {
    this._pageLoadingStatus = 'loading';
    let queryParams: QueryParam[] = [
      {
        key: ApiPath.OFFSET,
        value: (currentPage-1) * pageSize
      },
      {
        key: ApiPath.LIMIT,
        value: pageSize
      }
    ];

    this.testsListSubscription$ && this.testsListSubscription$.unsubscribe();
    this.testsListSubscription$ = this.testsService
      .getEntityList(queryParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((tests: TestEntity[]) => {
          this._pageLoadingStatus = 'loaded';
          this._tests = tests;

          this.retrieveTestsStatistics();
        })
      )
      .subscribe();
  }

  private retrieveTestsStatistics() {
    this._testsStatisticsLoading = 'loading';

    let period: moment.unitOfTime.DurationConstructor = 'h';
    if (this.testsConfig.defaultPeriod === 'hour') {
      period = 'h';
    }
    else if (this.testsConfig.defaultPeriod === 'day') {
      period = 'd';
    }
    else if (this.testsConfig.defaultPeriod === 'month') {
      period = 'M';
    }
    let dateFrom: Date = moment().subtract(this.testsConfig.defaultDuration, period).toDate();
    let dateTo: Date = new Date();
    let testsList = this._tests.map((test: TestEntity) => {
      return test.name;
    });

    let reportParams: ReportRequestParametersModel = {
      startDate: dateFrom,
      endDate: dateTo,
      scale: this.testsConfig.defaultPeriod,
      united: false,
      phonesList: [],
      testsList: testsList
    };

    this.testsStatisticsSubscription$ && this.testsStatisticsSubscription$.unsubscribe();
    this.testsStatisticsSubscription$ = this.reportsService
      .getTasksStatistics(reportParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((statistics: Map<string, SummaryStatisticsItem>) => {

          if (statistics) {
            this._testsFullStatistics = statistics;
          } else {
            this._testsFullStatistics = new Map();
          }

          this._testsSummaryStatistics = this.reportsService.convertSummaryStatisticsPerItem(testsList, this._testsFullStatistics);
          this._testsSummaryVisualStatistics = this.reportsService.calculateSummaryVisualStatisticsPerItem(this._testsSummaryStatistics);

          this._testsStatisticsLoading = 'loaded';
        })
      )
      .subscribe();
  }
}
