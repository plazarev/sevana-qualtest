export interface TokenModel {
  session_token?: string;
  lifetime?: number;
  role?: string;
  errormsg?: string;

  user?: string;
  expiresIn?: number
}
