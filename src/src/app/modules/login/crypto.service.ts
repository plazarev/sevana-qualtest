import {Injectable} from "@angular/core";
import * as Crypto from "crypto-js";

@Injectable()
export class CryptoService {

  encrypt(message: string): any {
    return Crypto.SHA256(message);
  }
}
