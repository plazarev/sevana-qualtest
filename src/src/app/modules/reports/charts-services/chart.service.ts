import {Injectable, Injector} from "@angular/core";
import {StatisticsEntity} from "../../../common/models/entity/statistics-entity.model";
import {PhoneCharts} from "../model/chart.model";
import {DatePipe} from "@angular/common";
import {ChartPeriodModel} from "../model/chart-period.model";

@Injectable()
export abstract class ChartService<T> {
  private datePipe: DatePipe;

  public abstract convertEntitiesToModel(values: StatisticsEntity[], mode: string): PhoneCharts<T>[]
  public abstract createLineChart(phone: string, type: string, mode: string): T;

  constructor(protected injector: Injector) {
    this.datePipe = injector.get(DatePipe);
  }

  protected getAxesLabel(mode: string): string {
    if (mode === ChartPeriodModel.PERIOD_HOURS) {
      return "Time";
    } else if (mode === ChartPeriodModel.PERIOD_DAYS) {
      return "Days";
    } else if (mode === ChartPeriodModel.PERIOD_MONTHS) {
      return "Months";
    }
    return "Date";
  }

  protected getLabel(time: number, mode: string): string {
    let date: Date = new Date(time*1000);
    if (mode === ChartPeriodModel.PERIOD_HOURS) {
      return this.datePipe.transform(date, "yyyy-MM-dd hh:mm:ss");
    } else if (mode === ChartPeriodModel.PERIOD_DAYS) {
      return this.datePipe.transform(date, "yyyy-MM-dd");
    } else if (mode === ChartPeriodModel.PERIOD_MONTHS) {
      return this.datePipe.transform(date, "MMMM, yyyy");
    }
    return this.datePipe.transform(date, "yyyy-MM-dd hh:mm:ss");
  }
}
