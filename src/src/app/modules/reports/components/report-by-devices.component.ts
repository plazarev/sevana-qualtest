import {Component} from "@angular/core";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {Subscription} from "rxjs/index";
import {ReportsService} from "../reports.service";
import {StatisticsEntity} from "../../../common/models/entity/statistics-entity.model";
import {ChartDataModel, ChartEntity, ChartType, PhoneCharts} from "../model/chart.model";
import {DatePipe} from "@angular/common";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ReportBaseComponent} from "./report-base.component";
import {ReportParametersModel} from "../model/report-parameters.model";
import {ModelService} from "../../../common/services/model.service";
import {HighchartService} from "../charts-services/highchart.service";
import {HighChartModel} from "../../../../ux-lib/components/charts/highchart/model/highchart.model";
import {StatisticDataType} from "../../../common/models/statistic-data.type";
import {StatisticPeriodType} from "../../../common/models/statistic-period.type";

@Component({
  selector: "sq-report-by-devices",
  templateUrl: "report-by-devices.component.html",
  host: {"[class.sq-report-by-devices]": "true"}
})
export class ReportByDevicesComponent extends ReportBaseComponent {

  //_phonesCharts:PhoneCharts<ChartSpec>[] = [];
  _phonesCharts:PhoneCharts<HighChartModel>[] = [];

  _lastFilterError: string;

  private reportDataSubscription: Subscription;

  constructor(protected configService: AppConfigService,
              private reportsService: ReportsService,
              private modelService: ModelService,
              private datePipe: DatePipe,
              private chartService: HighchartService) {
    super(configService);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.reportDataSubscription && this.reportDataSubscription.unsubscribe();
  }

  public onApplyReport(reportParameters: ReportParametersModel): boolean {
    this._lastFilterError = undefined;
    this._phonesCharts = [];

    if (reportParameters.range.from === undefined || reportParameters.range.from === null ||
        reportParameters.range.to === undefined || reportParameters.range.to === null) {
      reportParameters.rangeError = "Please choose report range";
      this._lastFilterError = reportParameters.rangeError;
      return false;
    }

    let united: boolean = reportParameters.selectedReportType.id === "union";
    let phonesList: string[] = reportParameters.selectedDevices.map((device: UxMultipleFieldItemModel) => {
      return device.id;
    });
    if (phonesList.length === 0) {
      reportParameters.devicesError = "Please choose at least one device";
      this._lastFilterError = reportParameters.devicesError;
      return false;
    }

    this.inProgress = true;
    this.reportDataSubscription && this.reportDataSubscription.unsubscribe();
    this.reportDataSubscription = this.reportsService
      .getReportData(reportParameters.range.from,
        reportParameters.range.to,
        reportParameters.selectedPeriod.id,
        united,
        phonesList, [])
      .subscribe((values: StatisticsEntity[]) => {

        let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
        chartTypes.set("mos_pvqa", ["linear"]);
        chartTypes.set("mos_aqua", ["linear"]);

        this._phonesCharts = this.chartService.generateCharts(chartTypes, <StatisticPeriodType>reportParameters.selectedPeriod.id, values, "");
        this.inProgress = false;
    })
    return true;
  }

  public onResetReport(reportParameters: ReportParametersModel): void {
    reportParameters.rangeError = undefined;
    reportParameters.devicesError = undefined;
    this._lastFilterError = undefined;
    this._phonesCharts = [];
  }
}
