import {Component, Input} from "@angular/core";
import {
  StatisticsDataEntry, StatisticsDataStyledVisualEntry,
  StatisticsEntityBase
} from "../../../common/models/entity/statistics-entity.model";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {AQuAValueItem, ReportAquaModel} from "../../../common/models/entity/report-aqua.model";
import {CssService} from "../../../common/services/css.service";
import {ChartModel} from "../model/chart.model";
import {HighChartModel} from "../../../../ux-lib/components/charts/highchart/model/highchart.model";
import {HighchartService} from "../charts-services/highchart.service";
import * as csvtojson from 'csvtojson';
import {PvqaReportModel} from "../../../common/models/entity/report-pvqa.model";

@Component({
  selector: "sq-testcase-chart",
  templateUrl: "testcase-chart.component.html",
  host: {"[class.sq-testcase-chart]": "true"}
})
export class TestcaseChartComponent {

  private _result: TestResultEntity;
  private _aquaReport: ReportAquaModel<number>;
  private _aquaReportVisual: ReportAquaModel<string>;
  private _pvqaReport: PvqaReportModel<number>;
  private _pvqaReportVisual: PvqaReportModel<string>;
  private _spectrumChart: ChartModel<HighChartModel>;
  private _pvqaStatChart: ChartModel<HighChartModel>;

  @Input()
  public set result(value: TestResultEntity) {
    this._result = value;
    this.parseAquaReport();
    this.parsePvqaReport();
  }

  public get result(): TestResultEntity {
    return this._result;
  }

  constructor(protected cssStyle: CssService,
              private chartService: HighchartService) {
  }

  private parseAquaReport() {
    if (this._result.report_aqua && this._result.report_aqua.length > 0) {
      let parsedData = JSON.parse(this._result.report_aqua);
      if (parsedData && parsedData["AQuAReport"]) {
        this._aquaReport = <ReportAquaModel<number>>parsedData;
        this._aquaReport = this.checkForUp(this._aquaReport);
        this._aquaReportVisual = this.convertAquaReport(this._aquaReport);
        this._aquaReportVisual.AQuAReport.QualityResults.mosStyle = this.cssStyle.getLevel(this._aquaReport.AQuAReport.QualityResults.MOS);

        this._spectrumChart = this.chartService.generateSpectrumChart(this._result.id, this._result.endtime, this._aquaReport);
      }
    }
  }

  private parsePvqaReport() {
    this._pvqaReport = {
      mos_pvqa: this._result.mos_pvqa,
      endtime: this._result.endtime
    }
    this._pvqaReportVisual = this.convertPvqaReport(this._pvqaReport);
    this._pvqaReportVisual.mosStyle = this.cssStyle.getLevel(this._pvqaReport.mos_pvqa);
    this._pvqaReportVisual.endtime = this._result.endtime;

    if (this._result.report_pvqa && this._result.report_pvqa.length > 0) {
      csvtojson({
        delimiter: ";"
      })
        .fromString(this._result.report_pvqa)
        .then((jsonData)=>{
          if (Array.isArray(jsonData) && jsonData.length > 0) {
            this._pvqaStatChart = this.chartService.generatePvqaStatisticsChart(this._result.id, this._result.endtime, jsonData);
          }
        });
    }
  }

  private checkForUp(original: ReportAquaModel<number>) {
    if (original && original.AQuAReport && original.AQuAReport.ValuesArray) {
      for (let key in original.AQuAReport.ValuesArray) {
        let value: AQuAValueItem<number> = original.AQuAReport.ValuesArray[key];
        if (typeof value.DegradedValue === 'number') {
          if (value.DegradedValue > value.SourceValue) {
            value.up = true;
          }
          else if (value.DegradedValue < value.SourceValue) {
            value.up = false;
          }
        }
      }
    }
    return original;
  }

  private convertAquaReport(original: ReportAquaModel<number>): ReportAquaModel<string> {
    let converted: ReportAquaModel<string> = this.convertField(original);
    return converted;
  }

  private convertPvqaReport(original: PvqaReportModel<number>): PvqaReportModel<string> {
    let converted: PvqaReportModel<string> = this.convertField(original);
    return converted;
  }

  private convertField(value: any): any {
    if (value === undefined || value === null) {
      return value;
    }
    else if (typeof value === 'string') {
      return value;
    }
    else if (typeof value === 'number') {
      if (Number.isInteger(value)) {
        return value.toFixed(0);
      }
      else {
        return value.toFixed(2);
      }
    }
    else if (Array.isArray(value)) {
      let convertedValue = [];
      for (let item of value) {
        convertedValue.push(this.convertField(item));
      }
      return convertedValue;
    }
    else if (typeof value === 'object') {
      let convertedValue = {};
      for (let key in value) {
        convertedValue[key] = this.convertField(value[key]);
      }
      return convertedValue;
    }

    return value;
  }
}
