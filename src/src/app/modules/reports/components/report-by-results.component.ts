import {Component, Input, TemplateRef, ViewChild} from "@angular/core";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {DevicesService} from "../../devices/devices.service";
import {Subscription} from "rxjs/index";
import {ReportsService} from "../reports.service";
import {
  StatisticsDataEntry,
  StatisticsEntity
} from "../../../common/models/entity/statistics-entity.model";
import {DatePipe} from "@angular/common";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ReportBaseComponent} from "./report-base.component";
import {ReportParametersModel} from "../model/report-parameters.model";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {UxAccordionToggleEvent} from "../../../../ux-lib/components/accordion/accordion.component";
import {UxCardTreeModel} from "../../../../ux-lib/components/card-tree/card-tree.model";
import {ResultsService} from "../../results/results.service";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {ReportCardModel} from "../model/report-card.model";
import {ModelService} from "../../../common/services/model.service";
import {UxCardTreeCheckedEvent} from "../../../../ux-lib/components/card-tree/card-tree.component";
import {UxTabChangeEvent} from "../../../../ux-lib/components/tab-panel/tab-panel-event.model";
import {HighchartService} from "../charts-services/highchart.service";
import {ChartType, PhoneCharts} from "../model/chart.model";
import {HighChartModel} from "../../../../ux-lib/components/charts/highchart/model/highchart.model";
import {StatisticDataType} from "../../../common/models/statistic-data.type";
import {StatisticPeriodType} from "../../../common/models/statistic-period.type";

@Component({
  selector: "sq-report-by-results",
  templateUrl: "report-by-results.component.html",
  host: {"[class.sq-report-by-results]": "true"}
})
export class ReportsByResultsComponent extends ReportBaseComponent {

  private _availableTests: TestEntity[] = []
  @Input()
  public set availableTests(items: TestEntity[]) {
    this._availableTests = items;
    this.updateTestsList();
  }

  public get availableTests(): TestEntity[] {
    return this._availableTests;
  }

  openedViewItem: number = 0; // Tests tab
  testsTabTitle: string = "";

  _cardsModel: UxCardTreeModel;

  @ViewChild("commonTemplate")
  private commonTemplate: TemplateRef<any>;

  private resultsSubscription: Subscription;
  private testsResults: TestResultEntity[] = [];

  private isReportActivated = false;
  private selectedTests: string[] = [];

  constructor(protected configService: AppConfigService,
              private devicesService: DevicesService,
              private resultsService: ResultsService,
              private reportsService: ReportsService,
              private modelService: ModelService,
              private chartService: HighchartService,
              private datePipe: DatePipe) {
    super(configService);
  }

  ngOnInit(): void {
    this.testsTabTitle = "Tests";
    this._cardsModel = {
      template: this.commonTemplate,
      children: []
    };
    this.updateTestsList();
  }

  ngOnDestroy(): void {
    this.resultsSubscription && this.resultsSubscription.unsubscribe();
    this.resetTestsList();
  }

  private updateTestsList() {
    if (this._cardsModel) {
      this.resetTestsList();
      let _cardsModel: UxCardTreeModel = {
        template: this.commonTemplate,
        children: this.availableTests.map((test: TestEntity) => {
          return <ReportCardModel> {
            id: test.name,
            selected: false,
            visible: true,
            caption: test.name,
            test: test,
            hasTestResults: false,
            isLoadInProgress: false,
            hasChildren: false
          }
        })
      };

      this._cardsModel = _cardsModel;
    }
  }

  private resetTestsList() {
    if (this._cardsModel && this._cardsModel.children) {
      this._cardsModel.children.forEach((value: ReportCardModel) => {
        value.isLoadInProgress = false;
        value.statisticsSubscription && value.statisticsSubscription.unsubscribe();
        value.probesSubscription && value.probesSubscription.unsubscribe();
      })
    }
  }

  public onApplyReport(reportParameters: ReportParametersModel): boolean {
    if (reportParameters.range.from === undefined || reportParameters.range.from === null ||
      reportParameters.range.to === undefined || reportParameters.range.to === null) {
      reportParameters.rangeError = "Please choose report range";
      return false;
    }

    this.runReport(reportParameters);
    let useAll: boolean = this.selectedTests.length === 0;
    this.isReportActivated = true;

    return true;
  }

  public onResetReport(reportParameters: ReportParametersModel): void {
  }

  _onReportViewChanges(event: UxAccordionToggleEvent) {
    if (event.opened) {
      this.openedViewItem = event.index
    }
  }

  private runReport(reportParameters: ReportParametersModel) {
    let phonesList: string[] = reportParameters.selectedDevices.map((device: UxMultipleFieldItemModel) => {
      return device.id;
    });

    let from: number = reportParameters.range.from.getTime();
    let to: number = reportParameters.range.to.getTime();
    let united: boolean = reportParameters.selectedReportType.id === "union";

    if (this._cardsModel && this._cardsModel.children) {
      let useAll: boolean = this.selectedTests.length === 0;

      this._cardsModel.children.forEach((node: ReportCardModel) => {
        if (useAll || node.selected) {
          node.isLoadInProgress = true;
          node.statisticsSubscription && node.statisticsSubscription.unsubscribe();
          node.statisticsSubscription = this.reportsService.getReportData(
            reportParameters.range.from,
            reportParameters.range.to,
            reportParameters.selectedPeriod.id,
            united,
            phonesList,
            [node.test.name])
            .subscribe((statistics: StatisticsEntity[]) => {
              node.reportParameters = reportParameters;
              if (statistics) {
                node.statisticsResults = statistics;
              } else {
                node.statisticsResults = [];
              }

              node.pvqaCharts = [];
              node.aquaCharts = [];
              node.rFactorCharts = [];
              node.mosNetworkCharts = [];

              this.calculateSummaryStatistics(node);
              node.isLoadInProgress = false;
            });
        }
        else {
          node.visible = false;
        }
      })
    }
  }

  private calculateSummaryStatistics(value: ReportCardModel): void {
    if (value && value.statisticsResults) {
      value.statisticsResults.forEach((statValue: StatisticsEntity) => {
        if (statValue.mos_pvqa.Count > 0) {
          if (!value.pvqaSummary) {
            value.pvqaSummary = <StatisticsDataEntry> this.modelService.deepCopy(statValue.mos_pvqa);
          } else {
            value.pvqaSummary.Sum += statValue.mos_pvqa.Sum;
            value.pvqaSummary.Count += statValue.mos_pvqa.Count;
            if (statValue.mos_pvqa.min < value.pvqaSummary.min) {
              value.pvqaSummary.min = statValue.mos_pvqa.min;
            }
            if (statValue.mos_pvqa.max > value.pvqaSummary.max) {
              value.pvqaSummary.max = statValue.mos_pvqa.max;
            }
          }
        }

        if (statValue.mos_aqua.Count > 0) {
          if (!value.aquaSummary) {
            value.aquaSummary = <StatisticsDataEntry> this.modelService.deepCopy(statValue.mos_aqua);
          } else {
            value.aquaSummary.Sum += statValue.mos_aqua.Sum;
            value.aquaSummary.Count += statValue.mos_aqua.Count;
            if (statValue.mos_aqua.min < value.aquaSummary.min) {
              value.aquaSummary.min = statValue.mos_aqua.min;
            }
            if (statValue.mos_aqua.max > value.aquaSummary.max) {
              value.aquaSummary.max = statValue.mos_aqua.max;
            }
          }
        }

        if (statValue.r_factor.Count > 0) {
          if (!value.rFactorSummary) {
            value.rFactorSummary = <StatisticsDataEntry> this.modelService.deepCopy(statValue.r_factor);
          } else {
            value.rFactorSummary.Sum += statValue.r_factor.Sum;
            value.rFactorSummary.Count += statValue.r_factor.Count;
            if (statValue.r_factor.min < value.rFactorSummary.min) {
              value.rFactorSummary.min = statValue.r_factor.min;
            }
            if (statValue.r_factor.max > value.rFactorSummary.max) {
              value.rFactorSummary.max = statValue.r_factor.max;
            }
          }
        }

        if (statValue.mos_network.Count > 0) {
          if (!value.mosNetworkSummary) {
            value.mosNetworkSummary = <StatisticsDataEntry> this.modelService.deepCopy(statValue.mos_network);
          } else {
            value.mosNetworkSummary.Sum += statValue.mos_network.Sum;
            value.mosNetworkSummary.Count += statValue.mos_network.Count;
            if (statValue.mos_network.min < value.mosNetworkSummary.min) {
              value.mosNetworkSummary.min = statValue.mos_network.min;
            }
            if (statValue.mos_network.max > value.mosNetworkSummary.max) {
              value.mosNetworkSummary.max = statValue.mos_network.max;
            }
          }
        }
      });

      if (value.pvqaSummary) {
        value.pvqaSummary.average = value.pvqaSummary.Sum / value.pvqaSummary.Count;
        value.pvqaVisibleSummary = {
          Sum: value.pvqaSummary.Sum.toFixed(0),
          Count: value.pvqaSummary.Count.toFixed(0),
          average: value.pvqaSummary.average.toFixed(2),
          min: value.pvqaSummary.min.toFixed(2),
          max: value.pvqaSummary.max.toFixed(2),
          avgLevel: this.getLevel(value.pvqaSummary.average),
          minLevel: this.getLevel(value.pvqaSummary.min),
          maxLevel: this.getLevel(value.pvqaSummary.max)
        }
        value.hasTestResults = true;
      }

      if (value.aquaSummary) {
        value.aquaSummary.average = value.aquaSummary.Sum / value.aquaSummary.Count;
        value.aquaVisibleSummary = {
          Sum: value.aquaSummary.Sum.toFixed(0),
          Count: value.aquaSummary.Count.toFixed(0),
          average: value.aquaSummary.average.toFixed(2),
          min: value.aquaSummary.min.toFixed(2),
          max: value.aquaSummary.max.toFixed(2),
          avgLevel: this.getLevel(value.aquaSummary.average),
          minLevel: this.getLevel(value.aquaSummary.min),
          maxLevel: this.getLevel(value.aquaSummary.max)
        }
        value.hasTestResults = true;
      }

      if (value.rFactorSummary) {
        value.rFactorSummary.average = value.rFactorSummary.Sum / value.rFactorSummary.Count;
        value.rFactorVisibleSummary = {
          Sum: value.rFactorSummary.Sum.toFixed(0),
          Count: value.rFactorSummary.Count.toFixed(0),
          average: value.rFactorSummary.average.toFixed(2),
          min: value.rFactorSummary.min.toFixed(2),
          max: value.rFactorSummary.max.toFixed(2),
          avgLevel: this.getLevel(value.rFactorSummary.average),
          minLevel: this.getLevel(value.rFactorSummary.min),
          maxLevel: this.getLevel(value.rFactorSummary.max)
        }
        value.hasTestResults = true;
      }

      if (value.mosNetworkSummary) {
        value.mosNetworkSummary.average = value.mosNetworkSummary.Sum / value.mosNetworkSummary.Count;
        value.mosNetworkVisibleSummary = {
          Sum: value.mosNetworkSummary.Sum.toFixed(0),
          Count: value.mosNetworkSummary.Count.toFixed(0),
          average: value.mosNetworkSummary.average.toFixed(2),
          min: value.mosNetworkSummary.min.toFixed(2),
          max: value.mosNetworkSummary.max.toFixed(2),
          avgLevel: this.getLevel(value.mosNetworkSummary.average),
          minLevel: this.getLevel(value.mosNetworkSummary.min),
          maxLevel: this.getLevel(value.mosNetworkSummary.max)
        }
        value.hasTestResults = true;
      }
    }
  }

  private getLevel(value: number): string {
    if (value < 2.6) {
      return "_level_0";
    } else if (value < 3.1) {
      return "_level_1";
    } else if (value < 3.6) {
      return "_level_2";
    } else if (value < 4.0) {
      return "_level_3";
    } else if (value < 4.3) {
      return "_level_4";
    } else {
      return "_level_5";
    }
  }

  private loadResultsData(reportParameters: ReportParametersModel) {
    this.resultsSubscription && this.resultsSubscription.unsubscribe();
    this.resultsSubscription = this.resultsService.getEntityList()
      .subscribe((results: TestResultEntity[]) => {
        this.testsResults = results;
      })
    ;
  }

  _onNodeExpand(node: ReportCardModel) {
    if (node.isExpanded) {
      node.isExpanded = false;
    } else {
      node.isExpanded = true;
    }
  }

  _onBackToTests() {
    if (this._cardsModel && this._cardsModel.children) {
      this._cardsModel.children.forEach((value: ReportCardModel) => {
        value.visible = true;
        value.selected = false;
        value.hasTestResults = false;
      })
    }

    this.isReportActivated = false;
  }

  _onCardChecked(event: UxCardTreeCheckedEvent) {
    this.selectedTests = this.selectedTests.filter((value: string) => value !== event.model.id);

    if (event.model.selected) {
      this.selectedTests.push(event.model.id);
    }
  }

  _onDetailsTabChange(tabEvent: UxTabChangeEvent, node: ReportCardModel) {
    let tabName = this.getTabName(node, tabEvent.index);

    if (tabName === "pvqaSummary") {
      this.updatePvqaCharts(node);
    }

    if (tabName === "aquaSummary") {
      this.updateAquaCharts(node);
    }

    if (tabName === "rFactorSummary") {
      this.updateRFactorCharts(node);
    }

    if (tabName === "mosNetworkSummary") {
      this.updateMosNetworkCharts(node);
    }

    if (tabName === "tests") {
      this.requestTestResults(node);
    }
  }

  private requestTestResults(node: ReportCardModel) {
    let phonesList: string[] = node.reportParameters.selectedDevices.map((device: UxMultipleFieldItemModel) => {
      return device.id;
    });

    node.probesInProgress = true;
    node.probesSubscription && node.probesSubscription.unsubscribe();
    node.probesSubscription = this.resultsService.getResults(
        node.reportParameters.range.from,
        node.reportParameters.range.to,
        [node.test.name],
        phonesList)
      .subscribe((probes: TestResultEntity[]) => {

        if (probes) {
          node.probesResults = probes;

        } else {
          node.probesResults = [];
        }

        node.probesInProgress = false;
      });
  }

  private updatePvqaCharts(node: ReportCardModel) {
    node.chartsInProgress = true;
    setTimeout(() => {
      let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
      chartTypes.set("mos_pvqa", ["linear", "calendar"]);

      node.pvqaCharts = this.chartService.generateCharts(chartTypes,
        <StatisticPeriodType>node.reportParameters.selectedPeriod.id,
        node.statisticsResults,
        "MOS distribution for");

      node.chartsInProgress = false;
    }, 0);
  }

  private updateAquaCharts(node: ReportCardModel) {
    node.chartsInProgress = true;
    setTimeout(() => {
      let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
      chartTypes.set("mos_aqua", ["linear", "calendar"]);

      node.aquaCharts = this.chartService.generateCharts(chartTypes,
        <StatisticPeriodType>node.reportParameters.selectedPeriod.id,
        node.statisticsResults,
        "MOS distribution for");

      node.chartsInProgress = false;
    }, 0);
  }

  private updateRFactorCharts(node: ReportCardModel) {
    node.chartsInProgress = true;
    setTimeout(() => {
      let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
      chartTypes.set("r_factor", ["linear", "calendar"]);

      node.rFactorCharts = this.chartService.generateCharts(chartTypes,
        <StatisticPeriodType>node.reportParameters.selectedPeriod.id,
        node.statisticsResults,
        "Sevana R-factor distribution for");

      node.chartsInProgress = false;
    }, 0);
  }

  private updateMosNetworkCharts(node: ReportCardModel) {
    node.chartsInProgress = true;
    setTimeout(() => {
      let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
      chartTypes.set("mos_network", ["linear", "calendar"]);

      node.mosNetworkCharts = this.chartService.generateCharts(chartTypes,
        <StatisticPeriodType>node.reportParameters.selectedPeriod.id,
        node.statisticsResults,
        "MOS distribution for");

      node.chartsInProgress = false;
    }, 0);
  }

  private isValidProbe(probe: TestResultEntity): boolean {
    return probe.mos_pvqa > 0 ||
      probe.mos_aqua > 0 ||
      probe.mos_network > 0 ||
      probe.r_factor > 0;
  }

  private getTabName(node: ReportCardModel, idx: number): string {
    let tabs = ["summary", "pvqaSummary", "aquaSummary", "rFactorSummary", "mosNetworkSummary", "tests"];
    if (idx === 0) {
      return tabs[0];
    }

    tabs.shift();
    let currIndex = 1;
    do {
      let tab = tabs.shift();
      if (node[tab] !== undefined) {
        if (currIndex === idx) {
          return tab;
        }
        currIndex++;
      }

      if (tab === "tests") {
        if (currIndex === idx) {
          return tab;
        }
      }
    } while (tabs.length > 0);

    return "summary";
  }

  /** @internal */
  public _trackByFn(index: number, item: PhoneCharts<HighChartModel>): string {
    return item && item.id;
  }
}
