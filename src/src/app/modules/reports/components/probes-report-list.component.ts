
import {Component, Input} from "@angular/core";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {UxTable, UxTableColumn, UxTableRow} from "../../../../ux-lib/components/table/table.model";
import {ModelService} from "../../../common/services/model.service";

@Component({
  selector: "sq-probes-report-list",
  templateUrl: "probes-report-list.component.html",
  host: {"[class.sq-probes-report-list]": "true"}
})
export class ProbesReportListComponent {

  private _splittedItems: Map<string, TestResultEntity[]>;
  private _items: TestResultEntity[] = [];
  @Input()
  public set items(values: TestResultEntity[]) {
    if (values) {
      this._items = values.filter((value: TestResultEntity) => this.isValidProbe(value));
      this.updateTableBody();
      //this.splitItems();
    }
  }

  public get items(): TestResultEntity[] {
    return this._items;
  }

  private _groupByDevices: boolean = false;
  @Input()
  public set groupByDevices(value: boolean) {
    if (this._groupByDevices !== value) {
      this._groupByDevices = value;
      //this.splitItems();
    }
  }

  _model: UxTable = {
    header: {
      rows: []
    },
    body: {rows: []},
    emptyTableContent: `No results found`
  };

  constructor(private modelService: ModelService) {
    this.setHeader();
  }

  private splitItems() {
    let splittedItems: Map<string, TestResultEntity[]> = new Map();
    if (this._groupByDevices) {
      this._items.forEach((probe: TestResultEntity) => {
        if (this.isValidProbe(probe)) {
          if (!splittedItems.has(probe.phone_name)) {
            splittedItems.set(probe.phone_name, []);
          }
          let sortedProbes: TestResultEntity[] = splittedItems.get(probe.phone_name);
          sortedProbes.push(probe);
        }
      });
    }
    else {
      splittedItems.set("", this._items);
    }
    this._splittedItems = splittedItems;
  }

  private isValidProbe(probe: TestResultEntity): boolean {
    return probe.mos_pvqa > 0 ||
      probe.mos_aqua > 0 ||
      probe.mos_network > 0 ||
      probe.r_factor > 0;
  }

  private setHeader() {
    this._model.header.rows = [
      {
        styleClass: "_header",
        columns: [
          {
            value: "Device",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "Target",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            },
          },
          {
            value: "Test",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "Duration",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "End time",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            },
          },
          {
            value: "Mos (pvqa)",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "Mos (aqua)",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "Mos (network)",
            styleClass: "_header",
            sort: true,
            resizable: true,
            contentModel: {
              "id": "name"
            }
          },
          {
            value: "R-Factor",
            styleClass: "_header",
            sort: true,
            contentModel: {
              "id": "name"
            }
          }
        ]
      }
    ];
  }

  private updateTableBody() {
    this._model.body.rows =
      this._items.map((test: TestResultEntity) => {
        return this.getTableRow(test);
      });
  }

  private getTableRow(result: TestResultEntity): UxTableRow {
    let columns: UxTableColumn[]  = [
      {
        value: result.phone_name
      },
      {
        value: result.target
      },
      {
        value: result.task_name
      },
      {
        value: this.modelService.formatSeconds(result.duration)
      },
      {
        value: result.endtime > 0 ? new Date(result.endtime*1000).toLocaleTimeString() : ""
        //value: result.endtime > 0 ? new Date(result.endtime*1000).toLocaleString() : ""
      },
      {
        value: result.mos_pvqa === 0 ? result.mos_pvqa : result.mos_pvqa.toFixed(3)
      },
      {
        value: result.mos_aqua === 0 ? result.mos_aqua : result.mos_aqua.toFixed(3)
      },
      {
        value: result.mos_network === 0 ? result.mos_network : result.mos_network.toFixed(3)
      },
      {
        value: result.r_factor === 0 ? result.r_factor : result.r_factor.toFixed(3)
      },
    ];

    return {
      id: result.id,
      columns: columns
    };
  }
}
