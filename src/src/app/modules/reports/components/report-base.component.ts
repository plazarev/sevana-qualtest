import {Component, OnDestroy, OnInit} from "@angular/core";
import {UxRadioItemModel} from "../../../../ux-lib/components/fields/radio-group/radio/radio-field.component";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ChartPeriodModel} from "../model/chart-period.model";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {ReportParametersModel} from "../model/report-parameters.model";
import {ChartEntity} from "../model/chart.model";

@Component({
})
export abstract class ReportBaseComponent implements OnInit, OnDestroy {

  periodItemsModel: UxDropdownListItem[] = [
    {
      id: ChartPeriodModel.PERIOD_HOURS,
      label: "Hours",
      value: ChartPeriodModel.PERIOD_HOURS,
      disabled: false
    },
    {
      id: ChartPeriodModel.PERIOD_DAYS,
      label: "Days",
      value: ChartPeriodModel.PERIOD_DAYS,
      disabled: false
    },
    {
      id: ChartPeriodModel.PERIOD_MONTHS,
      label: "Months",
      value: ChartPeriodModel.PERIOD_MONTHS,
      disabled: false
    }
  ];

  reportTypesModel: UxRadioItemModel[] = [
    {
      id: "separate",
      label: "Compare separately",
      name: "separate",
      disabled: false
    },
    {
      id: "union",
      label: "Combined data",
      name: "union",
      disabled: false
    }
  ];

  public inProgress: boolean = false;


  constructor(protected configService: AppConfigService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  public abstract onApplyReport(reportParameters: ReportParametersModel): boolean;
  public abstract onResetReport(reportParameters: ReportParametersModel): void;

  /** @internal */
  public _trackByChartEntity(index: number, item: ChartEntity): string {
    return item && item.id;
  }
}
