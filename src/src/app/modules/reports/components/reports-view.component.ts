import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, ViewChild} from "@angular/core";
import {UxRadioItemModel} from "../../../../ux-lib/components/fields/radio-group/radio/radio-field.component";
import {
  UxRangeDateFieldModel,
  UxRangeDateFieldPlaceholderModel, UxRangeDateFieldViewEventModel
} from "../../../../ux-lib/components/fields/date/range/range-date-field.model";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {UxValueChangeEvent} from "../../../../ux-lib/components/fields/abstract-field.component";
import {DevicesService} from "../../devices/devices.service";
import {Subscription} from "rxjs/index";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ChartPeriodModel} from "../model/chart-period.model";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {UxAccordionToggleEvent} from "../../../../ux-lib/components/accordion/accordion.component";
import {ReportByDevicesComponent} from "./report-by-devices.component";
import {ReportsByResultsComponent} from "./report-by-results.component";
import {ReportBaseComponent} from "./report-base.component";
import {ReportParametersModel} from "../model/report-parameters.model";
import {UxSwitchItemModel} from "../../../../ux-lib/components/fields/switch-selector/switch-item.model";
import {TabChangeEvent} from "../../../../ux-lib/components/tab-panel/tab-panel-event.model";
import {TestsService} from "../../tests/tests.service";
import {TestEntity} from "../../../common/models/entity/test-entity.model";

@Component({
  selector: "sq-reports-view",
  templateUrl: "reports-view.component.html",
  host: {"[class.sq-reports-view]": "true"}
})
export class ReportsViewComponent implements AfterViewInit, OnDestroy {
  _isInitialView: boolean = false;

  periodItemsModel: UxSwitchItemModel[] = [
    {
      id: ChartPeriodModel.PERIOD_HOURS,
      label: "Hours",
      name: ChartPeriodModel.PERIOD_HOURS,
      value: ChartPeriodModel.PERIOD_HOURS,
      disabled: false
    },
    {
      id: ChartPeriodModel.PERIOD_DAYS,
      label: "Days",
      name: ChartPeriodModel.PERIOD_DAYS,
      value: ChartPeriodModel.PERIOD_DAYS,
      disabled: false
    },
    {
      id: ChartPeriodModel.PERIOD_MONTHS,
      label: "Months",
      name: ChartPeriodModel.PERIOD_MONTHS,
      value: ChartPeriodModel.PERIOD_MONTHS,
      disabled: false
    }
  ];

  reportTypesModel: UxSwitchItemModel[] = [
    {
      id: "separate",
      label: "Compare separately",
      name: "separate",
      value: "separate",
      disabled: false
    },
    {
      id: "union",
      label: "Combined data",
      name: "union",
      value: "union",
      disabled: false
    }
  ];


  selectedReport: 'by-devices' | 'by-results' = 'by-results';
  selectedReportComponent: ReportBaseComponent;

  @ViewChild("reportByDevicesEl")
  reportByDevices: ReportByDevicesComponent;

  devReportParameters: ReportParametersModel = {
    selectedPeriod: this.periodItemsModel[0],
    selectedReportType: this.reportTypesModel[0],

    selectedDevices: [],
    devicesError: undefined,

    range: {},
    rangeView: {},
    rangeError: undefined
  };

  @ViewChild("reportByResultsEl")
  reportByResults: ReportsByResultsComponent;

  resReportParameters: ReportParametersModel = {
    selectedPeriod: this.periodItemsModel[0],
    selectedReportType: this.reportTypesModel[0],

    selectedDevices: [],
    devicesError: undefined,

    range: {},
    rangeView: {},
    rangeError: undefined
  };

  _availableDevices: UxMultipleFieldItemModel[];
  _availableTests: TestEntity[] = [];

  _rangePlaceHolder: UxRangeDateFieldPlaceholderModel = {
    from: "Start:",
    to: "Finish:"
  };

  private devicesListSubscription: Subscription;
  private testsListSubscription: Subscription;

  constructor(private devicesService: DevicesService,
              private testsService: TestsService,
              protected configService: AppConfigService,
              private cdr: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this.updateDevicesList();
    this.updateTestsList();

    //this.selectedReportComponent = this.reportByDevices;
    this.selectedReportComponent = this.reportByResults;
  }

  ngOnDestroy(): void {
    this.devicesListSubscription && this.devicesListSubscription.unsubscribe();
    this.testsListSubscription && this.testsListSubscription.unsubscribe();
  }

  _onPeriodSelected(value: UxValueChangeEvent<UxDropdownListItem>, reportParameters: ReportParametersModel): void {
    reportParameters.selectedPeriod = value.newValue;
  }

  _onReportTypeSelected(value: UxValueChangeEvent<UxRadioItemModel>, reportParameters: ReportParametersModel): void {
    reportParameters.selectedReportType = value.newValue;
  }

  _onRangeSelected(value: UxRangeDateFieldModel, reportParameters: ReportParametersModel): void {
    if (value === undefined) {
      return;
    }

    reportParameters.range = value;
    let fromDate = reportParameters.range.from;
    let toDate = reportParameters.range.to;
    if (fromDate && toDate) {
      toDate.setHours(23, 59, 59);

      let fromTime = fromDate.getTime();
      let toTime = toDate.getTime();

      let rangeError = undefined;
      let maxPeriods = this.configService.report().periods;
      let diff = toTime - fromTime;

      if (reportParameters.selectedPeriod.id === ChartPeriodModel.PERIOD_HOURS) {
        let hours = (diff / 1000) / 3600;
        if (hours > maxPeriods) {
          rangeError = `Selected period contains more than ${maxPeriods} hours`;
        }
      } else if (reportParameters.selectedPeriod.id === ChartPeriodModel.PERIOD_DAYS) {
        let days = ((diff / 1000) / 3600) / 24;
        if (days > maxPeriods) {
          rangeError = `Selected period contains more than ${maxPeriods} days`;
        }
      } else if (reportParameters.selectedPeriod.id === ChartPeriodModel.PERIOD_MONTHS) {
        let months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
        months -= fromDate.getMonth() + 1;
        months += toDate.getMonth();
        if (months > maxPeriods) {
          rangeError = `Selected period contains more than ${maxPeriods} months`;
        }
      }
      reportParameters.rangeError = rangeError;
    }
  }

  _onDevicesSelected(value: UxMultipleFieldItemModel[], reportParameters: ReportParametersModel): void {
    if (value === undefined) {
      return;
    }

    reportParameters.selectedDevices = value;
    reportParameters.devicesError = undefined;
  }

  _onApplyReport(reportComponent: ReportBaseComponent, reportParameters: ReportParametersModel): void {
    if (reportParameters.range.to) {
      reportParameters.range.to.setHours(23, 59, 59);
    }
    if (!this.selectedReportComponent.onApplyReport(reportParameters)) {
      return;
    }
    this._isInitialView = false;
  }

  _onResetReport(reportComponent: ReportBaseComponent, reportParameters: ReportParametersModel): void {
    this.selectedReportComponent.onResetReport(reportParameters);
  }

  private updateDevicesList() {
    this.devicesListSubscription && this.devicesListSubscription.unsubscribe();
    this.devicesListSubscription = this.devicesService.getEntityList()
      .subscribe((devices: DeviceEntity[]) => {
        this._availableDevices = devices.map((device: DeviceEntity) => {
          return this.deviceToMultipleFieldItem(device);
        });
      });
  }

  private deviceToMultipleFieldItem(device: DeviceEntity): UxMultipleFieldItemModel {
    return {
      id: device.instance,
      caption: device.instance,
      selected: false
    };
  }

  private updateTestsList() {
    this.testsListSubscription && this.testsListSubscription.unsubscribe();
    this.testsListSubscription = this.testsService.getEntityList()
      .subscribe((tests: TestEntity[]) => {
        this._availableTests = tests;
      });
  }

  _onTabPanelChange(event: TabChangeEvent) {
    this.onReportChanged(event.index);
  }

  _onReportToggle(event: UxAccordionToggleEvent): void {
    if (event.opened) {
      this.onReportChanged(event.index);
    }
  }

  private onReportChanged(index: number): void {
    if (index === 0) {
      this.selectedReport = 'by-devices';
      window.setTimeout(() => {
        this.selectedReportComponent = this.reportByDevices;
      });
    } else {
      this.selectedReport = 'by-results';
      window.setTimeout(() => {
        this.selectedReportComponent = this.reportByResults;
      });
    }
    this.cdr.detectChanges();
  }
}
