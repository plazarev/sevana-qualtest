import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output} from "@angular/core";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {UxValueChangeEvent} from "../../../../ux-lib/components/fields/abstract-field.component";
import {UxRadioItemModel} from "../../../../ux-lib/components/fields/radio-group/radio/radio-field.component";
import {
  UxRangeDateFieldModel, UxRangeDateFieldPlaceholderModel,
  UxRangeDateFieldViewEventModel
} from "../../../../ux-lib/components/fields/date/range/range-date-field.model";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {UxSwitchItemModel} from "../../../../ux-lib/components/fields/switch-selector/switch-item.model";

@Component({
  selector: "sq-report-filter",
  templateUrl: "report-filter.component.html",
  host: {"[class.sq-report-filter]": "true"}
})
export class ReportFilterComponent implements AfterViewInit, OnDestroy {

  @Input()
  public isCompact: boolean = false;

  @Input()
  public periodModel: UxSwitchItemModel[];

  @Input()
  public selectedPeriod: UxSwitchItemModel;

  @Output()
  public selectedPeriodChange = new EventEmitter<UxSwitchItemModel>();

  @Input()
  public reportTypesModel: UxSwitchItemModel[];

  @Input()
  public selectedReportType: UxSwitchItemModel;

  @Output()
  public selectedReportTypeChange = new EventEmitter<UxSwitchItemModel>();

  @Input()
  public selectedRange: UxRangeDateFieldModel;

  @Input()
  public selectedViewRange: UxRangeDateFieldViewEventModel;

  @Output()
  public selectedRangeChange = new EventEmitter<UxRangeDateFieldModel>();

  @Input()
  public rangeError: string;

  @Input()
  public availableDevices: UxMultipleFieldItemModel[];

  @Input()
  public selectedDevices: UxMultipleFieldItemModel[];

  @Output()
  public selectedDevicesChange = new EventEmitter<UxMultipleFieldItemModel[]>();

  @Input()
  public devicesError: string;

  @Output()
  public onApplyFilter = new EventEmitter();

  @Output()
  public onResetFilter = new EventEmitter();

  _rangePlaceHolder: UxRangeDateFieldPlaceholderModel = {
    from: "Start:",
    to: "Finish:"
  };

  _endAvailableDate = new Date();

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
  }

  _onPeriodSelected(value: UxValueChangeEvent<UxSwitchItemModel>): void {
    this.selectedPeriod = value.newValue;
    this.selectedPeriodChange.emit(this.selectedPeriod);
  }

  _onReportTypeSelected(value: UxValueChangeEvent<UxSwitchItemModel>): void {
    this.selectedReportType = value.newValue;
    this.selectedReportTypeChange.emit(this.selectedReportType);
  }

  _onRangeSelected(value: UxRangeDateFieldModel): void {
    if (this.selectedRange !== value) {
      this.selectedRange = value;
      this.selectedRangeChange.emit(this.selectedRange);
    }
  }

  _onDevicesSelected(value: UxValueChangeEvent<UxMultipleFieldItemModel[]>): void {
    if (this.selectedDevices !== value.newValue) {
      this.selectedDevices = value.newValue;
      this.selectedDevicesChange.emit(this.selectedDevices);
    }
  }
}
