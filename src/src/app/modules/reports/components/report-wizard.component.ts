
import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {UxWizardStep} from "../../../../ux-lib/components/wizard-navigation/wizard-step.model";
import {PagedComponent} from "../../../components/base/paged.component";
import {AppConfigService} from "../../../common/services/app-config.service";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {Subject, Subscription} from "rxjs/index";
import {takeUntil, tap} from "rxjs/internal/operators";
import {CountResult} from "../../../common/services/entity.service";
import {TestsService} from "../../tests/tests.service";
import {DevicesService} from "../../devices/devices.service";
import {ApiPath} from "../../../common/services/api.path";
import {QueryParam} from "../../../common/models/query-param.type";
import {UxCardTreeCheckedEvent} from "../../../../ux-lib/components/card-tree/card-tree.component";
import {DevicesCardListComponent} from "../../devices/components/devices-card-list.component";
import {TestsCardListComponent} from "../../tests/components/tests-card-list.component";
import {Progress} from "../../../common/models/progress.model";
import {UxSwitchItemModel} from "../../../../ux-lib/components/fields/switch-selector/switch-item.model";
import {StatisticPeriod} from "../../../common/models/statistic-period.type";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {
  UxRangeDateFieldModel,
  UxRangeDateFieldPlaceholderModel, UxRangeDateFieldViewEventModel
} from "../../../../ux-lib/components/fields/date/range/range-date-field.model";
import {Router} from "@angular/router";
import {PagingModel} from "../../../common/models/paging.model";
import {ModelService} from "../../../common/services/model.service";
import {UxPageChangeEvent} from "../../../../ux-lib/components/paging/paging.component";

@Component({
  selector: "sq-report-wizard",
  templateUrl: "report-wizard.component.html",
  host: {"[class.sq-report-wizard]": "true"}
})
export class ReportWizardComponent extends PagedComponent{
  private devicesPagingConfig: PagingModel;
  private testsPagingConfig: PagingModel;

  private wizardSteps: UxWizardStep[] = [
    {
      id: 'select-devices',
      name: 'Choose devices'
    },
    {
      id: 'select-tests',
      name: 'Choose tests'
    },
    {
      id: 'configure',
      name: 'configure'
    },
  ];

  private _periodItemsModel: UxSwitchItemModel[] = [
    {
      id: StatisticPeriod.PERIOD_HOURS,
      label: "Hours",
      name: StatisticPeriod.PERIOD_HOURS,
      value: StatisticPeriod.PERIOD_HOURS,
      disabled: false
    },
    {
      id: StatisticPeriod.PERIOD_DAYS,
      label: "Days",
      name: StatisticPeriod.PERIOD_DAYS,
      value: StatisticPeriod.PERIOD_DAYS,
      disabled: false
    },
    {
      id: StatisticPeriod.PERIOD_MONTHS,
      label: "Months",
      name: StatisticPeriod.PERIOD_MONTHS,
      value: StatisticPeriod.PERIOD_MONTHS,
      disabled: false
    }
  ];

  private _selectedPeriod: UxDropdownListItem = this._periodItemsModel[0];

  private _selectedRange: UxRangeDateFieldModel = {
    from: new Date(),
    to: new Date()
  };
  private _selectedViewRange: UxRangeDateFieldViewEventModel;
  private _rangePlaceHolder: UxRangeDateFieldPlaceholderModel = {
    from: "mm/dd/yyyy",
    to: "mm/dd/yyyy"
  };
  private _endAvailableDate = new Date();
  private _rangeError: string = "";

  private _stepIndex: number = 0;
  private _devices: DeviceEntity[] = [];
  private _devicesLoadingStatus: Progress = 'loading';
  private _selectedDevices: string[] = [];

  private _tests: TestEntity[] = [];
  private _testsLoadingStatus: Progress = 'loading';
  private _selectedTests: string[] = [];

  private _reportAllowed: boolean = false;

  private stopSubscription$ = new Subject<boolean>();
  private devicesCountSubscription$: Subscription;
  private devicesListSubscription$: Subscription;
  private testsCountSubscription$: Subscription;
  private testsListSubscription$: Subscription;

  @ViewChild("devicesCardList")
  private devicesCardList: DevicesCardListComponent;

  @ViewChild("testsCardList")
  private testsCardList: TestsCardListComponent;

  constructor(protected configService: AppConfigService,
              private modelService: ModelService,
              private devicesService: DevicesService,
              private testsService: TestsService,
              private router: Router,
              private cdRef: ChangeDetectorRef) {
    super(configService);
  }

  public loadInitialData() {
    this.devicesPagingConfig = this.modelService.deepCopy(this.pagingConfig);
    this.testsPagingConfig = this.modelService.deepCopy(this.pagingConfig);

    super.loadInitialData();
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  protected _onLoadDevicesInitialData() {
    this.loadDevicesPage(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
  }

  protected _onLoadTestInitialData() {
    this.loadTestsPage(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
  }

  protected _onPageChange(pageEvent: UxPageChangeEvent) {
    if (this.viewInitiated) {
      if (this._stepIndex === 0) {
        if (this.devicesPagingConfig.currentPage != pageEvent.page) {
          this.devicesPagingConfig.currentPage = pageEvent.page;
          this.loadDevicesPage(pageEvent.page, pageEvent.items);
        }
      }
      else if (this._stepIndex === 1) {
        if (this.testsPagingConfig.currentPage != pageEvent.page) {
          this.testsPagingConfig.currentPage = pageEvent.page;
          this.loadTestsPage(pageEvent.page, pageEvent.items);
        }
      }
    }
  }

  protected _onPageSizeChange(pageEvent: UxPageChangeEvent) {
    if (this.viewInitiated) {
      if (this._stepIndex === 0) {
        if (this.devicesPagingConfig.pageSize != pageEvent.items) {
          this.devicesPagingConfig.pageSize = pageEvent.items;
          this.loadDevicesPage(pageEvent.page, pageEvent.items);
        }
      }
      else if (this._stepIndex === 1) {
        if (this.testsPagingConfig.pageSize != pageEvent.items) {
          this.testsPagingConfig.pageSize = pageEvent.items;
          this.loadTestsPage(pageEvent.page, pageEvent.items);
        }
      }
    }
  }

  public loadPageData(currentPage: number, pageSize: number): void {
    if (this._stepIndex === 0) {
      this.loadDevicesPage(currentPage, pageSize);
    }
    else if (this._stepIndex === 1) {
      this.loadTestsPage(currentPage, pageSize);
    }
  }

  private loadDevicesPage(currentPage: number, pageSize: number): void {
    this._devicesLoadingStatus = 'loading';
    this.devicesCountSubscription$ && this.devicesCountSubscription$.unsubscribe();
    this.devicesCountSubscription$ = this.devicesService
      .getEntitiesCount()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((result: CountResult) => {
          this.pagingConfig.totalCount = result.totalcount;
          this._devicesLoadingStatus = 'loaded';
          this.retrieveDevicesListPage(currentPage, pageSize);
        })
      )
      .subscribe();
    this.cdRef.detectChanges();
  }

  private retrieveDevicesListPage(currentPage: number, pageSize: number): void {
    this._devicesLoadingStatus = 'loading';
    let queryParams: QueryParam[] = [
      {
        key: ApiPath.OFFSET,
        value: (currentPage-1) * pageSize
      },
      {
        key: ApiPath.LIMIT,
        value: pageSize
      }
    ];

    this.devicesListSubscription$ && this.devicesListSubscription$.unsubscribe();
    this.devicesListSubscription$ = this.devicesService
      .getEntityList(queryParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((devices: DeviceEntity[]) => {
          this._devicesLoadingStatus = 'loaded';
          this._devices = devices;
        })
      )
      .subscribe();
  }

  private loadTestsPage(currentPage: number, pageSize: number): void {
    this._testsLoadingStatus = 'loading';
    this.testsCountSubscription$ && this.testsCountSubscription$.unsubscribe();
    this.testsCountSubscription$ = this.testsService
      .getEntitiesCount()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((result: CountResult) => {
          this.pagingConfig.totalCount = result.totalcount;
          this._testsLoadingStatus = 'loaded';
          this.retrieveTestsListPage(currentPage, pageSize);
        })
      )
      .subscribe();
    this.cdRef.detectChanges();
  }

  protected retrieveTestsListPage(currentPage: number, pageSize: number): void {
    this._testsLoadingStatus = 'loading';
    let queryParams: QueryParam[] = [
      {
        key: ApiPath.OFFSET,
        value: (currentPage-1) * pageSize
      },
      {
        key: ApiPath.LIMIT,
        value: pageSize
      }
    ];

    this.testsListSubscription$ && this.testsListSubscription$.unsubscribe();
    this.testsListSubscription$ = this.testsService
      .getEntityList(queryParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((tests: TestEntity[]) => {
          this._testsLoadingStatus = 'loaded';
          this._tests = tests;
        })
      )
      .subscribe();
  }

  protected _onCardChecked(event: UxCardTreeCheckedEvent) {
    if (this._stepIndex === 0) { // select devices page
      this._selectedDevices = this._selectedDevices.filter((value: string) => value !== event.model.id);
      if (event.model.selected) {
        this._selectedDevices.push(event.model.id);
      }
    }
    else if (this._stepIndex === 1) { // select tests page
      this._selectedTests = this._selectedTests.filter((value: string) => value !== event.model.id);
      if (event.model.selected) {
        this._selectedTests.push(event.model.id);
      }
    }
  }

  protected _onCardCheckedAll(event: UxCardTreeCheckedEvent) {
    if (this._stepIndex === 0) { // select devices page
      this._selectedDevices = this._devices.map((device: DeviceEntity) => device.instance);
    }
    else if (this._stepIndex === 1) { // select tests page
      this._selectedTests = this._tests.map((test: TestEntity) => test.name);
    }
  }

  _onStepChange(index: number) {
    if (this._stepIndex !== index) {
      this._stepIndex = index;
    }
  }

  _onNextClick(hasNext: boolean) {
    if (hasNext) {
      this._stepIndex++;
    }
  }

  _onBackClick() {
    this._stepIndex--;
  }

  _onPeriodSelected(value: UxDropdownListItem) {
    if (this._selectedPeriod !== value) {
      this._selectedPeriod = value;
    }

    this.validatePeriodAndRange();
  }

  _onRangeSelected(value: UxRangeDateFieldModel): void {
    if (this._selectedRange !== value) {
      this._selectedRange = value;
    }
    this.validatePeriodAndRange();
  }

  private validatePeriodAndRange() {
    this._rangeError = "";
    let allowed = this._reportAllowed;

    if (this._selectedRange && this._selectedRange.from && this._selectedRange.to) {
      let fromDate = new Date(this._selectedRange.from);
      let toDate = new Date(this._selectedRange.to);
      fromDate.setHours(0, 0, 0);
      toDate.setHours(23, 59, 59);

      let currDate = new Date();
      if (toDate.getTime() > currDate.getTime()) {
        toDate = currDate;
      }

      let fromTime = fromDate.getTime();
      let toTime = toDate.getTime();

      let maxPeriods = this.configService.report().periods;
      let diff = toTime - fromTime;

      if (this._selectedPeriod.id === StatisticPeriod.PERIOD_HOURS) {
        let hours = (diff / 1000) / 3600;
        if (hours > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} hours`;
        }
      }
      else if (this._selectedPeriod.id === StatisticPeriod.PERIOD_DAYS) {
        let days = ((diff / 1000) / 3600) / 24;
        if (days > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} days`;
        }
      }
      else if (this._selectedPeriod.id === StatisticPeriod.PERIOD_MONTHS) {
        let months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
        months -= fromDate.getMonth() + 1;
        months += toDate.getMonth();
        if (months > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} months`;
        }
      }

      allowed = (this._rangeError.length === 0);
    }
    else {
      allowed = false;
    }

    if (this._reportAllowed != allowed) {
      this._reportAllowed = allowed;
    }

  }

  _onGetReport() {
    this.router.navigate(['/reports/view'], {
      queryParams: {
        period: this._selectedPeriod.id,
        from: this._selectedRange.from.getTime(),
        to: this._selectedRange.to.getTime(),
        devices: this._selectedDevices,
        tests: this._selectedTests
      }
    });
  }
}
