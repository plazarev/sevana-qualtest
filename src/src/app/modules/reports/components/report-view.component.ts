import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import {UxRadioItemModel} from "../../../../ux-lib/components/fields/radio-group/radio/radio-field.component";
import {
  UxRangeDateFieldModel,
  UxRangeDateFieldPlaceholderModel, UxRangeDateFieldViewEventModel
} from "../../../../ux-lib/components/fields/date/range/range-date-field.model";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {UxValueChangeEvent} from "../../../../ux-lib/components/fields/abstract-field.component";
import {DevicesService} from "../../devices/devices.service";
import {Subject, Subscription} from "rxjs/index";
import {DeviceEntity} from "../../../common/models/entity/device-entity.model";
import {AppConfigService} from "../../../common/services/app-config.service";
import {ChartPeriodModel} from "../model/chart-period.model";
import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {UxAccordionToggleEvent} from "../../../../ux-lib/components/accordion/accordion.component";
import {ReportByDevicesComponent} from "./report-by-devices.component";
import {ReportsByResultsComponent} from "./report-by-results.component";
import {ReportBaseComponent} from "./report-base.component";
import {ReportParametersModel} from "../model/report-parameters.model";
import {UxSwitchItemModel} from "../../../../ux-lib/components/fields/switch-selector/switch-item.model";
import {TabChangeEvent} from "../../../../ux-lib/components/tab-panel/tab-panel-event.model";
import {TestsService} from "../../tests/tests.service";
import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {ActivatedRoute, ParamMap, Params, Router} from "@angular/router";
import {StatisticPeriod, StatisticPeriodType} from "../../../common/models/statistic-period.type";
import * as moment from "moment";
import {Progress} from "../../../common/models/progress.model";
import {ReportRequestParametersModel} from "../model/report-request-parameters.model";
import {ReportsService} from "../reports.service";
import {takeUntil, tap} from "rxjs/internal/operators";
import {
  StatisticsDataEntry, StatisticsDataStyledVisualEntry,
  StatisticsEntity,
  StatisticsEntityBaseWithLastValueTimestamp
} from "../../../common/models/entity/statistics-entity.model";
import {ChartEntity, ChartType, PhoneCharts} from "../model/chart.model";
import {HighChartModel} from "../../../../ux-lib/components/charts/highchart/model/highchart.model";
import {StatisticDataType} from "../../../common/models/statistic-data.type";
import {HighchartService} from "../charts-services/highchart.service";
import {ResultsService} from "../../results/results.service";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {ConfigComponent} from "../../../components/base/config.component";
import {UxTable, UxTableColumn, UxTableRow} from "../../../../ux-lib/components/table/table.model";
import {UxPageChangeEvent} from "../../../../ux-lib/components/paging/paging.component";

@Component({
  selector: "sq-report-view",
  templateUrl: "report-view.component.html",
  host: {"[class.sq-report-view]": "true"}
})
export class ReportViewComponent extends ConfigComponent {
  private _reportProgress: Progress = 'no_data';
  private _resultsProgress: Progress = 'no_data';
  private _resultProgress: Progress = 'no_data';

  private _reportStatistics: StatisticsEntity[]
  private _results: TestResultEntity[] = [];
  private _selectedTestResult: TestResultEntity;

  private _summary: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry>;
  private _summaryVisual: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry>;
  private _pvqaCharts: PhoneCharts<HighChartModel>[];
  private _aquaCharts: PhoneCharts<HighChartModel>[];
  private _rFactorCharts: PhoneCharts<HighChartModel>[];
  private _mosNetworkCharts: PhoneCharts<HighChartModel>[];

  private _selectedPeriod: StatisticPeriodType;
  private _selectedRange: UxRangeDateFieldModel = {
    from: new Date(),
    to: new Date()
  };
  private _selectedViewRange: UxRangeDateFieldViewEventModel;
  private _rangePlaceHolder: UxRangeDateFieldPlaceholderModel = {
    from: "Start:",
    to: "Finish:"
  };
  private _endAvailableDate = new Date();
  private _rangeError: string = "";
  private _reportAllowed: boolean = false;

  private _selectedDevices: string[] = [];
  private _selectedTests: string[] = [];

  private _probeID: string;
  private _resultsPagingVisible: boolean = true;
  private _resultsTableDataModel: UxTable = {
    header: {
      rows: [
        {
          styleClass: "_header",
          columns: [
            {
              value: "Device",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "Test",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "Target",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              },
            },
            {
              value: "Time",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              },
            },
            {
              value: "Duration",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "AQUA",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "PVQA",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "MOS Network",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "R-Factor",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "",
              styleClass: "_header"
            }
          ]
        }
      ]
    },
    body: {rows: []}
  };

  private stopSubscription$ = new Subject<boolean>();
  private statisticsSubscription$?: Subscription;
  private resultsSubscription$?: Subscription;
  private resultSubscription$?: Subscription;

  @ViewChild("tableEmptyContent")
  tableEmptyContent: TemplateRef<any>;

  @ViewChild("timeColumn")
  timeColumn: TemplateRef<any>;

  @ViewChild("durationColumn")
  durationColumn: TemplateRef<any>;

  @ViewChild("functionsColumn")
  functionsColumn: TemplateRef<any>;

  constructor(private reportsService: ReportsService,
              private resultsService: ResultsService,
              private chartService: HighchartService,
              private devicesService: DevicesService,
              private testsService: TestsService,
              private cdr: ChangeDetectorRef,
              private route: ActivatedRoute,
              private router: Router,
              protected configService: AppConfigService) {
    super(configService);
  }

  public initComponent() {
    super.initComponent();
    this._resultsTableDataModel.emptyTableContent = this.tableEmptyContent;
  }

  public loadInitialData() {
    super.loadInitialData();
    this.loadQueryParams();
  }

  public destroyComponent() {
    super.destroyComponent();

    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  private loadQueryParams() {
    let defaultDuration = this.configService.report().defaultDuration;
    this.route
      .queryParamMap
      .subscribe((params: ParamMap) => {
        let probeValue = params.get('probe');

        let periodValue = params.get('period');
        let devicesValue = params.getAll('devices') || [];
        let testsValue = params.getAll('tests') || [];
        let fromValue = params.get('from');
        let toValue = params.get('to');

        if (probeValue && probeValue != null) {
          this._probeID = probeValue;
          this.loadTestResult(this._probeID);
        }
        else {
          let period: moment.unitOfTime.DurationConstructor = 'h';
          if (periodValue === undefined || periodValue === null) {
            periodValue = StatisticPeriod.PERIOD_HOURS;
          }
          this._selectedPeriod = <StatisticPeriodType>periodValue;

          if (this._selectedPeriod === 'hour') {
            period = 'h';
          }
          else if (this._selectedPeriod === 'day') {
            period = 'd';
          }
          else if (this._selectedPeriod === 'month') {
            period = 'M';
          }

          if (toValue === undefined || toValue === null) {
            toValue = new Date().getTime().toString();
          }
          if (fromValue === undefined || fromValue === null) {
            let dateFrom: Date = moment(parseInt(toValue)).subtract(defaultDuration, period).toDate();
            fromValue = dateFrom.getTime().toString();
          }

          this._selectedRange.from = new Date(parseInt(fromValue));
          this._selectedRange.to = new Date(parseInt(toValue));

          this._selectedDevices = devicesValue;
          this._selectedTests = testsValue;

          this.loadReportData();
        }
      })
  }

  private loadTestResult(probeID: string) {
    this._resultProgress = 'loading';

    this.resultSubscription$ && this.resultSubscription$.unsubscribe();
    this.resultSubscription$ = this.resultsService
      .getEntityById(probeID)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((result: TestResultEntity) => {
          this._selectedTestResult = result;
          this._resultProgress = this._selectedTestResult ? 'loaded' : 'no_data';
        })
      )
      .subscribe();
  }

  private loadReportData() {
    this._reportProgress = 'loading';

    let reportParams: ReportRequestParametersModel = {};
    reportParams.startDate = this._selectedRange.from;
    reportParams.endDate = this._selectedRange.to;
    reportParams.scale = this._selectedPeriod;
    reportParams.united = true;
    reportParams.phonesList = this._selectedDevices;
    reportParams.testsList = this._selectedTests;

    this.statisticsSubscription$ && this.statisticsSubscription$.unsubscribe();
    this.statisticsSubscription$ = this.reportsService
      .getReportDataEx(reportParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((statistics: StatisticsEntity[]) => {
          this._reportProgress = 'loaded';

          if (statistics) {
            this._reportStatistics = statistics;
          } else {
            this._reportStatistics = [];
          }

          this._summary = this.reportsService.calculateSummaryStatistics(this._reportStatistics);
          this._summaryVisual = this.reportsService.calculateSummaryVisualStatistics(this._summary);

          this.createAquaCharts();
          this.createPvqaCharts();
          this.createRFactorCharts();
          this.createMosNetworkCharts();

          this.loadTestResults(this.pagingConfig.currentPage, this.pagingConfig.pageSize);
        })
      )
      .subscribe();
  }

  private loadTestResults(currentPage: number, pageSize: number) {
    this._resultsProgress = 'loading';
    this.resultsSubscription$ && this.resultsSubscription$.unsubscribe();
    this.resultsSubscription$ = this.resultsService
      .getResults(
        this._selectedRange.from,
        this._selectedRange.to,
        this._selectedTests,
        this._selectedDevices
        )
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((results: TestResultEntity[]) => {
          if (results) {
            this.pagingConfig.totalCount = results.length
            this._resultsPagingVisible = this.pagingConfig.totalCount > Math.min(...this.pagingConfig.pageSizeOptions);

            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = startIndex + pageSize;
            this._results = results.slice(startIndex, endIndex > results.length ? results.length : endIndex);
          }
          else {
            this.pagingConfig.totalCount = 0;
            this._resultsPagingVisible = false;
          }

          this._resultsTableDataModel.body.rows =
            this._results
              .map((result: TestResultEntity) => {
                return this.getTableRow(result);
              });
          this._resultsProgress = 'loaded';
        })
      )
      .subscribe();
  }

  private getTableRow(result: TestResultEntity): UxTableRow {
    let columns: UxTableColumn[]  = [
      {
        value: result.phone_name
      },
      {
        value: result.task_name
      },
      {
        value: result.target
      },
      {
        type: 'content',
        value: this.timeColumn,
        contentModel: {
          endtime: result.endtime * 1000
        }
      },
      {
        type: 'content',
        value: this.durationColumn,
        contentModel: {
          duration: result.duration
        }
      },
      {
        value: result.mos_aqua === 0 ? result.mos_aqua : result.mos_aqua.toFixed(3)
      },
      {
        value: result.mos_pvqa === 0 ? result.mos_pvqa : result.mos_pvqa.toFixed(3)
      },
      {
        value: result.mos_network === 0 ? result.mos_network : result.mos_network.toFixed(3)
      },
      {
        value: result.r_factor
      },
      {
        type: 'content',
        value: this.functionsColumn,
        contentModel: {
          result: result
        }
      }
    ];

    return {
      id: result.id,
      columns: columns
    };
  }

  private createAquaCharts() {
    let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
    chartTypes.set("mos_aqua", ["linear", "calendar"]);

    this._aquaCharts = this.chartService
      .generateCharts(chartTypes,
                      this._selectedPeriod,
                      this._reportStatistics);
  }

  private createPvqaCharts() {
    let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
    chartTypes.set("mos_pvqa", ["linear", "calendar"]);

    this._pvqaCharts = this.chartService
      .generateCharts(chartTypes,
                      this._selectedPeriod,
                      this._reportStatistics);
  }

  private createRFactorCharts() {
    let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
    chartTypes.set("r_factor", ["linear", "calendar"]);

    this._rFactorCharts = this.chartService
      .generateCharts(chartTypes,
                      this._selectedPeriod,
                      this._reportStatistics);
  }

  private createMosNetworkCharts() {
    let chartTypes: Map<StatisticDataType, ChartType[]> = new Map();
    chartTypes.set("mos_network", ["linear", "calendar"]);

    this._mosNetworkCharts = this.chartService
      .generateCharts(chartTypes,
                      this._selectedPeriod,
                      this._reportStatistics);
  }

  private _onCalendarVisibleChange(bVisible: boolean) {
    if (!bVisible && this.validatePeriodAndRange(this._selectedRange)) {
      let period: moment.unitOfTime.DurationConstructor = 'h';
      if (this._selectedPeriod === 'hour') {
        period = 'h';
      }
      else if (this._selectedPeriod === 'day') {
        period = 'd';
      }
      else if (this._selectedPeriod === 'month') {
        period = 'M';
      }
      let dateFrom: Date = this._selectedRange.from;
      let dateTo: Date = this._selectedRange.to;

      this.router.navigate(['/reports/view'], {
        queryParams: {
          period: this._selectedPeriod,
          from: dateFrom.getTime(),
          to: dateTo.getTime(),
          devices: this._selectedDevices,
          tests: this._selectedTests
        }
      });
    }
  }

  private _onPageChange(pageEvent: UxPageChangeEvent) {
    if (this.viewInitiated) {
      this.loadTestResults(pageEvent.page, pageEvent.items);
    }
  }

  private _onPageSizeChange(pageEvent: UxPageChangeEvent) {
    if (this.viewInitiated) {
      this.loadTestResults(pageEvent.page, pageEvent.items);
    }
  }

  private _onChartIcon(result: TestResultEntity) {
    this._selectedTestResult = result;
  }

  private validatePeriodAndRange(value: UxRangeDateFieldModel): boolean {
    this._rangeError = "";
    let allowed = this._reportAllowed;

    if (!value) {
      this._rangeError = "Please chose correct period of report";
      allowed = false;
    }
    else if (!value.from) {
      this._rangeError = "Please chose report period start date";
      allowed = false;
    }
    else if (!value.to) {
      this._rangeError = "Please chose report period end date";
      allowed = false;
    }
    else if (value && value.from && value.to) {
      let fromDate = new Date(value.from);
      let toDate = new Date(value.to);
      fromDate.setHours(0, 0, 0);
      toDate.setHours(23, 59, 59);

      let currDate = new Date();
      if (toDate.getTime() > currDate.getTime()) {
        toDate = currDate;
      }

      let fromTime = fromDate.getTime();
      let toTime = toDate.getTime();

      let maxPeriods = this.configService.report().periods;
      let diff = toTime - fromTime;

      if (this._selectedPeriod === StatisticPeriod.PERIOD_HOURS) {
        let hours = (diff / 1000) / 3600;
        if (hours > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} hours`;
        }
      }
      else if (this._selectedPeriod === StatisticPeriod.PERIOD_DAYS) {
        let days = ((diff / 1000) / 3600) / 24;
        if (days > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} days`;
        }
      }
      else if (this._selectedPeriod === StatisticPeriod.PERIOD_MONTHS) {
        let months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
        months -= fromDate.getMonth() + 1;
        months += toDate.getMonth();
        if (months > maxPeriods) {
          this._rangeError = `Selected period contains more than ${maxPeriods} months`;
        }
      }

      allowed = (this._rangeError.length === 0);
    }
    else {
      allowed = false;
    }

    if (this._reportAllowed != allowed) {
      this._reportAllowed = allowed;
    }

    return this._reportAllowed;
  }

  /** @internal */
  public _trackByChartEntity(index: number, item: ChartEntity): string {
    return item && item.id;
  }
}
