
import {StatisticDataType} from "../../../common/models/statistic-data.type";

export type ChartType = 'linear' | 'calendar' | 'column';

// phone -----------------.
//   |                     \
//  mos_pvqa              mos_aqua ....
//   |      \               |     \
//  linear   calendar     linear   calendar

export interface ChartEntity {
  id: string;
}

export interface ChartModel<T> extends ChartEntity {
  chartType: ChartType;
  chartModel: T;
}

export interface ChartDataModel<T> extends ChartEntity {
  dataType: StatisticDataType;
  chartModels: ChartModel<T>[];
  chartModelsMap: Map<ChartType, ChartModel<T>>;
}

export interface PhoneCharts<T> extends ChartEntity {
  phone: string;
  title?: string;
  isEmpty?: boolean;
  chartDataModels: ChartDataModel<T>[];
  chartDataModelsMap: Map<StatisticDataType, ChartDataModel<T>>;
}

// phone -----------------.
//   |                     \
//  mos_pvqa              mos_aqua ....
//   |      \               |     \
//  linear   calendar     linear   calendar
