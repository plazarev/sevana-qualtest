import {UxDropdownListItem} from "../../../../ux-lib/components/fields/dropdown/dropdown-field.component";
import {UxRadioItemModel} from "../../../../ux-lib/components/fields/radio-group/radio/radio-field.component";
import {UxMultipleFieldItemModel} from "../../../../ux-lib/components/fields/multiple/multiple-field-item.model";
import {
  UxRangeDateFieldModel,
  UxRangeDateFieldViewEventModel
} from "../../../../ux-lib/components/fields/date/range/range-date-field.model";

export interface ReportParametersModel {
  selectedPeriod: UxDropdownListItem;
  selectedReportType: UxRadioItemModel;

  selectedDevices: UxMultipleFieldItemModel[];
  devicesError: string;

  range: UxRangeDateFieldModel;
  rangeView: UxRangeDateFieldViewEventModel;
  rangeError: string;
}
