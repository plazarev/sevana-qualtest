
import {StatisticPeriodType} from "../../../common/models/statistic-period.type";

export interface ReportRequestParametersModel {
  startDate?: Date;
  endDate?: Date;
  scale?: StatisticPeriodType;
  united?: boolean;
  phonesList?: string[];
  testsList?: string[];
}
