import {TestEntity} from "../../../common/models/entity/test-entity.model";
import {
  StatisticsDataEntry, StatisticsDataStyledVisualEntry,
  StatisticsEntity
} from "../../../common/models/entity/statistics-entity.model";
import {UxCardTreeNodeModel} from "../../../../ux-lib/components/card-tree/card-tree.model";
import {Subscription} from "rxjs/index";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {ReportParametersModel} from "./report-parameters.model";
import {HighChartModel} from "../../../../ux-lib/components/charts/highchart/model/highchart.model";
import {PhoneCharts} from "./chart.model";

export interface ReportCardModel extends UxCardTreeNodeModel {
  caption: string,
  test: TestEntity,
  hasTestResults: boolean,
  isLoadInProgress: boolean,
  isExpanded: boolean,

  reportParameters: ReportParametersModel,

  statisticsSubscription?: Subscription,
  statisticsResults?: StatisticsEntity[]

  probesInProgress: boolean,
  probesSubscription?: Subscription,
  probesResults?: TestResultEntity[],

  pvqaSummary?: StatisticsDataEntry,
  aquaSummary?: StatisticsDataEntry,
  rFactorSummary?: StatisticsDataEntry,
  mosNetworkSummary?: StatisticsDataEntry,

  pvqaVisibleSummary?: StatisticsDataStyledVisualEntry,
  aquaVisibleSummary?: StatisticsDataStyledVisualEntry,
  rFactorVisibleSummary?: StatisticsDataStyledVisualEntry,
  mosNetworkVisibleSummary?: StatisticsDataStyledVisualEntry;

  chartsInProgress?: boolean;
  pvqaCharts?: PhoneCharts<HighChartModel>[];
  aquaCharts?: PhoneCharts<HighChartModel>[];
  rFactorCharts?: PhoneCharts<HighChartModel>[];
  mosNetworkCharts?: PhoneCharts<HighChartModel>[];
}
