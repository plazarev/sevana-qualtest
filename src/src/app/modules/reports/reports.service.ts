import {Injectable, Injector} from "@angular/core";
import {Observable, Subscriber} from "rxjs/index";
import {ApiPath} from "../../common/services/api.path";
import {ApiService} from "../../common/services/api.service";
import {QueryParam} from "../../common/models/query-param.type";
import {
  Statistics24Entity,
  StatisticsDataEntry, StatisticsDataStyledVisualEntry,
  StatisticsEntity, StatisticsEntityBase,
  StatisticsEntityBaseWithLastValueTimestamp, SummaryStatisticsItem
} from "../../common/models/entity/statistics-entity.model";
import {ChartPeriodModel} from "./model/chart-period.model";
import {ReportRequestParametersModel} from "./model/report-request-parameters.model";
import {StatisticPeriodType} from "../../common/models/statistic-period.type";
import {ModelService} from "../../common/services/model.service";
import {CssService} from "src/app/common/services/css.service";

@Injectable()
export class ReportsService extends ApiService {
  protected readonly baseUrl: string = ApiPath.STATISTICS_PATH;
  protected readonly getStatisticsUrlUnion: string[] = [this.baseUrl, "/json/statistics-union-device.json"];
  //protected readonly getStatisticsUrlUnion: string[] = [this.baseUrl, "/json/statistics-union-device-empty.json"];
  protected readonly getStatisticsUrlSeparated: string[] = [this.baseUrl, "/json/statistics-per-device.json"];
  protected readonly getStatisticsUrlDaysUnion: string[] = [this.baseUrl, "/json/statistics-union-device-days.json"];
  protected readonly getStatisticsUrlDaysSeparated: string[] = [this.baseUrl, "/json/statistics-per-device-days.json"];
  protected readonly getStatisticsUrlMonthsUnion: string[] = [this.baseUrl, "/json/statistics-union-device-month.json"];
  protected readonly getStatisticsUrlMonthsSeparated: string[] = [this.baseUrl, "/json/statistics-per-device-month.json"];

  protected readonly getStatisticsUrlDevices: string[] = [ApiPath.STATISTICS_PHONES_PATH, "/json/statistics-devices-24.json"];
  protected readonly getStatisticsUrlTasks: string[] = [ApiPath.STATISTICS_TASKS_PATH, "/json/statistics-tasks-24.json"];

  private modelService: ModelService;
  private cssService: CssService;

  constructor(protected injector: Injector) {
    super(injector);
    this.modelService = injector.get(ModelService);
    this.cssService = injector.get(CssService);
  }

  public getReportData(startDate: Date, endDate: Date, scale: string, united: boolean, phonesList: string[], testsList: string[]): Observable<StatisticsEntity[]> {
    return this.getReportDataEx({
      startDate: startDate,
      endDate: endDate,
      scale: scale as StatisticPeriodType,
      united: united,
      phonesList: phonesList,
      testsList: testsList
    })
  }

  public getReportDataEx(reportRequestParams: ReportRequestParametersModel): Observable<StatisticsEntity[]> {
    let o = Observable.create((subscriber: Subscriber<StatisticsEntity[]>) => {
      let queryParams: QueryParam[] = [
        {
          key: ApiPath.START_DATE,
          value: reportRequestParams.startDate.getTime()/1000
        },
        {
          key: ApiPath.END_DATE,
          value: reportRequestParams.endDate.getTime()/1000
        },
        {
          key: ApiPath.SCALE,
          value: reportRequestParams.scale
        },
        {
          key: ApiPath.UNITED,
          value: reportRequestParams.united
        }
        ];

      reportRequestParams.phonesList.forEach((device: string) => {
        queryParams.push({
          key: ApiPath.PHONES,
          value: device
        });
      });

      reportRequestParams.testsList.forEach((test: string) => {
        queryParams.push({
          key: ApiPath.TASK_ID,
          value: test
        });
      });

      let url = reportRequestParams.united ? this.getStatisticsUrlUnion : this.getStatisticsUrlSeparated;
      if (reportRequestParams.scale === ChartPeriodModel.PERIOD_DAYS) {
        url = reportRequestParams.united ? this.getStatisticsUrlDaysUnion : this.getStatisticsUrlDaysSeparated;
      }
      else if (reportRequestParams.scale === ChartPeriodModel.PERIOD_MONTHS) {
        url = reportRequestParams.united ? this.getStatisticsUrlMonthsUnion : this.getStatisticsUrlMonthsSeparated;
      }

      let get = this.get_collection(url, {
        queryParams: queryParams
      }).subscribe(async (items: StatisticsEntity[]) => {
        subscriber.next(items);
        subscriber.complete();
      });

      return () => {
        get && get.unsubscribe();
      };
    });
    return o;
  }

  public getDevicesStatistics(reportRequestParams: ReportRequestParametersModel): Observable<Map<string, SummaryStatisticsItem>> {
    let o = Observable.create((subscriber: Subscriber<Map<string, SummaryStatisticsItem>>) => {

      let duration = this.intDiv((reportRequestParams.endDate.getTime() - reportRequestParams.startDate.getTime()), 1000);

      let queryParams: QueryParam[] = [
        {
          key: ApiPath.DURATION,
          value: duration
        },
      ];

      reportRequestParams.phonesList.forEach((device: string) => {
        queryParams.push({
          key: ApiPath.PHONES,
          value: device
        });
      });

      let get = this.get_map(this.getStatisticsUrlDevices, {
        queryParams: queryParams
      }).subscribe(async (items: Map<string, SummaryStatisticsItem>) => {
        subscriber.next(items);
        subscriber.complete();
      });

      return () => {
        get && get.unsubscribe();
      };
    });
    return o;
  }

  public getTasksStatistics(reportRequestParams: ReportRequestParametersModel): Observable<Map<string, SummaryStatisticsItem>> {
    let o = Observable.create((subscriber: Subscriber<Map<string, SummaryStatisticsItem>>) => {

      let duration = this.intDiv((reportRequestParams.endDate.getTime() - reportRequestParams.startDate.getTime()), 1000);

      let queryParams: QueryParam[] = [
        {
          key: ApiPath.DURATION,
          value: duration
        },
      ];

      reportRequestParams.testsList.forEach((test: string) => {
        queryParams.push({
          key: ApiPath.TASK_ID,
          value: test
        });
      });

      let get = this.get_map(this.getStatisticsUrlTasks, {
        queryParams: queryParams
      }).subscribe(async (items: Map<string, SummaryStatisticsItem>) => {
        subscriber.next(items);
        subscriber.complete();
      });

      return () => {
        get && get.unsubscribe();
      };
    });
    return o;
  }

  public convertSummaryStatisticsPerItem(entities: string[], entityStatistics: Map<string, SummaryStatisticsItem>): Map<string, StatisticsEntityBase<StatisticsDataEntry>> {
    let summaryMap: Map<string, StatisticsEntityBase<StatisticsDataEntry>> = new Map<string, StatisticsEntityBase<StatisticsDataEntry>>();
    if (entityStatistics) {
      for (let statKey of entityStatistics.keys()) {
        let id = statKey;
        let statItem = entityStatistics.get(statKey);

        let deleteIdx = entities.indexOf(id);
        if (deleteIdx >= 0) {
          entities.splice(deleteIdx, 1);
        }

        let summaryItem: StatisticsEntityBase<StatisticsDataEntry>;
        summaryItem = summaryMap.get(id);
        if (summaryItem === undefined || summaryItem === null) {
          summaryItem = {
            mos_pvqa: {},
            mos_aqua: {},
            r_factor: {},
            mos_network: {},
          };
          summaryMap.set(id, summaryItem);
        }

        summaryItem.mos_pvqa.average = statItem.mos_pvqa.average;
        summaryItem.mos_pvqa.min = statItem.mos_pvqa.min;
        summaryItem.mos_pvqa.min_timestamp = statItem.mos_pvqa.min_probe.endtime*1000;
        summaryItem.mos_pvqa.min_probe_id = statItem.mos_pvqa.min_probe.id;
        summaryItem.mos_pvqa.max = statItem.mos_pvqa.max;
        summaryItem.mos_pvqa.max_timestamp = statItem.mos_pvqa.max_probe.endtime*1000;
        summaryItem.mos_pvqa.max_probe_id = statItem.mos_pvqa.max_probe.id;
        summaryItem.mos_pvqa.last = statItem.mos_pvqa.last;
        summaryItem.mos_pvqa.last_timestamp = statItem.mos_pvqa.last_probe.endtime*1000;
        summaryItem.mos_pvqa.last_probe_id = statItem.mos_pvqa.last_probe.id;

        summaryItem.mos_aqua.average = statItem.mos_aqua.average;
        summaryItem.mos_aqua.min = statItem.mos_aqua.min;
        summaryItem.mos_aqua.min_timestamp = statItem.mos_aqua.min_probe.endtime*1000;
        summaryItem.mos_aqua.min_probe_id = statItem.mos_aqua.min_probe.id;
        summaryItem.mos_aqua.max = statItem.mos_aqua.max;
        summaryItem.mos_aqua.max_timestamp = statItem.mos_aqua.max_probe.endtime*1000;
        summaryItem.mos_aqua.max_probe_id = statItem.mos_aqua.max_probe.id;
        summaryItem.mos_aqua.last = statItem.mos_aqua.last;
        summaryItem.mos_aqua.last_timestamp = statItem.mos_aqua.last_probe.endtime*1000;
        summaryItem.mos_aqua.last_probe_id = statItem.mos_aqua.last_probe.id;

        summaryItem.r_factor.average = statItem.r_factor.average;
        summaryItem.r_factor.min = statItem.r_factor.min;
        summaryItem.r_factor.min_timestamp = statItem.r_factor.min_probe.endtime*1000;
        summaryItem.r_factor.min_probe_id = statItem.r_factor.min_probe.id;
        summaryItem.r_factor.max = statItem.r_factor.max;
        summaryItem.r_factor.max_timestamp = statItem.r_factor.max_probe.endtime*1000;
        summaryItem.r_factor.max_probe_id = statItem.r_factor.max_probe.id;
        summaryItem.r_factor.last = statItem.r_factor.last;
        summaryItem.r_factor.last_timestamp = statItem.r_factor.last_probe.endtime*1000;
        summaryItem.r_factor.last_probe_id = statItem.r_factor.last_probe.id;

        summaryItem.mos_network.average = statItem.mos_network.average;
        summaryItem.mos_network.min = statItem.mos_network.min;
        summaryItem.mos_network.min_timestamp = statItem.mos_network.min_probe.endtime*1000;
        summaryItem.mos_network.min_probe_id = statItem.mos_network.min_probe.id;
        summaryItem.mos_network.max = statItem.mos_network.max;
        summaryItem.mos_network.max_timestamp = statItem.mos_network.max_probe.endtime*1000;
        summaryItem.mos_network.max_probe_id = statItem.mos_network.max_probe.id;
        summaryItem.mos_network.last = statItem.mos_network.last;
        summaryItem.mos_network.last_timestamp = statItem.mos_network.last_probe.endtime*1000;
        summaryItem.mos_network.last_probe_id = statItem.mos_network.last_probe.id;
      }

      for (let item of entities) {
        summaryMap.set(item, {});
      }
    }

    return summaryMap;
  }

  public calculateSummaryStatistics(entityStatistics: StatisticsEntity[]): StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry> {
    let summary: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry> = {
      lastValueTimestampStart: 0,
      lastValueTimestampEnd: 0
    };

    if (entityStatistics) {
      for (let statValue of entityStatistics) {
        this.calculateEntitySummaryStatistics(summary, statValue);
      }

      this.calculateAverageValues(summary);
    }

    return summary;
  }

  public calculateSummaryVisualStatisticsPerItem(summaryMap: Map<string, StatisticsEntityBase<StatisticsDataEntry>>): Map<string, StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry>> {
    let summaryVisualMap = new Map<string, StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry>>();
    for (let k of summaryMap.keys()) {
      let summary = summaryMap.get(k);

      let summaryVisual: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry> =
        this.calculateSummaryVisualStatistics(summary);

      summaryVisualMap.set(k, summaryVisual);
    }
    return summaryVisualMap;
  }

  public calculateSummaryVisualStatistics(summary: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry>): StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry> {
    let summaryVisual: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataStyledVisualEntry> = {
      lastValueTimestampStart: summary ? summary.lastValueTimestampStart : 0,
      lastValueTimestampEnd: summary ? summary.lastValueTimestampEnd : 0
    };

    summaryVisual.mos_pvqa = this.calculateItemVisualStatistics(summary ? summary.mos_pvqa : undefined);
    summaryVisual.mos_aqua = this.calculateItemVisualStatistics(summary ? summary.mos_aqua : undefined);
    summaryVisual.r_factor = this.calculateItemVisualStatistics(summary ? summary.r_factor : undefined);
    summaryVisual.mos_network = this.calculateItemVisualStatistics(summary ? summary.mos_network : undefined);

    return summaryVisual;
  }

  public calculateItemVisualStatistics(item: StatisticsDataEntry): StatisticsDataStyledVisualEntry {

    return {
      Sum: (item && item.Sum) ? item.Sum.toFixed(0) : 'no data',
      Count: (item && item.Count) ? item.Count.toFixed(0) : 'no data',
      average: (item && item.average) ? item.average.toFixed(2) : 'no data',
      min: (item && item.min) ? item.min.toFixed(2) : 'no data',
      min_timestamp: (item && item.min_timestamp) ? item.min_timestamp : 0,
      min_probe_id: (item && item.min_probe_id) ? item.min_probe_id : '',
      max: (item && item.max) ? item.max.toFixed(2) : 'no data',
      max_timestamp: (item && item.max_timestamp) ? item.max_timestamp : 0,
      max_probe_id: (item && item.max_probe_id) ? item.max_probe_id : '',
      last: (item && item.last) ? item.last.toFixed(2) : 'no data',
      last_timestamp: (item && item.last_timestamp) ? item.last_timestamp : 0,
      last_probe_id: (item && item.last_probe_id) ? item.last_probe_id : '',
      avgLevel: this.cssService.getLevel((item) ? item.average : undefined),
      minLevel: this.cssService.getLevel((item) ? item.min : undefined),
      maxLevel: this.cssService.getLevel((item) ? item.max : undefined),
      lastLevel: this.cssService.getLevel((item) ? item.last : undefined)
    }
  }

  private calculateEntitySummaryStatistics(entitySummary: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry>, entityStatistic: StatisticsEntity): void {
    if (entitySummary && entityStatistic) {

      if (entityStatistic.start_timestamp * 1000 > entitySummary.lastValueTimestampStart) {
        entitySummary.lastValueTimestampStart = entityStatistic.start_timestamp * 1000;
        entitySummary.lastValueTimestampEnd = entityStatistic.end_timestamp * 1000;
      }
      let end_timestamp = entityStatistic.end_timestamp * 1000;

      if (entityStatistic.mos_pvqa.Count > 0) {
        if (!entitySummary.mos_pvqa) {
          entitySummary.mos_pvqa = <StatisticsDataEntry> this.modelService.deepCopy(entityStatistic.mos_pvqa);
          entitySummary.mos_pvqa.min_timestamp = end_timestamp;
          entitySummary.mos_pvqa.max_timestamp = end_timestamp;
        }
        else {
          entitySummary.mos_pvqa.Sum += entityStatistic.mos_pvqa.Sum;
          entitySummary.mos_pvqa.Count += entityStatistic.mos_pvqa.Count;
          if (entityStatistic.mos_pvqa.min < entitySummary.mos_pvqa.min) {
            entitySummary.mos_pvqa.min = entityStatistic.mos_pvqa.min;
            entitySummary.mos_pvqa.min_timestamp = end_timestamp;
          }
          if (entityStatistic.mos_pvqa.max > entitySummary.mos_pvqa.max) {
            entitySummary.mos_pvqa.max = entityStatistic.mos_pvqa.max;
            entitySummary.mos_pvqa.max_timestamp = end_timestamp;
          }
        }
      }

      if (entityStatistic.mos_aqua.Count > 0) {
        if (!entitySummary.mos_aqua) {
          entitySummary.mos_aqua = <StatisticsDataEntry> this.modelService.deepCopy(entityStatistic.mos_aqua);
          entitySummary.mos_aqua.min_timestamp = end_timestamp;
          entitySummary.mos_aqua.max_timestamp = end_timestamp;
        } else {
          entitySummary.mos_aqua.Sum += entityStatistic.mos_aqua.Sum;
          entitySummary.mos_aqua.Count += entityStatistic.mos_aqua.Count;
          if (entityStatistic.mos_aqua.min < entitySummary.mos_aqua.min) {
            entitySummary.mos_aqua.min = entityStatistic.mos_aqua.min;
            entitySummary.mos_aqua.min_timestamp = end_timestamp;
          }
          if (entityStatistic.mos_aqua.max > entitySummary.mos_aqua.max) {
            entitySummary.mos_aqua.max = entityStatistic.mos_aqua.max;
            entitySummary.mos_aqua.max_timestamp = end_timestamp;
          }
        }
      }

      if (entityStatistic.r_factor.Count > 0) {
        if (!entitySummary.r_factor) {
          entitySummary.r_factor = <StatisticsDataEntry> this.modelService.deepCopy(entityStatistic.r_factor);
          entitySummary.r_factor.min_timestamp = end_timestamp;
          entitySummary.r_factor.max_timestamp = end_timestamp;
        } else {
          entitySummary.r_factor.Sum += entityStatistic.r_factor.Sum;
          entitySummary.r_factor.Count += entityStatistic.r_factor.Count;
          if (entityStatistic.r_factor.min < entitySummary.r_factor.min) {
            entitySummary.r_factor.min = entityStatistic.r_factor.min;
            entitySummary.r_factor.min_timestamp = end_timestamp;
          }
          if (entityStatistic.r_factor.max > entitySummary.r_factor.max) {
            entitySummary.r_factor.max = entityStatistic.r_factor.max;
            entitySummary.r_factor.max_timestamp = end_timestamp;
          }
        }
      }

      if (entityStatistic.mos_network.Count > 0) {
        if (!entitySummary.mos_network) {
          entitySummary.mos_network = <StatisticsDataEntry> this.modelService.deepCopy(entityStatistic.mos_network);
          entitySummary.mos_network.min_timestamp = end_timestamp;
          entitySummary.mos_network.max_timestamp = end_timestamp;
        } else {
          entitySummary.mos_network.Sum += entityStatistic.mos_network.Sum;
          entitySummary.mos_network.Count += entityStatistic.mos_network.Count;
          if (entityStatistic.mos_network.min < entitySummary.mos_network.min) {
            entitySummary.mos_network.min = entityStatistic.mos_network.min;
            entitySummary.mos_network.min_timestamp = end_timestamp;
          }
          if (entityStatistic.mos_network.max > entitySummary.mos_network.max) {
            entitySummary.mos_network.max = entityStatistic.mos_network.max;
            entitySummary.mos_network.max_timestamp = end_timestamp;
          }
        }
      }
    }
  }

  private calculateAverageValues(entitySummary: StatisticsEntityBaseWithLastValueTimestamp<StatisticsDataEntry>): void {
    if (entitySummary.mos_pvqa) {
      entitySummary.mos_pvqa.average = entitySummary.mos_pvqa.Sum / entitySummary.mos_pvqa.Count;
    }

    if (entitySummary.mos_aqua) {
      entitySummary.mos_aqua.average = entitySummary.mos_aqua.Sum / entitySummary.mos_aqua.Count;
    }

    if (entitySummary.r_factor) {
      entitySummary.r_factor.average = entitySummary.r_factor.Sum / entitySummary.r_factor.Count;
    }

    if (entitySummary.mos_network) {
      entitySummary.mos_network.average = entitySummary.mos_network.Sum / entitySummary.mos_network.Count;
    }
  }

  private intDiv(val, div): number {
    return (val - val % div) / div;
  }
}
