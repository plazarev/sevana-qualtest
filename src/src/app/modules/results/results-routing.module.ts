import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {ResultsListComponent} from "./components/results-list.component";
import {ResultsReportPvqaComponent} from "./components/results-report-pvqa.component";
import {ResultsReportAquaComponent} from "./components/results-report-aqua.component";
import {ResultsFullListComponent} from "./components/results-full-list.component";
import {SevanaQualtestAuthGuard} from "../../sevana-qualtest-auth.guard";

const routes: Routes = [
  {
    path: 'results',
    canActivate: [SevanaQualtestAuthGuard],
    canActivateChild: [SevanaQualtestAuthGuard],
    children: [
      {path: '', redirectTo: 'list', pathMatch: 'full'},
      {path: 'list', component: ResultsFullListComponent},
      {
        path: 'report',
        children: [
          {path: '', redirectTo: '/results', pathMatch: 'full'},
          {path: 'pvqa', redirectTo: '/results', pathMatch: 'full'},
          {path: 'aqua', redirectTo: '/results', pathMatch: 'full'},
          {path: 'pvqa/:id', component: ResultsReportPvqaComponent},
          {path: 'aqua/:id', component: ResultsReportAquaComponent},
          ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultsRoutingModule {
}
