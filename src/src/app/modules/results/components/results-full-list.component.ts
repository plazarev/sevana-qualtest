import {
  ChangeDetectorRef, Component, TemplateRef,
  ViewChild
} from "@angular/core";
import {
  UxSortTypes,
  UxTable, UxTableColumn,
  UxTableRow
} from "../../../../ux-lib/components/table/table.model";
import {Router} from "@angular/router";
import {Subject, Subscription} from "rxjs/index";
import {ResultsService} from "../results.service";
import {TestResultEntity} from "../../../common/models/entity/test-result-entity.model";
import {ModelService} from "../../../common/services/model.service";
import {CountResult, DeleteOperationResult} from "../../../common/services/entity.service";
import {NotificationService} from "../../../common/services/notification.service";
import {AppConfigService} from "../../../common/services/app-config.service";
import {PagedComponent} from "../../../components/base/paged.component";
import {ApiPath} from "../../../common/services/api.path";
import {takeUntil, tap} from "rxjs/internal/operators";
import {QueryParam} from "../../../common/models/query-param.type";
import {ConfirmPopupComponent} from "../../../components/confirm-popup/confirm-popup.component";

export interface ResultColumnSortEvent {
  columnIndex: number;
  columnName: string;
  sortOrder: UxSortTypes;
}

@Component({
  selector: "sq-results-full-list",
  templateUrl: "results-full-list.component.html",
  host: {"[class.sq-results-full-list]": "true"}
})
export class ResultsFullListComponent extends PagedComponent {
  private _results: TestResultEntity[] = [];
  private _resultDeleteID: string;

  _tableDataModel: UxTable = {
    header: {
      rows: [
        {
          styleClass: "_header",
          columns: [
            {
              value: "Device",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "Test",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "Target",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              },
            },
            {
              value: "Time",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              },
            },
            {
              value: "Duration",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "AQUA",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "PVQA",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "MOS Network",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "Sevana R-factor",
              styleClass: "_header",
              contentModel: {
                "id": "name"
              }
            },
            {
              value: "",
              styleClass: "_header"
            }
          ]
        }
      ]
    },
    body: {rows: []}
  };

  private stopSubscription$ = new Subject<boolean>();
  private resultsCountSubscription$: Subscription;
  private resultsSubscription$: Subscription;
  private resultsDeleteSubscription$: Subscription;

  @ViewChild("tableEmptyContent")
  tableEmptyContent: TemplateRef<any>;

  @ViewChild("timeColumn")
  timeColumn: TemplateRef<any>;

  @ViewChild("durationColumn")
  durationColumn: TemplateRef<any>;

  @ViewChild("functionsColumn")
  functionsColumn: TemplateRef<any>;

  @ViewChild("resultDeleteConfirmPopup")
  resultDeleteConfirmPopup: ConfirmPopupComponent<TestResultEntity>;

  constructor(private resultsService: ResultsService,
              private notificationService: NotificationService,
              private modelService: ModelService,
              private router: Router,
              private cdr: ChangeDetectorRef,
              protected configService: AppConfigService) {
    super(configService);
  }

  public initComponent() {
    super.initComponent();
    this._tableDataModel.emptyTableContent = this.tableEmptyContent;
  }

  public destroyComponent() {
    this.stopSubscription$.next(true);
    this.stopSubscription$.complete();
  }

  public loadPageData(currentPage: number, pageSize: number): void {
    let queryParams = [
      {
        key: ApiPath.OFFSET,
        value: (currentPage-1) * pageSize
      },
      {
        key: ApiPath.LIMIT,
        value: pageSize
      }
    ];

    queryParams[ApiPath.OFFSET] = (currentPage-1) * pageSize;
    queryParams[ApiPath.LIMIT] = pageSize;

    this.loadResultsCount(queryParams);
  }

  private loadResultsCount(queryParams: QueryParam[]): void {
    this._pageLoadingStatus = 'loading';

    this.resultsCountSubscription$ && this.resultsCountSubscription$.unsubscribe();
    this.resultsCountSubscription$ = this.resultsService
      .getEntitiesCount()
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((value: CountResult) => {
          this.pagingConfig.totalCount = value.totalcount;
          this._pageLoadingStatus = 'loading';
          this.loadResultsPage(queryParams);
        })
      )
      .subscribe();
  }

  private loadResultsPage(queryParams: QueryParam[]): void {
    this.resultsSubscription$ && this.resultsSubscription$.unsubscribe();
    this.resultsSubscription$ = this.resultsService
      .getEntityList(queryParams)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((values: TestResultEntity[]) => {
          if (values) {
            this._results = values;

            this._tableDataModel.body.rows =
              this._results
                .map((result: TestResultEntity) => {
                  return this.getTableRow(result);
                });
          }
          else {
            this._results = [];
          }
          this._pageLoadingStatus = 'loaded';
        })
      )
      .subscribe();
  }

  _onDeleteIcon(result: TestResultEntity) {
    this._resultDeleteID = result.id;
    this.resultDeleteConfirmPopup.userData = result;
    this.resultDeleteConfirmPopup.show();
  }

  _onChartIcon(result: TestResultEntity) {
    this.router.navigate(['/reports/view'], {
      queryParams: {
        probe: result.id
      }
    });
  }

  _onDeleteCancel(result: TestResultEntity) {
    this._resultDeleteID = undefined;
  }

  _onResultDelete(result: TestResultEntity) {
    let resultID = result.id;
    this.resultsDeleteSubscription$ && this.resultsDeleteSubscription$.unsubscribe();
    this.resultsDeleteSubscription$ = this.resultsService
      .deleteEntity(resultID)
      .pipe(
        takeUntil(this.stopSubscription$),
        tap((data: DeleteOperationResult) => {
          if (data.opResult) {
            let deletedIndex: number = this.modelService.findInArray(this._tableDataModel.body.rows, (item: UxTableRow) => {
              return (item.id === resultID);
            });
            if (deletedIndex >= 0) {
              this._tableDataModel.body.rows.splice(deletedIndex, 1);
            }
          }
          else {
            this.notificationService.pushNotification({
              type: "error",
              caption: "Deletion error",
              content: `Fail to delete test result ${resultID}`
            })
          }
        })
      )
      .subscribe();

    this._resultDeleteID = undefined;
  }

  private getTableRow(result: TestResultEntity): UxTableRow {
    let columns: UxTableColumn[]  = [
      {
        value: result.phone_name
      },
      {
        value: result.task_name
      },
      {
        value: result.target
      },
      {
        type: 'content',
        value: this.timeColumn,
        contentModel: {
          endtime: result.endtime * 1000
        }
      },
      {
        type: 'content',
        value: this.durationColumn,
        contentModel: {
          duration: result.duration
        }
      },
      {
        value: result.mos_aqua === 0 ? result.mos_aqua : result.mos_aqua.toFixed(3)
      },
      {
        value: result.mos_pvqa === 0 ? result.mos_pvqa : result.mos_pvqa.toFixed(3)
      },
      {
        value: result.mos_network === 0 ? result.mos_network : result.mos_network.toFixed(3)
      },
      {
        value: result.r_factor
      },
      {
        type: 'content',
        value: this.functionsColumn,
        contentModel: {
          result: result
        }
      }
    ];

    return {
      id: result.id,
      columns: columns
    };
  }
}
