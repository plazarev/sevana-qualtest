
## Prerequisites
You need to have NodeJS installed

---

## Prepare environment

1. download all dependencies:
`node install`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Configuration of app:
File 'src/src/app-config.json' contains app configuration:

```json
{
  // 'paging' section allow to configure default settings for pagination in tables
  "paging": {
    // default count of items per page
    "pageSize": 10,
    
    // available count of items per page, displayed in table paginator
    "pageSizeOptions": [10, 20, 50, 100, 200]
  },
  
  // 'routes' section allow to configure REST settings
  "routes": {
    // server to get REST data. If missed, when current server will be used.
    "server": "http://voipobjects.com:8080"
  }
}
```